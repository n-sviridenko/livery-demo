<?php

namespace Livery\Bundle\ApiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Livery\Bundle\CoreBundle\Location\LocationManager;
use Livery\Bundle\ApiBundle\Form\DataTransformer\PlaceIdToLocationTransformer;

/**
 * Class PlaceIdType
 */
class PlaceIdType extends AbstractType
{
    /**
     * @var LocationManager
     */
    private $locationManager;

    /**
     * LatLngToLocationTransformer constructor.
     *
     * @param LocationManager $locationManager
     */
    public function __construct(LocationManager $locationManager)
    {
        $this->locationManager = $locationManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new PlaceIdToLocationTransformer($this->locationManager, $options['location_types']);

        $builder->addModelTransformer($transformer);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'location_types' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return TextType::class;
    }
}
