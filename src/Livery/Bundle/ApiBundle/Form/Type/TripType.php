<?php

namespace Livery\Bundle\ApiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

use Livery\Bundle\EntityBundle\Entity\Trip\Trip;
use Livery\Bundle\CoreBundle\Measure\MeasureManager;
use Livery\Bundle\EntityBundle\Entity\Location\AbstractLocation;

/**
 * Class TripType
 */
class TripType extends AbstractType
{
    /**
     * @var MeasureManager
     */
    private $measureManager;

    /**
     * TripType constructor.
     *
     * @param MeasureManager $measureManager
     */
    public function __construct(MeasureManager $measureManager)
    {
        $this->measureManager = $measureManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locationTypes = [
            AbstractLocation::TYPE_LOCATION,
            AbstractLocation::TYPE_AREA,
        ];

        $supportedMassMeasures = $this->measureManager->getSupportedMassMeasures();
        $supportedMassMeasures = array_combine($supportedMassMeasures, $supportedMassMeasures);

        $allowedMaxResponseDelaysHourly = array_combine(
            Trip::getAllowedResponseDelaysHourly(),
            Trip::getAllowedResponseDelaysHourly()
        );

        $allowedVehicles = array_combine(Trip::getAllowedVehicles(), Trip::getAllowedVehicles());

        $builder
            ->add('from', PlaceIdType::class, ['location_types' => $locationTypes])
            ->add('to', PlaceIdType::class, ['location_types' => $locationTypes])
            ->add('departAt', DateTimeType::class, ['widget' => 'single_text'])
            ->add('arriveAt', DateTimeType::class, ['widget' => 'single_text'])
            // @todo: add some sanitizer that will prevent more than 1 \n to the EACH multiline text that can be filled by user
            ->add('description', TextareaType::class)
            ->add('maxResponseDelayHourly', ChoiceType::class, ['choices' => $allowedMaxResponseDelaysHourly])
            ->add('vehicle', ChoiceType::class, ['choices' => $allowedVehicles])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class'      => Trip::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'trip';
    }
}
