<?php

namespace Livery\Bundle\ApiBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserGet
 */
class UserGet
{
    const FIELD_PROTECTED    = 'protected';
    const FIELD_REVIEWS      = 'reviews';
    const FIELD_REVIEWS_STAT = 'reviews_stat';

    /**
     * @var array
     *
     * @Assert\Choice(multiple=true, callback={UserGet::class, "getAllowedFields"})
     */
    private $fields = [];

    /**
     * @return array
     */
    public static function getAllowedFields()
    {
        return [
            self::FIELD_PROTECTED,
            self::FIELD_REVIEWS,
            self::FIELD_REVIEWS_STAT,
        ];
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param string $field
     *
     * @return bool
     */
    public function hasField($field)
    {
        return in_array($field, $this->fields);
    }

    /**
     * @param array $fields
     */
    public function setFields(array $fields)
    {
        $this->fields = $fields;
    }
}
