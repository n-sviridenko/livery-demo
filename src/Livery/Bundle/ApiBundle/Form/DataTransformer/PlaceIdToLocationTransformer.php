<?php

namespace Livery\Bundle\ApiBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

use Livery\Bundle\CoreBundle\Location\LocationManager;

/**
 * Class PlaceIdToLocationTransformer
 */
class PlaceIdToLocationTransformer implements DataTransformerInterface
{
    /**
     * @var LocationManager
     */
    private $locationManager;

    /**
     * @var array|null
     */
    private $types;

    /**
     * LatLngToLocationTransformer constructor.
     *
     * @param LocationManager $locationManager
     * @param array|null      $types
     */
    public function __construct(LocationManager $locationManager, array $types = null)
    {
        $this->locationManager = $locationManager;
        $this->types           = $types;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        return $value !== null ? $value->getPlaceId() : null;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return null;
        }

        $location = $this->locationManager->getLocationByPlaceId($value, $this->types);

        if (!$location) {
            throw new TransformationFailedException(sprintf('Could not find a location [placeId=%s]!', $value));
        }

        return $location;
    }
}
