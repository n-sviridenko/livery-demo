<?php

namespace Livery\Bundle\ApiBundle\Form\DataTransformer;

use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use Http\Discovery\MessageFactoryDiscovery;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

use Livery\Bundle\MediaBundle\Model\RemoteFile;

/**
 * Class UrlToUploadedFileTransformer
 */
class UrlToUploadedFileTransformer implements DataTransformerInterface
{
    /**
     * @var HttpClient
     */
    private $client;

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var int
     */
    private $maxLength;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * MediaUploadType constructor.
     *
     * @param HttpClient         $client
     * @param ValidatorInterface $validator
     * @param int                $maxLength
     */
    public function __construct(HttpClient $client, ValidatorInterface $validator, $maxLength)
    {
        $this->client         = $client;
        $this->messageFactory = MessageFactoryDiscovery::find();
        $this->validator      = $validator;
        $this->maxLength      = $maxLength;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        // just a one-way transformation
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return null;
        }

        // this transformer is used always, even if the $value is an UploadedFile
        if ($value instanceof UploadedFile) {
            return $value;
        }

        if (!$this->validator->validate($value, [new Assert\Url(['checkDNS' => true])])) {
            throw new TransformationFailedException('Provided URL is not valid.');
        }

        $request = $this->messageFactory->createRequest('GET', $value);

        try {
            $response = $this->client->sendRequest($request);
        } catch (\Exception $e) {
            throw new TransformationFailedException($e->getMessage());
        }

        if ($response->getHeaderLine('Content-Length') > $this->maxLength) {
            throw new TransformationFailedException('The file is too large.');
        }

        $filesystem = new Filesystem();
        $tempPath   = $filesystem->tempnam(sys_get_temp_dir(), 'url_upload');

        if (!@file_put_contents($tempPath, $response->getBody())) {
            throw new \Exception('Could not write file contents.');
        }

        $file = new RemoteFile($tempPath);
        $file->setSourceUrl($value);

        return $file;
    }
}
