<?php

namespace Livery\Bundle\ApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class LiveryApiBundle
 */
class LiveryApiBundle extends Bundle
{
}
