<?php

namespace Livery\Bundle\ApiBundle\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;

use Livery\Bundle\EntityBundle\Entity\User;
use Livery\Component\User\Model\UserInterface;

/**
 * Class UserNormalizer
 */
class UserNormalizer implements NormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    const PROFILE = 'profile';
    const PICTURE = 'picture';

    /**
     * @var PhoneNumberNormalizer
     */
    private $phoneNumberNormalizer;

    /**
     * @var MediaNormalizer
     */
    private $mediaNormalizer;

    /**
     * UserNormalizer constructor.
     *
     * @param PhoneNumberNormalizer $phoneNumberNormalizer
     * @param MediaNormalizer       $mediaNormalizer
     */
    public function __construct(
        PhoneNumberNormalizer $phoneNumberNormalizer,
        MediaNormalizer $mediaNormalizer
    ) {
        $this->phoneNumberNormalizer      = $phoneNumberNormalizer;
        $this->mediaNormalizer            = $mediaNormalizer;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        /** @var UserInterface $user */
        $user = $object;

        $fields  = array_key_exists('fields', $context) ? $context['fields'] : [];
        $picture = $user->getPicture() ? $this->mediaNormalizer->normalize($user->getPicture()) : null;

        $result = [
            'id'            => $user->getId(),
            'firstName'     => $user->getFirstName(),
            'lastName'      => $user->getLastName(),
            'age'           => $user->getAge(),
            'locale'        => $user->getLocale(),
            'picture'       => $picture,
            'createdAt'     => $user->getCreatedAt(),
            'lastLogin'     => $user->getLastLogin(),
            'about'         => $user->getAbout(),
        ];

        if (in_array('protected', $fields)) {
            $result += $this->normalizeProtected($user, $format);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return ($data instanceof User);
    }

    /**
     * @param UserInterface $user
     * @param string|null   $format
     *
     * @return array
     */
    private function normalizeProtected(UserInterface $user, $format)
    {
        $phone = $user->getPhone() ? $this->normalizer->normalize($user->getPhone(), $format) : null;

        return [
            'email'     => $user->getEmail(),
            'phone'     => $phone,
            'sex'       => $user->getSex(),
            'birthdate' => $user->getBirthdate(),
        ];
    }
}
