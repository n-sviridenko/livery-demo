<?php

namespace Livery\Bundle\ApiBundle\Normalizer;

use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class PhoneNumberNormalizer
 */
class PhoneNumberNormalizer implements NormalizerInterface
{
    /**
     * @var PhoneNumberUtil
     */
    private $phoneNumberUtil;

    /**
     * PhoneNumberNormalizer constructor.
     *
     * @param PhoneNumberUtil $phoneNumberUtil
     */
    public function __construct(PhoneNumberUtil $phoneNumberUtil)
    {
        $this->phoneNumberUtil = $phoneNumberUtil;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'countryCode'    => $object->getCountryCode(),
            'nationalNumber' => $object->getNationalNumber(),
            'localized'      => $this->phoneNumberUtil->format($object, PhoneNumberFormat::NATIONAL),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return ($data instanceof PhoneNumber);
    }
}
