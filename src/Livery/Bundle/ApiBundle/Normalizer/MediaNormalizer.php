<?php

namespace Livery\Bundle\ApiBundle\Normalizer;

use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

use Livery\Bundle\EntityBundle\Entity\Media\Image;
use Livery\Bundle\MediaBundle\Manager\ImageManager;
use Livery\Bundle\MediaBundle\Manager\UploadManager;
use Livery\Bundle\EntityBundle\Entity\Media\AbstractMedia;

/**
 * Class MediaNormalizer
 */
class MediaNormalizer implements NormalizerInterface
{
    /**
     * @var UploadManager
     */
    private $uploadManager;

    /**
     * @var ImageManager
     */
    private $imageManager;

    /**
     * MediaNormalizer constructor.
     *
     * @param UploadManager $uploadManager
     * @param ImageManager  $imageManager
     */
    public function __construct(UploadManager $uploadManager, ImageManager $imageManager)
    {
        $this->uploadManager = $uploadManager;
        $this->imageManager  = $imageManager;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $return = $this->normalizeMedia($object);

        if ($object instanceof Image) {
            $return += $this->normalizeImage($object);
        }

        return $return;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return ($data instanceof AbstractMedia);
    }

    /**
     * @param object $object
     *
     * @return array
     */
    private function normalizeMedia($object)
    {
        $url = $this->uploadManager->getDownloadUrl($object->getFullPath());

        return [
            'id'   => $object->getId(),
            'type' => $object->getType(),
            'url'  => $url,
        ];
    }

    /**
     * @param Image $image
     *
     * @return array
     */
    private function normalizeImage(Image $image)
    {
        $filters    = $this->imageManager->getFiltersByCategory($image->getCategory());
        $thumbnails = [];

        foreach ($filters as $filter) {
            $thumbnailUrl = $this->imageManager->getThumbnailUrl($image, $filter);

            $thumbnails[] = [
                'type' => $filter,
                'url'  => $thumbnailUrl,
            ];
        }

        return [
            'thumbnails' => $thumbnails,
        ];
    }
}
