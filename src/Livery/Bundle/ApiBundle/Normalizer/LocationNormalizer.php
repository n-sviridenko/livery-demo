<?php

namespace Livery\Bundle\ApiBundle\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

use Livery\Bundle\EntityBundle\Entity\Location\Area;
use Livery\Bundle\CoreBundle\Location\LocationManager;
use Livery\Bundle\EntityBundle\Entity\Location\Country;
use Livery\Bundle\EntityBundle\Entity\Location\Location;
use Livery\Bundle\EntityBundle\Entity\Location\AbstractLocation;

/**
 * Class LocationNormalizer
 */
class LocationNormalizer implements NormalizerInterface
{
    /**
     * @var LocationManager
     */
    protected $locationManager;

    /**
     * LocationNormalizer constructor.
     *
     * @param LocationManager $locationManager
     */
    public function __construct(LocationManager $locationManager)
    {
        $this->locationManager = $locationManager;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        if ($object instanceof Area) {
            return $this->normalizeArea($object);
        }

        if ($object instanceof Country) {
            return $this->normalizeCountry($object);
        }

        if ($object instanceof Location) {
            return $this->normalizeLocation($object);
        }

        throw new \Exception(
            sprintf('Class "%s" doesn\'t covered by this normalizer', get_class($object))
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return ($data instanceof AbstractLocation);
    }

    /**
     * @param AbstractLocation $object
     *
     * @return array
     */
    protected function normalizeAbstractLocation(AbstractLocation $object)
    {
        $object = $this->locationManager->hydrateLocation($object);

        return [
            'id'      => $object->getId(),
            'placeId' => $object->getPlaceId(),
            'type'    => $object->getType(),
        ];
    }

    /**
     * @param Location $object
     *
     * @return array
     */
    protected function normalizeLocation(Location $object)
    {
        $area = $this->normalizeArea($object->getArea());

        $return  = $this->normalizeAbstractLocation($object);
        $return += [
            'lat'              => $object->getLat(),
            'lng'              => $object->getLng(),
            'area'             => $area,
            'formattedAddress' => $object->translate()->getFormattedAddress(),
        ];

        return $return;
    }

    /**
     * @param Area $object
     *
     * @return array
     */
    protected function normalizeArea(Area $object)
    {
        $country = $this->normalizeCountry($object->getCountry());

        // @todo: use just getFormattedAddress() whan It will work always
        $formattedAddress = $object->translate()->getFormattedAddress()
            ?: sprintf('%s, %s', $object->translate()->getName(), $object->getCountry()->getName());

        $return  = $this->normalizeAbstractLocation($object);
        $return += [
            'lat'              => $object->getLat(),
            'lng'              => $object->getLng(),
            'country'          => $country,
            'slug'             => $object->getSlug(),
            'name'             => $object->translate()->getName(),
            'formattedAddress' => $formattedAddress,
        ];

        return $return;
    }

    /**
     * @param Country $object
     *
     * @return array
     */
    protected function normalizeCountry(Country $object)
    {
        $return  = $this->normalizeAbstractLocation($object);
        $return += [
            'code' => $object->getCode(),
            'slug' => $object->getSlug(),
            'name' => $object->translate()->getName(),
        ];

        return $return;
    }
}
