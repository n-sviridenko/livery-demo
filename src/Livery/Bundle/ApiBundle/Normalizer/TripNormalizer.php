<?php

namespace Livery\Bundle\ApiBundle\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

use Livery\Bundle\EntityBundle\Entity\Trip\Trip;

/**
 * Class TripNormalizer
 */
class TripNormalizer implements NormalizerInterface
{
    /**
     * @var LocationNormalizer
     */
    private $locationNormalizer;

    /**
     * @var UserNormalizer
     */
    private $userNormalizer;

    /**
     * TripNormalizer constructor.
     *
     * @param LocationNormalizer $locationNormalizer
     * @param UserNormalizer     $userNormalizer
     */
    public function __construct(LocationNormalizer $locationNormalizer, UserNormalizer $userNormalizer)
    {
        $this->locationNormalizer = $locationNormalizer;
        $this->userNormalizer     = $userNormalizer;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        /** @var Trip $trip */
        $trip = $object;

        $from      = $this->locationNormalizer->normalize($trip->getFrom());
        $to        = $this->locationNormalizer->normalize($trip->getTo());
        $traveller = $this->userNormalizer->normalize($trip->getTraveller());

        return [
            'id'                     => $trip->getId(),
            'from'                   => $from,
            'to'                     => $to,
            'departAt'               => $trip->getDepartAt(),
            'arriveAt'               => $trip->getArriveAt(),
            'traveller'              => $traveller,
            'description'            => $trip->getDescription(),
            'maxResponseDelayHourly' => $trip->getMaxResponseDelayHourly(),
            'vehicle'                => $trip->getVehicle(),
            'createdAt'              => $trip->getCreatedAt(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return ($data instanceof Trip);
    }
}
