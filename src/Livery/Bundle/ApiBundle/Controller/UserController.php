<?php

namespace Livery\Bundle\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;

use Livery\Bundle\EntityBundle\Entity\User;
use Livery\Bundle\ApiBundle\Form\Model\UserGet;
use Livery\Bundle\ApiBundle\Form\Type\UserGetType;

/**
 * Class UserController
 */
class UserController extends FOSRestController
{
    /**
     * @todo: add tests for viewing of fields
     *
     * @param Request $request
     * @param User    $user
     *
     * @return Response
     */
    public function getUserAction(Request $request, User $user)
    {
        $this->denyAccessUnlessGranted('view', $user);

        $options = new UserGet();

        $form = $this->createForm(UserGetType::class, $options);
        $form->handleRequest($request);

        // form could be not submitted because all fields aren't required
        if ($form->isSubmitted() && !$form->isValid()) {
            $view = $this->view($form, Response::HTTP_BAD_REQUEST);
        } else {
            if ($options->hasField(UserGet::FIELD_PROTECTED)) {
                $this->denyAccessUnlessGranted('viewAll', $user);
            }

            $view = $this->view($user);
            $view->getContext()->setAttribute('fields', $options->getFields());
        }

        return $this->handleView($view);
    }
}
