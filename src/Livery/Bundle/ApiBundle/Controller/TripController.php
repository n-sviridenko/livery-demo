<?php

namespace Livery\Bundle\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;

use Livery\Bundle\ApiBundle\Form\Type\TripType;
use Livery\Bundle\EntityBundle\Entity\Trip\Trip;

/**
 * Class TripController
 */
class TripController extends FOSRestController
{
    /**
     * @param Trip $trip
     *
     * @return Response
     */
    public function getTripAction(Trip $trip)
    {
        $this->denyAccessUnlessGranted('view', $trip);

        $view = $this->view($trip);

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function postTripsAction(Request $request)
    {
        $trip = new Trip();
        $trip->setTraveller($this->getUser());

        $form = $this->createForm(TripType::class, $trip);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            $view = $this->view($form, Response::HTTP_BAD_REQUEST);
        } else {
            $this->get('livery.core.trip.trip_manager')->saveTrip($trip);

            $view = $this->view($trip, Response::HTTP_CREATED);
        }

        return $this->handleView($view);
    }
}
