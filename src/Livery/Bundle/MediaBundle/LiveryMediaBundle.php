<?php

namespace Livery\Bundle\MediaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class LiveryMediaBundle
 *
 * @todo: move into core
 */
class LiveryMediaBundle extends Bundle
{
}
