<?php

namespace Livery\Bundle\MediaBundle\Manager;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\File\File;

use Livery\Bundle\MediaBundle\Model\RemoteFile;
use Livery\Bundle\EntityBundle\Entity\Media\Image;
use Livery\Bundle\EntityBundle\Entity\Media\Document;
use Livery\Bundle\EntityBundle\Entity\Media\AbstractMedia;

/**
 * Class UploadManager
 *
 * @todo: attach all medias that doesn't have an owner when they've attached to some entity that has an user
 */
class UploadManager
{
    /**
     * @var string
     */
    private $publicPath;

    /**
     * @var string
     */
    private $uploadPath;

    /**
     * @var string
     */
    private $host;

    /**
     * UploadManager constructor.
     *
     * @param string $publicPath
     * @param string $uploadPath
     * @param string $host
     */
    public function __construct($publicPath, $uploadPath, $host)
    {
        $this->publicPath = $publicPath;
        $this->uploadPath = $uploadPath;
        $this->host       = $host;
        $this->filesystem = new Filesystem();
    }

    /**
     * @param File   $file
     * @param string $category
     *
     * @return AbstractMedia
     */
    public function uploadFile(File $file, $category)
    {
        if (in_array($file->getMimeType(), ImageManager::$supportedMimeTypes)) {
            $media = new Image();

            // @todo: set image width etc.
        } else {
            $media = new Document();
        }

        // @todo: add unique check by source url (with some time cache)
        // if some media with the same source url exists and it was created
        // not too much time ago, copy-paste this entity, but use the same file
        // path, and if a media will be removed check if there is no media that
        // links to this path, and if no - remove the file by this path
        if ($file instanceof RemoteFile) {
            $media->setSourceUrl($file->getSourceUrl());
        }

        $media
            ->setCategory($category)
            ->setPath($this->generateFreeRandomPath())
            ->setExtension($file->getClientOriginalExtension())
            ->setOriginalName($file->getClientOriginalName())
            ->setMimeType($file->getMimeType())
            ->setSize($file->getSize())
            ->setStatus(null)
        ;

        $absolutePath = $this->getAbsolutePath($media->getFullPath());

        $file->move(dirname($absolutePath), basename($absolutePath));

        return $media;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function getDownloadUrl($path)
    {
        return "http://{$this->host}{$this->uploadPath}/{$path}";
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function getAbsolutePath($path)
    {
        return $this->publicPath.$this->uploadPath.DIRECTORY_SEPARATOR.$path;
    }

    /**
     * @param string $path
     */
    public function removeFile($path)
    {
        $absolutePath = $this->getAbsolutePath($path);

        if ($this->filesystem->exists($absolutePath)) {
            $this->filesystem->remove($absolutePath);
        }

        $dirPath = dirname($absolutePath);

        $this->cleanEmptyDirs($dirPath);
    }

    /**
     * @return string
     */
    private function generateRandomString()
    {
        return base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }

    /**
     * @return string
     */
    private function generateRandomPath()
    {
        $regexp  = "/^(.{2})(.{2})(.+?)$/";
        $pattern = implode(DIRECTORY_SEPARATOR, ['$1', '$2', '$3']);
        $string  = $this->generateRandomString();

        return preg_replace($regexp, $pattern, $string);
    }

    /**
     * @return string
     */
    private function generateFreeRandomPath()
    {
        do {
            $path         = $this->generateRandomPath();
            $absolutePath = $this->getAbsolutePath($path);
        } while ($this->filesystem->exists($absolutePath));

        return $path;
    }

    /**
     * @param string $path
     */
    private function cleanEmptyDirs($path)
    {
        if ($this->filesystem->exists($path) && $this->isAccessiblePath($path)) {
            $finder = new Finder();

            if ($finder->files()->in($path)->count() === 0) {
                $this->filesystem->remove($path);

                $parentPath = dirname($path);

                $this->cleanEmptyDirs($parentPath);
            }
        }
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    private function isAccessiblePath($path)
    {
        $rootPath = $this->publicPath.$this->uploadPath;

        return $path !== $rootPath && strpos($path, $rootPath) === 0;
    }
}
