<?php

namespace Livery\Bundle\MediaBundle\Manager;

use Symfony\Component\Routing\Router;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;

use Livery\Bundle\EntityBundle\Entity\Media\Image;

/**
 * Class ImageManager
 */
class ImageManager
{
    /**
     * @var array
     */
    public static $supportedMimeTypes = [
        'image/gif',
        'image/jpeg',
        'image/pjpeg',
        'image/png',
    ];

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $uploadPath;

    /**
     * @var array
     */
    private $filters;

    /**
     * ImageManager constructor.
     *
     * @param CacheManager $cacheManager
     * @param Router       $router
     * @param string       $host
     * @param string       $uploadPath
     * @param array        $filters
     */
    public function __construct(CacheManager $cacheManager, Router $router, $host, $uploadPath, array $filters = [])
    {
        $this->cacheManager = $cacheManager;
        $this->router       = $router;
        $this->host         = $host;
        $this->uploadPath   = $uploadPath;
        $this->filters      = $filters;
    }

    /**
     * @param string $category
     *
     * @return array
     */
    public function getFiltersByCategory($category)
    {
        return array_key_exists($category, $this->filters) ? $this->filters[$category] : [];
    }

    /**
     * @param Image  $image
     * @param string $filter
     *
     * @return string
     */
    public function getThumbnailUrl(Image $image, $filter)
    {
        $fullPath = $image->getFullPath();
        $fullPath = "{$this->uploadPath}/{$fullPath}";

        $context = $this->router->getContext();
        $host    = $context->getHost();

        $context->setHost($this->host);

        $url = $this->cacheManager->getBrowserPath($fullPath, $filter);

        $context->setHost($host);

        return $url;
    }
}
