<?php

namespace Livery\Bundle\MediaBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('livery_media');

        $rootNode
            ->children()
                ->scalarNode('public_path')->isRequired()->end()
                ->scalarNode('upload_path')->isRequired()->end()
                ->scalarNode('host')->isRequired()->end()
                ->arrayNode('image')
                    ->children()
                        ->arrayNode('filters')
                            ->useAttributeAsKey('name')
                            ->prototype('array')
                                ->prototype('scalar')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
