<?php

namespace Livery\Bundle\MediaBundle\Model;

use Symfony\Component\HttpFoundation\File\File;

/**
 * Class RemoteFile
 */
class RemoteFile extends File
{
    /**
     * @var string
     */
    private $sourceUrl;

    /**
     * @var string
     */
    private $clientOriginalName;

    /**
     * @var string
     */
    private $clientOriginalExtension;

    /**
     * @return string
     */
    public function getSourceUrl()
    {
        return $this->sourceUrl;
    }

    /**
     * @param string $sourceUrl
     *
     * @return RemoteFile
     */
    public function setSourceUrl($sourceUrl)
    {
        $this->sourceUrl               = $sourceUrl;
        $this->clientOriginalName      = basename($this->sourceUrl);
        $this->clientOriginalExtension = pathinfo($this->clientOriginalName, PATHINFO_EXTENSION);

        return $this;
    }

    /**
     * @return string
     */
    public function getClientOriginalName()
    {
        return $this->clientOriginalName;
    }

    /**
     * @return string
     */
    public function getClientOriginalExtension()
    {
        return $this->clientOriginalExtension;
    }
}
