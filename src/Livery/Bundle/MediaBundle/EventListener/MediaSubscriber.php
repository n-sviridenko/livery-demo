<?php

namespace Livery\Bundle\MediaBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Livery\Bundle\MediaBundle\Manager\UploadManager;
use Livery\Bundle\EntityBundle\Entity\Media\AbstractMedia;

/**
 * Class MediaSubscriber
 */
class MediaSubscriber implements EventSubscriber
{
    /**
     * @var UploadManager
     */
    private $uploadManager;

    /**
     * MediaSubscriber constructor.
     *
     * @param UploadManager $uploadManager
     */
    public function __construct(UploadManager $uploadManager)
    {
        $this->uploadManager = $uploadManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            'postRemove',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof AbstractMedia) {
            $path = $entity->getFullPath();

            if ($path) {
                $this->uploadManager->removeFile($path);
            }

            // @todo: remove all thumbnails if it's an image
        }
    }
}
