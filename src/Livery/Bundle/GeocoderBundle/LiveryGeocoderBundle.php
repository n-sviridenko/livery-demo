<?php

namespace Livery\Bundle\GeocoderBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class LiveryGeocoderBundle
 */
class LiveryGeocoderBundle extends Bundle
{
}
