<?php

namespace Livery\Bundle\GeocoderBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class GeocodeCommand
 */
class GeocodeCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('livery:geocoder:geocode')
            ->setDescription('Geocode an address or a ip address')
            ->addArgument('query', InputArgument::REQUIRED, 'The query')
            ->addOption('type', 't', InputOption::VALUE_OPTIONAL, 'The type: address, latlng or place_id', 'address')
            ->setHelp(<<<HELP
The <info>livery:geocoder:geocoder</info> command will fetch the latitude
and longitude from the given address.

<info>php app/console livery:geocoder:geocode "Eiffel Tower"</info>
HELP
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $geocoder = $this->getContainer()->get('livery.geocoder.factory')->createBuilder();

        $type  = $input->getOption('type');
        $query = $input->getArgument('query');

        switch ($type) {
            case 'address':
                $geocoder->setAddress($query);
                break;
            case 'latlng':
                list($lat, $lng) = explode(',', $query);

                $geocoder->setLatitudeLongitude($lat, $lng);
                break;
            case 'place_id':
                $geocoder->setPlaceId($query);
                break;
            default:
                $output->writeln(sprintf(
                    '<error>%s</error>',
                    'The type must be one of following: address, latlng or place_id'
                ));
        }

        $results = $geocoder->geocode();
        $data    = $results->first()->toArray();

        $max = 0;

        foreach ($data as $key => $value) {
            $length = strlen($key);
            if ($max < $length) {
                $max = $length;
            }
        }

        $max += 2;

        foreach ($data as $key => $value) {
            $key = $this->humanize($key);

            $output->writeln(sprintf(
                '<comment>%s</comment>: %s',
                str_pad($key, $max, ' ', STR_PAD_RIGHT),
                is_array($value) ? json_encode($value) : $value
            ));
        }
    }

    /**
     * @param string $text
     *
     * @return string
     */
    private function humanize($text)
    {
        $text = preg_replace('/([A-Z][a-z]+)|([A-Z][A-Z]+)|([^A-Za-z ]+)/', ' \1', $text);

        return ucfirst(strtolower($text));
    }
}
