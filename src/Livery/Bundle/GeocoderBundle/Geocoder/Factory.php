<?php

namespace Livery\Bundle\GeocoderBundle\Geocoder;

use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use Http\Discovery\MessageFactoryDiscovery;

use Livery\Bundle\GeocoderBundle\Model\AddressFactory;

/**
 * Class Factory
 */
class Factory
{
    /**
     * @var HttpClient
     */
    private $client;

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var AddressFactory
     */
    private $addressFactory;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var bool
     */
    private $https;

    /**
     * Factory constructor.
     *
     * @param HttpClient     $client
     * @param AddressFactory $addressFactory
     * @param string|null    $apiKey
     * @param bool           $https
     */
    public function __construct(HttpClient $client, AddressFactory $addressFactory, $apiKey = null, $https = false)
    {
        $this->client         = $client;
        $this->messageFactory = MessageFactoryDiscovery::find();
        $this->addressFactory = $addressFactory;
        $this->apiKey         = $apiKey;
        $this->https          = $https;
    }

    /**
     * Returns the HTTP adapter.
     *
     * @return HttpClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return MessageFactory
     */
    public function getMessageFactory()
    {
        return $this->messageFactory;
    }

    /**
     * @return AddressFactory
     */
    public function getAddressFactory()
    {
        return $this->addressFactory;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @return bool
     */
    public function getHttps()
    {
        return $this->https;
    }

    /**
     * @return Builder
     */
    public function createBuilder()
    {
        return new Builder($this);
    }
}
