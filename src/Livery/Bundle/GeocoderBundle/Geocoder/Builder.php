<?php

namespace Livery\Bundle\GeocoderBundle\Geocoder;

use Livery\Bundle\GeocoderBundle\Model\AddressCollection;
use Livery\Bundle\GeocoderBundle\Exception\NoResultException;
use Livery\Bundle\GeocoderBundle\Exception\QuotaExceededException;
use Livery\Bundle\GeocoderBundle\Exception\InvalidCredentialsException;

/**
 * Class Builder
 */
class Builder
{
    /**
     * API URL.
     */
    const API_URL = 'maps.googleapis.com/maps/api/geocode/json';

    /**
     * Address to geocode.
     *
     * @var string
     */
    private $address;

    /**
     * Latitude to reverse geocode to the closest address.
     *
     * @var float|string
     */
    private $latitude;

    /**
     * Longitude to reverse geocode to the closest address.
     *
     * @var float|string
     */
    private $longitude;

    /**
     * Place id
     *
     * @var string
     */
    private $placeId;

    /**
     * Southwest latitude of the bounding box within which to bias geocode
     * results.
     *
     * @var float|string
     */
    private $boundsSouthwestLatitude;

    /**
     * Southwest longitude of the bounding box within which to bias geocode
     * results.
     *
     * @var float|string
     */
    private $boundsSouthwestLongitude;

    /**
     * Northeast latitude of the bounding box within which to bias geocode
     * results.
     *
     * @var float|string
     */
    private $boundsNortheastLatitude;

    /**
     * Northeast longitude of the bounding box within which to bias geocode
     * results.
     *
     * @var float|string
     */
    private $boundsNortheastLongitude;

    /**
     * Two-character, top-level domain (ccTLD) within which to bias geocode
     * results.
     *
     * @var string
     */
    private $region;

    /**
     * Language code in which to return results.
     *
     * @var string
     */
    private $language;

    /**
     * Address type(s) to restrict results to.
     *
     * @var array
     */
    private $resultType = [];

    /**
     * Location type(s) to restrict results to.
     *
     * @var array
     */
    private $locationType = [];

    /**
     * Components to restrict results to.
     *
     * @var array
     */
    private $components = [];

    /**
     * Client ID for Business clients.
     *
     * @var string
     */
    private $clientId;

    /**
     * Cryptographic signing key for Business clients.
     *
     * @var string
     */
    private $signingKey;

    /**
     * @var Factory
     */
    private $factory;

    /**
     * Constructor. The request is not executed until `geocode()` is called.
     *
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return Factory
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * Set the address to geocode.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#geocoding
     * @param  string $address address to geocode
     * @return Builder
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the address to geocode.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#geocoding
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the latitude/longitude to reverse geocode to the closest address.
     *
     * @link  https://developers.google.com/maps/documentation/geocoding/intro#ReverseGeocoding
     * @param float|string|array $latitude  latitude to reverse geocode
     * @param float|string|null  $longitude longitude to reverse geocode
     *
     * @return Builder
     */
    public function setLatitudeLongitude($latitude, $longitude = null)
    {
        if (is_array($latitude)) {
            list($latitude, $longitude) = $latitude;
        }

        $this
            ->setLatitude($latitude)
            ->setLongitude($longitude)
        ;

        return $this;
    }

    /**
     * Get the latitude/longitude to reverse geocode to the closest address
     * in comma-separated format.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#ReverseGeocoding
     * @return string|false comma-separated coordinates, or false if not set
     */
    public function getLatitudeLongitude()
    {
        $latitude  = $this->getLatitude();
        $longitude = $this->getLongitude();

        if ($latitude && $longitude) {
            return "{$latitude},{$longitude}";
        } else {
            return false;
        }
    }

    /**
     * Set the latitude to reverse geocode to the closest address.
     *
     * @link  https://developers.google.com/maps/documentation/geocoding/intro#ReverseGeocoding
     * @param float|string $latitude latitude to reverse geocode
     *
     * @return Builder
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get the latitude to reverse geocode to the closest address.
     *
     * @link https://developers.google.com/maps/documentation/geocoding/intro#ReverseGeocoding
     *
     * @return float|string latitude to reverse geocode
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set the longitude to reverse geocode to the closest address.
     *
     * @link  https://developers.google.com/maps/documentation/geocoding/intro#ReverseGeocoding
     * @param float|string $longitude longitude to reverse geocode
     *
     * @return Builder
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get the longitude to reverse geocode to the closest address.
     *
     * @link https://developers.google.com/maps/documentation/geocoding/intro#ReverseGeocoding
     *
     * @return float|string longitude to reverse geocode
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $placeId
     *
     * @return $this
     */
    public function setPlaceId($placeId)
    {
        $this->placeId = $placeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceId()
    {
        return $this->placeId;
    }

    /**
     * Set the bounding box coordinates within which to bias geocode results.
     *
     * @link https://developers.google.com/maps/documentation/geocoding/intro#Viewports
     *
     * @param float|string $southwestLatitude  southwest latitude boundary
     * @param float|string $southwestLongitude southwest longitude boundary
     * @param float|string $northeastLatitude  northeast latitude boundary
     * @param float|string $northeastLongitude northeast longitude boundary
     *
     * @return Builder
     */
    public function setBounds($southwestLatitude, $southwestLongitude, $northeastLatitude, $northeastLongitude)
    {
        $this->setBoundsSouthwest($southwestLatitude, $southwestLongitude)
            ->setBoundsNortheast($northeastLatitude, $northeastLongitude);

        return $this;
    }

    /**
     * Get the bounding box coordinates within which to bias geocode results
     * in comma-separated, pipe-delimited format.
     *
     * @link https://developers.google.com/maps/documentation/geocoding/intro#Viewports
     *
     * @return string|false comma-separated, pipe-delimited coordinates, or
     *                      false if not set
     */
    public function getBounds()
    {
        $southwest = $this->getBoundsSouthwest();
        $northeast = $this->getBoundsNortheast();

        if ($southwest && $northeast) {
            return "{$southwest}|{$northeast}";
        } else {
            return false;
        }
    }

    /**
     * Set the southwest coordinates of the bounding box within which to bias
     * geocode results.
     *
     * @link  https://developers.google.com/maps/documentation/geocoding/intro#Viewports
     * @param float|string $latitude  southwest latitude boundary
     * @param float|string $longitude southwest longitude boundary
     *
     * @return Builder
     */
    public function setBoundsSouthwest($latitude, $longitude)
    {
        $this->boundsSouthwestLatitude  = $latitude;
        $this->boundsSouthwestLongitude = $longitude;

        return $this;
    }

    /**
     * Get the southwest coordinates of the bounding box within which to bias
     * geocode results in comma-separated format.
     *
     * @link https://developers.google.com/maps/documentation/geocoding/intro#Viewports
     *
     * @return string|false comma-separated coordinates, or false if not set
     */
    public function getBoundsSouthwest()
    {
        $latitude  = $this->getBoundsSouthwestLatitude();
        $longitude = $this->getBoundsSouthwestLongitude();

        if ($latitude && $longitude) {
            return "{$latitude},{$longitude}";
        } else {
            return false;
        }
    }

    /**
     * Get the southwest latitude of the bounding box within which to bias
     * geocode results.
     *
     * @link https://developers.google.com/maps/documentation/geocoding/intro#Viewports
     *
     * @return float|string southwest latitude boundary
     */
    public function getBoundsSouthwestLatitude()
    {
        return $this->boundsSouthwestLatitude;
    }

    /**
     * Get the southwest longitude of the bounding box within which to bias
     * geocode results.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#Viewports
     * @return float|string southwest longitude boundary
     */
    public function getBoundsSouthwestLongitude()
    {
        return $this->boundsSouthwestLongitude;
    }

    /**
     * Set the northeast coordinates of the bounding box within which to bias
     * geocode results.
     *
     * @link  https://developers.google.com/maps/documentation/geocoding/intro#Viewports
     * @param float|string $latitude  northeast latitude boundary
     * @param float|string $longitude northeast longitude boundary
     *
     * @return Builder
     */
    public function setBoundsNortheast($latitude, $longitude)
    {
        $this->boundsNortheastLatitude  = $latitude;
        $this->boundsNortheastLongitude = $longitude;

        return $this;
    }

    /**
     * Get the northeast coordinates of the bounding box within which to bias
     * geocode results in comma-separated format.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#Viewports
     *
     * @return string|false comma-separated coordinates, or false if not set
     */
    public function getBoundsNortheast()
    {
        $latitude  = $this->getBoundsNortheastLatitude();
        $longitude = $this->getBoundsNortheastLongitude();

        if ($latitude && $longitude) {
            return "{$latitude},{$longitude}";
        } else {
            return false;
        }
    }

    /**
     * Get the northeast latitude of the bounding box within which to bias
     * geocode results.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#Viewports
     *
     * @return float|string northeast latitude boundary
     */
    public function getBoundsNortheastLatitude()
    {
        return $this->boundsNortheastLatitude;
    }

    /**
     * Get the northeast longitude of the bounding box within which to bias
     * geocode results.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#Viewports
     *
     * @return float|string northeast longitude boundary
     */
    public function getBoundsNortheastLongitude()
    {
        return $this->boundsNortheastLongitude;
    }

    /**
     * Set the two-character, top-level domain (ccTLD) within which to bias
     * geocode results.
     *
     * @link  https://developers.google.com/maps/documentation/geocoding/intro#RegionCodes
     * @param string $region two-character, top-level domain (ccTLD)
     *
     * @return Builder
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get the two-character, top-level domain (ccTLD) within which to bias
     * geocode results.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#RegionCodes
     *
     * @return string two-character, top-level domain (ccTLD)
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set the language code in which to return results.
     *
     * @link  https://developers.google.com/maps/faq#languagesupport
     * @param string $language language code
     *
     * @return Builder
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get the language code in which to return results.
     *
     * @link   https://developers.google.com/maps/faq#languagesupport
     *
     * @return string language code
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set the address type(s) to restrict results to.
     *
     * @link  https://developers.google.com/maps/documentation/geocoding/intro#reverse-restricted
     * @param string|array $resultType address type(s)
     *
     * @return Builder
     */
    public function setResultType($resultType)
    {
        $this->resultType = is_array($resultType) ? $resultType : [$resultType];

        return $this;
    }

    /**
     * Get the address type(s) to restrict results to.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#reverse-restricted
     *
     * @return array address type(s)
     */
    public function getResultType()
    {
        return $this->resultType;
    }

    /**
     * Get the address type(s) to restrict results to separated by a pipe (|).
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#reverse-restricted
     *
     * @return string address type(s) separated by a pipe (|)
     */
    public function getResultTypeFormatted()
    {
        return implode('|', $this->getResultType());
    }

    /**
     * Set the location type(s) to restrict results to.
     *
     * @link  https://developers.google.com/maps/documentation/geocoding/intro#reverse-restricted
     * @param string|array $locationType location type(s)
     *
     * @return Builder
     */
    public function setLocationType($locationType)
    {
        $this->locationType = is_array($locationType) ? $locationType : [$locationType];

        return $this;
    }

    /**
     * Get the location type(s) to restrict results to.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#reverse-restricted
     *
     * @return array location type(s)
     */
    public function getLocationType()
    {
        return $this->locationType;
    }

    /**
     * Get the location type(s) to restrict results to separated by a pipe (|).
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#reverse-restricted
     *
     * @return string location type(s) separated by a pipe (|)
     */
    public function getLocationTypeFormatted()
    {
        return implode('|', $this->getLocationType());
    }

    /**
     * @param array $components
     *
     * @return Builder
     */
    public function setComponents(array $components)
    {
        $this->components = $components;

        return $this;
    }

    /**
     * @return array
     */
    public function getComponetns()
    {
        return $this->components;
    }

    /**
     * @return string
     */
    public function getComponentsFormatted()
    {
        $components = [];

        foreach ($this->getComponetns() as $key => $value) {
            $components[] = "{$key}:{$value}";
        }

        return implode('|', $components);
    }

    /**
     * Set the client ID for Business clients.
     *
     * @link  https://developers.google.com/maps/documentation/business/webservices/#client_id
     * @param string $clientId client ID
     *
     * @return Builder
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get the client ID for Business clients.
     *
     * @link   https://developers.google.com/maps/documentation/business/webservices/#client_id
     *
     * @return string client ID
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set the cryptographic signing key for Business clients.
     *
     * @link  https://developers.google.com/maps/documentation/business/webservices/#cryptographic_signing_key
     * @param string $signingKey cryptographic signing key
     *
     * @return Builder
     */
    public function setSigningKey($signingKey)
    {
        $this->signingKey = $signingKey;

        return $this;
    }

    /**
     * Get the cryptographic signing key for Business clients.
     *
     * @link   https://developers.google.com/maps/documentation/business/webservices/#cryptographic_signing_key
     *
     * @return string cryptographic signing key
     */
    public function getSigningKey()
    {
        return $this->signingKey;
    }

    /**
     * Whether the request is for a Business client.
     *
     * @return bool whether the request is for a Business client
     */
    public function isBusinessClient()
    {
        return $this->getClientId() && $this->getSigningKey();
    }

    /**
     * Execute the geocoding request. The return type is based on the requested
     * format: associative array if JSON, SimpleXMLElement object if XML.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#GeocodingResponses
     *
     * @throws InvalidCredentialsException
     * @throws NoResultException
     * @throws QuotaExceededException
     * @throws \Exception
     *
     * @return AddressCollection
     */
    public function geocode()
    {
        $query   = $this->geocodeUrl();
        $content = $this->getResponseContent($query);

        // Throw exception if invalid clientID and/or privateKey used with GoogleMapsBusinessProvider
        if (strpos($content, "Provided 'signature' is not valid for the provided client ID") !== false) {
            throw new InvalidCredentialsException(sprintf('Invalid client ID / API Key %s', $query));
        }

        if (empty($content)) {
            throw new NoResultException(sprintf('Could not execute query "%s".', $query));
        }

        $json = json_decode($content);

        // API error
        if (!isset($json)) {
            throw new NoResultException(sprintf('Could not execute query "%s".', $query));
        }

        if ('REQUEST_DENIED' === $json->status && 'The provided API key is invalid.' === $json->error_message) {
            throw new InvalidCredentialsException(sprintf('API key is invalid %s', $query));
        }

        if ('REQUEST_DENIED' === $json->status) {
            throw new \Exception(sprintf('API access denied. Request: %s - Message: %s',
                $query, $json->error_message));
        }

        // you are over your quota
        if ('OVER_QUERY_LIMIT' === $json->status) {
            throw new QuotaExceededException(sprintf('Daily quota exceeded %s', $query));
        }

        // no result
        if (!isset($json->results) || !count($json->results) || 'OK' !== $json->status) {
            throw new NoResultException(sprintf('Could not execute query "%s".', $query));
        }

        $results = [];
        foreach ($json->results as $result) {
            $resultSet = $this->getDefaults();

            // update address components
            foreach ($result->address_components as $component) {
                foreach ($component->types as $type) {
                    $this->updateAddressComponent($resultSet, $type, $component);
                }
            }

            // update coordinates
            $coordinates = $result->geometry->location;
            $resultSet['latitude']  = $coordinates->lat;
            $resultSet['longitude'] = $coordinates->lng;

            $resultSet['bounds'] = null;
            if (isset($result->geometry->bounds)) {
                $resultSet['bounds'] = array(
                    'south' => $result->geometry->bounds->southwest->lat,
                    'west'  => $result->geometry->bounds->southwest->lng,
                    'north' => $result->geometry->bounds->northeast->lat,
                    'east'  => $result->geometry->bounds->northeast->lng
                );
            } elseif ('ROOFTOP' === $result->geometry->location_type) {
                // Fake bounds
                $resultSet['bounds'] = array(
                    'south' => $coordinates->lat,
                    'west'  => $coordinates->lng,
                    'north' => $coordinates->lat,
                    'east'  => $coordinates->lng
                );
            }

            $resultSet['formattedAddress'] = $result->formatted_address;
            $resultSet['placeId']          = $result->place_id;
            $resultSet['types']            = $result->types;

            $results[] = array_merge($this->getDefaults(), $resultSet);
        }

        return $this->getFactory()->getAddressFactory()->createFromArray($results);
    }

    /**
     * @param string $query
     *
     * @return string
     */
    protected function getResponseContent($query)
    {
        $request = $this->getFactory()->getMessageFactory()->createRequest('GET', $query);
        $content = $this->getFactory()->getClient()->sendRequest($request)->getBody();

        return (string) $content;
    }

    /**
     * Generate the signature for a Business client geocode request.
     *
     * @link  https://developers.google.com/maps/documentation/business/webservices/auth#digital_signatures
     * @param string $pathQueryString path and query string of the request
     *
     * @return string Base64 encoded signature that's URL safe
     */
    private function generateSignature($pathQueryString)
    {
        $decodedSigningKey = self::base64DecodeUrlSafe($this->getSigningKey());

        $signature = hash_hmac('sha1', $pathQueryString, $decodedSigningKey, true);
        $signature = self::base64EncodeUrlSafe($signature);

        return $signature;
    }

    /**
     * Build the query string with all set parameters of the geocode request.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#GeocodingRequests
     *
     * @return string encoded query string of the geocode request
     */
    private function geocodeQueryString()
    {
        $queryString = [];

        // One of the following is required.
        $address           = $this->getAddress();
        $latitudeLongitude = $this->getLatitudeLongitude();
        $placeId           = $this->getPlaceId();

        // If both are set for some reason, favor address to geocode.
        if ($address) {
            $queryString['address'] = $address;
        } elseif ($latitudeLongitude) {
            $queryString['latlng'] = $latitudeLongitude;
        } elseif ($placeId) {
            $queryString['place_id'] = $placeId;
        }

        // Optional parameters.
        $queryString['bounds'] = $this->getBounds();
        $queryString['region'] = $this->getRegion();
        $queryString['language'] = $this->getLanguage();
        $queryString['result_type'] = $this->getResultTypeFormatted();
        $queryString['location_type'] = $this->getLocationTypeFormatted();
        $queryString['components'] = $this->getComponentsFormatted();

        // Remove any unset parameters.
        $queryString = array_filter($queryString);

        // The signature is added later using the path + query string.
        if ($this->isBusinessClient()) {
            $queryString['client'] = $this->getClientId();
        } elseif ($this->getFactory()->getApiKey()) {
            $queryString['key'] = $this->getFactory()->getApiKey();
        }

        // Convert array to proper query string.
        return http_build_query($queryString);
    }

    /**
     * Build the URL (with query string) of the geocode request.
     *
     * @link   https://developers.google.com/maps/documentation/geocoding/intro#GeocodingRequests
     *
     * @return string URL of the geocode request
     */
    private function geocodeUrl()
    {
        // HTTPS is required if an API key is set.
        if ($this->getFactory()->getHttps()) {
            $scheme = "https";
        }
        else {
            $scheme = "http";
        }

        $pathQueryString = '?' . $this->geocodeQueryString();

        if ($this->isBusinessClient()) {
            $pathQueryString .= "&signature=" . $this->generateSignature($pathQueryString);
        }

        return $scheme . "://" . self::API_URL . $pathQueryString;
    }

    /**
     * Update current resultSet with given key/value.
     *
     * @param array  $resultSet resultSet to update
     * @param string $type      Component type
     * @param object $values    The component values
     *
     * @return array
     */
    private function updateAddressComponent(&$resultSet, $type, $values)
    {
        switch ($type) {
            case 'postal_code':
                $resultSet['postalCode'] = $values->long_name;
                break;

            case 'locality':
            case 'postal_town':
                $resultSet['locality'] = $values->long_name;
                break;

            case 'administrative_area_level_1':
            case 'administrative_area_level_2':
            case 'administrative_area_level_3':
            case 'administrative_area_level_4':
            case 'administrative_area_level_5':
                $resultSet['adminLevels'][]= [
                    'name' => $values->long_name,
                    'code' => $values->short_name,
                    'level' => intval(substr($type, -1))
                ];
                break;

            case 'country':
                $resultSet['country'] = $values->long_name;
                $resultSet['countryCode'] = $values->short_name;
                break;

            case 'street_number':
                $resultSet['streetNumber'] = $values->long_name;
                break;

            case 'route':
                $resultSet['streetName'] = $values->long_name;
                break;

            case 'sublocality':
                $resultSet['subLocality'] = $values->long_name;
                break;

            default:
        }

        return $resultSet;
    }

    /**
     * Returns the default results.
     *
     * @return array
     */
    private function getDefaults()
    {
        return [
            'latitude'         => null,
            'longitude'        => null,
            'bounds'           => [
                'south' => null,
                'west'  => null,
                'north' => null,
                'east'  => null,
            ],
            'streetNumber'     => null,
            'streetName'       => null,
            'locality'         => null,
            'postalCode'       => null,
            'subLocality'      => null,
            'adminLevels'      => [],
            'country'          => null,
            'countryCode'      => null,
            'timezone'         => null,
            'formattedAddress' => null,
        ];
    }

    /**
     * Encode a string with Base64 using only URL safe characters.
     *
     * @param string $value value to encode
     *
     * @return string encoded value
     */
    private static function base64EncodeUrlSafe($value)
    {
        return strtr(base64_encode($value), '+/', '-_');
    }

    /**
     * Decode a Base64 string that uses only URL safe characters.
     *
     * @param string $value value to decode
     *
     * @return string decoded value
     */
    private static function base64DecodeUrlSafe($value)
    {
        return base64_decode(strtr($value, '-_', '+/'));
    }
}
