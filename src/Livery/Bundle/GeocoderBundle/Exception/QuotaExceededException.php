<?php

namespace Livery\Bundle\GeocoderBundle\Exception;

/**
 * Class QuotaExceededException
 */
class QuotaExceededException extends \RuntimeException implements ExceptionInterface
{
}
