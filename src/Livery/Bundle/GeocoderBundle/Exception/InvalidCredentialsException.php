<?php

namespace Livery\Bundle\GeocoderBundle\Exception;

/**
 * Class InvalidCredentialsException
 */
class InvalidCredentialsException extends \RuntimeException implements ExceptionInterface
{
}
