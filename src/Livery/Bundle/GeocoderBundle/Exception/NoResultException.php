<?php

namespace Livery\Bundle\GeocoderBundle\Exception;

/**
 * Class NoResultException
 */
class NoResultException extends \RuntimeException implements ExceptionInterface
{
}
