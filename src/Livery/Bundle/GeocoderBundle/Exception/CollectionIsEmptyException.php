<?php

namespace Livery\Bundle\GeocoderBundle\Exception;

/**
 * Class CollectionIsEmptyException
 */
class CollectionIsEmptyException extends \RuntimeException implements ExceptionInterface
{
}
