<?php

namespace Livery\Bundle\GeocoderBundle\Model;

use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class AddressFactory
 */
final class AddressFactory
{
    /**
     * @param  array $results
     * @return AddressCollection
     */
    public function createFromArray(array $results)
    {
        $addresses = [];
        foreach ($results as $result) {
            $adminLevels = [];
            foreach ($this->readArrayValue($result, '[adminLevels]') as $adminLevel) {
                $adminLevels[] = new AdminLevel(
                    intval($this->readValueOrNull($adminLevel, '[level]')),
                    $this->readValueOrNull($adminLevel, '[name]'),
                    $this->readValueOrNull($adminLevel, '[code]')
                );
            }

            $addresses[] = new Address(
                $this->createCoordinates(
                    $this->readMixedValue($result, '[latitude]'),
                    $this->readMixedValue($result, '[longitude]')
                ),
                new Bounds(
                    $this->readMixedValue($result, '[bounds][south]'),
                    $this->readMixedValue($result, '[bounds][west]'),
                    $this->readMixedValue($result, '[bounds][north]'),
                    $this->readMixedValue($result, '[bounds][east]')
                ),
                $this->readValueOrNull($result, '[streetNumber]'),
                $this->readValueOrNull($result, '[streetName]'),
                $this->readValueOrNull($result, '[postalCode]'),
                $this->readValueOrNull($result, '[locality]'),
                $this->readValueOrNull($result, '[subLocality]'),
                new AdminLevelCollection($adminLevels),
                new Country(
                    $this->readValueOrNull($result, '[country]'),
                    $this->upperize($this->readMixedValue($result, '[countryCode]'))
                ),
                $this->readValueOrNull($result, '[timezone]'),
                $this->readValueOrNull($result, '[formattedAddress]'),
                $this->readValueOrNull($result, '[placeId]'),
                $this->readArrayValue($result, '[types]')
            );
        }

        return new AddressCollection($addresses);
    }

    /**
     * @param  array  $data
     * @param  string $key
     * @return double
     */
    private function readMixedValue(array $data, $key)
    {
        $value = null;

        try {
            $value = PropertyAccess::createPropertyAccessor()->getValue($data, $key);
        } catch (\Exception $e) {
        }

        return $value;
    }

    /**
     * @param  array  $data
     * @param  string $key
     * @return string
     */
    private function readValueOrNull(array $data, $key)
    {
        return $this->valueOrNull($this->readMixedValue($data, $key));
    }

    /**
     * @param  array  $data
     * @param  string $key
     * @return array
     */
    private function readArrayValue(array $data, $key)
    {
        return $this->readMixedValue($data, $key) ?: [];
    }

    /**
     * @return string|null
     */
    private function valueOrNull($str)
    {
        return empty($str) ? null : $str;
    }

    /**
     * @return string|null
     */
    private function upperize($str)
    {
        if (null !== $str = $this->valueOrNull($str)) {
            return extension_loaded('mbstring') ? mb_strtoupper($str, 'UTF-8') : strtoupper($str);
        }

        return null;
    }

    /**
     * @param double $latitude
     * @param double $longitude
     * @return Coordinates
     */
    private function createCoordinates($latitude, $longitude)
    {
        if (null === $latitude || null === $longitude) {
            return null;
        }

        return new Coordinates((double) $latitude, (double) $longitude);
    }
}
