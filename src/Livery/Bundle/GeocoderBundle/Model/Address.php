<?php

namespace Livery\Bundle\GeocoderBundle\Model;

/**
 * Class Address
 */
final class Address
{
    /**
     * @var Coordinates
     */
    private $coordinates;

    /**
     * @var Bounds
     */
    private $bounds;

    /**
     * @var string|int
     */
    private $streetNumber;

    /**
     * @var string
     */
    private $streetName;

    /**
     * @var string
     */
    private $subLocality;

    /**
     * @var string
     */
    private $locality;

    /**
     * @var string
     */
    private $postalCode;

    /**
     * @var AdminLevelCollection
     */
    private $adminLevels;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $formattedAddress;

    /**
     * @var string
     */
    private $placeId;

    /**
     * @var array
     */
    private $types;

    /**
     * @param Coordinates|null          $coordinates
     * @param Bounds|null               $bounds
     * @param string|null               $streetNumber
     * @param string|null               $streetName
     * @param string|null               $postalCode
     * @param string|null               $locality
     * @param string|null               $subLocality
     * @param AdminLevelCollection|null $adminLevels
     * @param Country|null              $country
     * @param string|null               $timezone
     * @param string|null               $formattedAddress
     * @param string|null               $placeId
     * @param array                     $types
     */
    public function __construct(
        Coordinates $coordinates = null,
        Bounds $bounds = null,
        $streetNumber = null,
        $streetName = null,
        $postalCode = null,
        $locality = null,
        $subLocality = null,
        AdminLevelCollection $adminLevels = null,
        Country $country = null,
        $timezone = null,
        $formattedAddress = null,
        $placeId = null,
        array $types = []
    ) {
        $this->coordinates      = $coordinates;
        $this->bounds           = $bounds;
        $this->streetNumber     = $streetNumber;
        $this->streetName       = $streetName;
        $this->postalCode       = $postalCode;
        $this->locality         = $locality;
        $this->subLocality      = $subLocality;
        $this->adminLevels      = $adminLevels ?: new AdminLevelCollection();
        $this->country          = $country;
        $this->timezone         = $timezone;
        $this->formattedAddress = $formattedAddress;
        $this->placeId          = $placeId;
        $this->types            = $types;
    }

    /**
     * Returns an array of coordinates (latitude, longitude).
     *
     * @return Coordinates
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Returns the latitude value.
     *
     * @return double
     */
    public function getLatitude()
    {
        if (null === $this->coordinates) {
            return null;
        }

        return $this->coordinates->getLatitude();
    }

    /**
     * Returns the longitude value.
     *
     * @return double
     */
    public function getLongitude()
    {
        if (null === $this->coordinates) {
            return null;
        }

        return $this->coordinates->getLongitude();
    }

    /**
     * Returns the bounds value.
     *
     * @return Bounds
     */
    public function getBounds()
    {
        return $this->bounds;
    }

    /**
     * Returns the street number value.
     *
     * @return string|int
     */
    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

    /**
     * Returns the street name value.
     *
     * @return string
     */
    public function getStreetName()
    {
        return $this->streetName;
    }

    /**
     * Returns the city or locality value.
     *
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Returns the postal code or zipcode value.
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Returns the locality district, or
     * sublocality, or neighborhood.
     *
     * @return string
     */
    public function getSubLocality()
    {
        return $this->subLocality;
    }

    /**
     * Returns the administrative levels.
     *
     * @return AdminLevelCollection
     */
    public function getAdminLevels()
    {
        return $this->adminLevels;
    }

    /**
     * Returns the country value.
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Returns the country ISO code.
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->country->getCode();
    }

    /**
     * Returns the timezone.
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Returns the formatted address.
     *
     * @return string
     */
    public function getFormattedAddress()
    {
        return $this->formattedAddress;
    }

    /**
     * Returns the place id.
     *
     * @return string
     */
    public function getPlaceId()
    {
        return $this->placeId;
    }

    /**
     * Returns the types.
     *
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function hasType($type)
    {
        return in_array($type, $this->types);
    }

    /**
     * @param array $types
     *
     * @return bool
     */
    public function hasOneOfTypes(array $types)
    {
        return count(array_intersect($types, $this->types)) > 0;
    }

    /**
     * Returns an array with data indexed by name.
     *
     * @return array
     */
    public function toArray()
    {
        $adminLevels = [];
        foreach ($this->adminLevels as $adminLevel) {
            $adminLevels[$adminLevel->getLevel()] = [
                'name'  => $adminLevel->getName(),
                'code'  => $adminLevel->getCode(),
            ];
        }

        return array(
            'latitude'         => $this->getLatitude(),
            'longitude'        => $this->getLongitude(),
            'bounds'           => $this->bounds->toArray(),
            'streetNumber'     => $this->streetNumber,
            'streetName'       => $this->streetName,
            'postalCode'       => $this->postalCode,
            'locality'         => $this->locality,
            'subLocality'      => $this->subLocality,
            'adminLevels'      => $adminLevels,
            'country'          => $this->country->getName(),
            'countryCode'      => $this->country->getCode(),
            'timezone'         => $this->timezone,
            'formattedAddress' => $this->formattedAddress,
            'placeId'          => $this->placeId,
            'types'            => $this->types,
        );
    }
}
