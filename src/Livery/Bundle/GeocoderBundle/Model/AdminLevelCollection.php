<?php

namespace Livery\Bundle\GeocoderBundle\Model;

use Livery\Bundle\GeocoderBundle\Exception\InvalidArgumentException;

/**
 * Class AdminLevelCollection
 */
final class AdminLevelCollection implements \IteratorAggregate, \Countable
{
    const MAX_LEVEL_DEPTH = 5;

    /**
     * @var AdminLevel[]
     */
    private $adminLevels;

    /**
     * AdminLevelCollection constructor.
     *
     * @param array $adminLevels
     */
    public function __construct(array $adminLevels = [])
    {
        $this->adminLevels = [];

        foreach ($adminLevels as $adminLevel) {
            $level = $adminLevel->getLevel();

            $this->checkLevel($level);

            if ($this->has($level)) {
                 throw new InvalidArgumentException(sprintf("Administrative level %d is defined twice", $level));
            }

            $this->adminLevels[$level] = $adminLevel;
        }

        ksort($this->adminLevels, SORT_NUMERIC);
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->all());
    }

    /**
     * {@inheritDoc}
     */
    public function count()
    {
        return count($this->adminLevels);
    }

    /**
     * @return AdminLevel|null
     */
    public function first()
    {
        if (empty($this->adminLevels)) {
            return null;
        }

        return reset($this->adminLevels);
    }

    /**
     * @param int      $offset
     * @param int|null $length
     *
     * @return AdminLevel[]
     */
    public function slice($offset, $length = null)
    {
        return array_slice($this->adminLevels, $offset, $length, true);
    }

    /**
     * @param int $level
     *
     * @return bool
     */
    public function has($level)
    {
        return isset($this->adminLevels[$level]);
    }

    /**
     * @param int $offset
     *
     * @return AdminLevel[]
     */
    public function getLevelsLessThan($offset)
    {
        $levels = [];

        for ($i = $offset; $i >= 1; $i --) {
            if ($this->has($i)) {
                $levels[] = $this->get($i);
            }
        }

        return $levels;
    }

    /**
     * @param int $offset
     *
     * @return AdminLevel|null
     */
    public function getFirstLevelLessThan($offset)
    {
        for ($i = $offset; $i >= 1; $i --) {
            if ($this->has($i)) {
                return $this->get($i);
            }
        }

        return null;
    }

    /**
     * @param int $level
     *
     * @return AdminLevel
     *
     * @throws \OutOfBoundsException
     * @throws InvalidArgumentException
     */
    public function get($level)
    {
        $this->checkLevel($level);

        if (! isset($this->adminLevels[$level])) {
            throw new InvalidArgumentException(sprintf("Administrative level %d is not set for this address", $level));
        }

        return  $this->adminLevels[$level];
    }

    /**
     * @return AdminLevel[]
     */
    public function all()
    {
        return $this->adminLevels;
    }

    /**
     * @param  integer               $level
     * @throws \OutOfBoundsException
     */
    private function checkLevel($level)
    {
        if ($level <= 0 || $level > self::MAX_LEVEL_DEPTH) {
            throw new \OutOfBoundsException(sprintf(
                "Administrative level should be an integer in [1,%d], %d given",
                self::MAX_LEVEL_DEPTH,
                $level
            ));
        }
    }
}
