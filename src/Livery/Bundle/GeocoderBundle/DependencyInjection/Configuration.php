<?php

namespace Livery\Bundle\GeocoderBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('livery_geocoder');

        $rootNode
            ->children()
                ->scalarNode('api_key')->isRequired()->end()
                ->booleanNode('use_ssl')->defaultFalse()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
