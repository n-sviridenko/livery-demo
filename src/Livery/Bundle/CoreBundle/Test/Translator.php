<?php

namespace Livery\Bundle\CoreBundle\Test;

use Symfony\Component\Translation\MessageCatalogue;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Config\ConfigCacheFactoryInterface;
use Symfony\Component\Translation\TranslatorBagInterface;

/**
 * Created for tests
 */
class Translator implements TranslatorInterface, TranslatorBagInterface
{
    /**
     * {@inheritdoc}
     */
    public function trans($id, array $parameters = [], $domain = null, $locale = null)
    {
        if (count($parameters) > 0) {
            return $id.' '.json_encode($parameters);
        }

        return $id;
    }

    /**
     * {@inheritdoc}
     */
    public function transChoice($id, $number, array $parameters = [], $domain = null, $locale = null)
    {
        if (count($parameters)) {
            return $id.' '.$number.' '.json_encode($parameters);
        }

        return $id.' '.$number;
    }

    /**
     * {@inheritdoc}
     */
    public function setLocale($locale)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getLocale()
    {
        return 'test';
    }

    /**
     * {@inheritdoc}
     */
    public function setFallbackLocales($locale)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function addResource($resource)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getCatalogue($locale = null)
    {
        return new MessageCatalogue($locale);
    }

    /**
     * {@inheritdoc}
     */
    public function setConfigCacheFactory(ConfigCacheFactoryInterface $configCacheFactory)
    {
    }
}
