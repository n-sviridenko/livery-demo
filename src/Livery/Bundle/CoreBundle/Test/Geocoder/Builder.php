<?php

namespace Livery\Bundle\CoreBundle\Test\Geocoder;

use Livery\Bundle\GeocoderBundle\Geocoder\Builder as BaseBuilder;
use Symfony\Component\Finder\Finder;

/**
 * Class Builder
 */
class Builder extends BaseBuilder
{
    /**
     * {@inheritdoc}
     */
    protected function getResponseContent($query)
    {
        parse_str(parse_url($query, PHP_URL_QUERY), $params);

        $hash = array_diff_key($params, array_flip(['key', 'client']));
        $hash = http_build_query($hash);
        $hash = md5($hash);

        $path  = $this->getFactory()->getMocksPath();
        $path .= "{$hash}.json";

        if (file_exists($path)) {
            return file_get_contents($path);
        }

        $content = parent::getResponseContent($query);

        file_put_contents($path, $content);

        return $content;
    }
}
