<?php

namespace Livery\Bundle\CoreBundle\Test\Geocoder;

use Livery\Bundle\GeocoderBundle\Geocoder\Factory as BaseFactory;

/**
 * Class Factory
 */
class Factory extends BaseFactory
{
    /**
     * @var string
     */
    private $mocksPath;

    /**
     * @param string $mocksPath
     */
    public function setMocksPath($mocksPath)
    {
        $this->mocksPath = $mocksPath;
    }

    /**
     * @return string
     */
    public function getMocksPath()
    {
        return $this->mocksPath;
    }

    /**
     * @return Builder
     */
    public function createBuilder()
    {
        return new Builder($this);
    }
}
