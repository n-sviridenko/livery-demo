<?php

namespace Livery\Bundle\CoreBundle\Test\Normalizer;

use Livery\Bundle\EntityBundle\Entity\Location\Area;
use Livery\Bundle\ApiBundle\Normalizer\LocationNormalizer as BaseNormalizer;

/**
 * Class LocationNormalizer
 *
 * @todo: move into the api
 */
class LocationNormalizer extends BaseNormalizer
{
    /**
     * {@inheritdoc}
     */
    protected function normalizeArea(Area $object)
    {
        $return = parent::normalizeArea($object);

        /** @var Area|null $parent */
        $parent = $object->getParent();

        if ($parent) {
            $return += [
                'parent' => $this->normalizeArea($parent),
            ];
        }

        return $return;
    }
}
