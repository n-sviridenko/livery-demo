<?php

namespace Livery\Bundle\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class LocaleType
 */
class LocaleType extends AbstractType
{
    /**
     * @var array
     */
    private $supportedLocales;

    /**
     * LocaleType constructor.
     *
     * @param array $supportedLocales
     */
    public function __construct(array $supportedLocales)
    {
        $this->supportedLocales = $supportedLocales;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $supportedLocales = array_combine($this->supportedLocales, $this->supportedLocales);

        $resolver->setDefaults([
            'choices' => $supportedLocales,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return ChoiceType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'locale';
    }
}
