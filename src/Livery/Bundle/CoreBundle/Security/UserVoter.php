<?php

namespace Livery\Bundle\CoreBundle\Security;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

use Livery\Bundle\EntityBundle\Entity\User;

/**
 * Class UserVoter
 */
class UserVoter extends Voter
{
    const VIEW         = 'view';
    const VIEW_ALL     = 'viewAll';

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        $allowedAttributes = [
            self::VIEW,
            self::VIEW_ALL,
        ];

        if (!in_array($attribute, $allowedAttributes)) {
            return false;
        }

        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $currentUser = $token->getUser();

        /** @var User $user */
        $user = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($user, $currentUser);
            case self::VIEW_ALL:
                return $this->canViewAll($user, $currentUser);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param User  $user
     * @param mixed $currentUser
     *
     * @return bool
     */
    private function canView(User $user, $currentUser)
    {
        return $this->isCurrentUser($user, $currentUser) || $this->isUserAvailable($user);
    }

    /**
     * @param User  $user
     * @param mixed $currentUser
     *
     * @return bool
     */
    private function canViewAll(User $user, $currentUser)
    {
        return $this->isCurrentUser($user, $currentUser);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function isUserAvailable(User $user)
    {
        return (
            $user->isEnabled() &&
            $user->isAccountNonExpired() &&
            $user->isAccountNonLocked()
        );
    }

    /**
     * @param User  $user
     * @param mixed $currentUser
     *
     * @return bool
     */
    private function isCurrentUser(User $user, $currentUser)
    {
        if (!$currentUser instanceof User) {
            return false;
        }

        return $user->getId() === $currentUser->getId();
    }
}
