<?php

namespace Livery\Bundle\CoreBundle\Security;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

use Livery\Bundle\EntityBundle\Entity\Trip\Trip;

/**
 * Class TripVoter
 *
 * @todo: add tests
 */
class TripVoter extends Voter
{
    const VIEW = 'view';

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::VIEW])) {
            return false;
        }

        if (!$subject instanceof Trip) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        /** @var Trip $trip */
        $trip = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($trip);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param Trip $trip
     *
     * @return bool
     */
    private function canView(Trip $trip)
    {
        return $trip->isPublished();
    }
}
