<?php

namespace Livery\Bundle\CoreBundle\Measure;

/**
 * Class MeasureManager
 */
final class MeasureManager
{
    /**
     * @var string[]
     */
    private $supportedMassMeasures;

    /**
     * MeasureManager constructor.
     *
     * @param string[] $supportedMassMeasures
     */
    public function __construct(array $supportedMassMeasures)
    {
        $this->supportedMassMeasures = $supportedMassMeasures;
    }

    /**
     * @return string[]
     */
    public function getSupportedMassMeasures()
    {
        return $this->supportedMassMeasures;
    }

    /**
     * @param string $massMeasure
     *
     * @return bool
     */
    public function isMassMeasureSupported($massMeasure)
    {
        return in_array($massMeasure, $this->supportedMassMeasures);
    }
}
