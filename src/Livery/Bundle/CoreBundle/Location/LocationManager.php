<?php

namespace Livery\Bundle\CoreBundle\Location;

use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyAccess\PropertyAccess;

use Livery\Bundle\GeocoderBundle\Model\Address;
use Livery\Bundle\GeocoderBundle\Geocoder\Factory;
use Livery\Bundle\GeocoderBundle\Model\AdminLevel;
use Livery\Bundle\EntityBundle\Entity\Location\Area;
use Livery\Bundle\EntityBundle\Entity\Location\Country;
use Livery\Bundle\EntityBundle\Entity\Location\Location;
use Livery\Bundle\GeocoderBundle\Model\AdminLevelCollection;
use Livery\Bundle\EntityBundle\Entity\Location\AbstractLocation;

/**
 * @todo: split into LocationManager and AddressManager
 * @todo: think about a solution to rewrite relations when an area hierarchy in some country will be changed
 *
 * Class LocationManager
 */
class LocationManager
{
    /**
     * @var array
     */
    static private $localityTypes = [
        'locality',
        'postal_town',
    ];

    /**
     * @var array
     */
    static private $administrativeAreaTypes = [
        'administrative_area_level_1',
        'administrative_area_level_2',
        'administrative_area_level_3',
        'administrative_area_level_4',
        'administrative_area_level_5',
    ];

    /**
     * @var Factory
     */
    private $geocoder;

    /**
     * @var EntityManager
     *
     * @todo: use registry.getManager each time
     */
    private $entityManager;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var string
     */
    private $fallbackLocale;

    /**
     * LocationManager constructor.
     *
     * @param Factory           $geocoder
     * @param RegistryInterface $registry
     * @param RequestStack      $requestStack
     * @param string            $fallbackLocale
     */
    public function __construct(Factory $geocoder, RegistryInterface $registry, RequestStack $requestStack, $fallbackLocale)
    {
        $this->geocoder       = $geocoder;
        $this->entityManager  = $registry->getManager();
        $this->requestStack   = $requestStack;
        $this->fallbackLocale = $fallbackLocale;
    }

    /**
     * @param string     $placeId
     * @param array|null $types
     *
     * @return AbstractLocation|null
     */
    public function getLocationByPlaceId($placeId, array $types = null)
    {
        $location = $this->entityManager
            ->getRepository(AbstractLocation::class)
            ->findOneBy(['placeId' => $placeId])
        ;

        if (!$location) {
            $address = $this->findAddressByPlaceId($placeId, $this->getDefaultLocale());

            if (!$address) {
                return null;
            }

            if ($address->hasType('country')) {
                $location = $this->createCountry($address);
            } elseif ($address->hasOneOfTypes($this->getAreaTypes())) {
                $location = $this->createArea($address);
            } elseif ($address->getSubLocality() || $address->getStreetName()) {
                $location = $this->createLocation($address);
            }

            if ($location) {
                $this->entityManager->flush();
            }
        }

        if (!$location || $types && !in_array($location->getType(), $types)) {
            return null;
        }

        return $this->hydrateLocation($location);
    }

    /**
     * @param string     $countrySlug
     * @param null|null  $areaSlug
     * @param array|null $types
     *
     * @return Location|null
     */
    public function getLocationByAlias($countrySlug, $areaSlug = null, array $types = null)
    {
        if ($areaSlug) {
            $location = $this->entityManager
                ->getRepository(Area::class)
                ->findOneBySlug($areaSlug, $countrySlug)
            ;
        } else {
            $location = $this->entityManager
                ->getRepository(Country::class)
                ->findOneBy(['slug' => $countrySlug])
            ;
        }

        if (!$location || $types && !in_array($location->getType(), $types)) {
            return null;
        }

        return $this->hydrateLocation($location);
    }

    /**
     * @param AbstractLocation $location
     *
     * @return Location
     */
    public function hydrateLocation(AbstractLocation $location)
    {
        if (!$location->getTranslations()->containsKey($this->getLocale())) {
            $address = $this->findAddressByPlaceId($location->getPlaceId(), $this->getLocale());

            if ($address) {
                if ($location instanceof Country) {
                    $this->translateCountry($location, $address, false);
                } elseif ($location instanceof Area) {
                    $this->translateArea($location, $address, false);
                } elseif ($location instanceof Location) {
                    $this->translateLocation($location, $address, false);
                }

                $this->entityManager->persist($location);
                $this->entityManager->flush();
            }
        }

        return $location;
    }

    /**
     * @return string
     */
    private function getLocale()
    {
        $request = $this->requestStack->getCurrentRequest();

        return $request ? $request->getLocale() : $this->fallbackLocale;
    }

    /**
     * @return string
     */
    private function getDefaultLocale()
    {
        $request = $this->requestStack->getCurrentRequest();

        return $request ? $request->getDefaultLocale() : $this->fallbackLocale;
    }

    /**
     * @param Address $address
     *
     * @return Location
     */
    private function createLocation(Address $address)
    {
        $area = $this->findLocalityByChildAddress($address);

        if (!$area) {
            return null;
        }

        $location = new Location();
        $location->setArea($area);

        $this->fillLocation($location, $address);
        $this->translateLocation($location, $address, true, false);

        $this->entityManager->persist($location);

        return $location;
    }

    /**
     * @param Address $child
     *
     * @return Area|null
     * @throws \Exception
     */
    private function findLocalityByChildAddress(Address $child)
    {
        return $this->findAreaByChildAddress($child, 'locality');
    }

    /**
     * @param Address $child
     *
     * @return Area|null
     */
    private function findAdministrativeAreaByChildAddress(Address $child)
    {
        $parentLevel = $this->getAddressParentAdminLevel($child);

        if (!$parentLevel) {
            return null;
        }

        $parentType = sprintf('administrative_area_level_%d', $parentLevel->getLevel());

        return $this->findAreaByChildAddress($child, $parentType);
    }

    /**
     * @param Address $child
     * @param string  $type
     *
     * @return Area|null
     */
    private function findAreaByChildAddress(Address $child, $type)
    {
        $address = $this->findParentByChildAddress($child, $type);

        if (!$address) {
            return null;
        }

        $area = $this->entityManager
            ->getRepository(Area::class)
            ->findOneBy(['placeId' => $address->getPlaceId()])
        ;

        if (!$area) {
            $area = $this->createArea($address);
        }

        return $area;
    }

    /**
     * @param Address $child
     * @param string  $type
     *
     * @return Address|null
     */
    private function findParentByChildAddress(Address $child, $type)
    {
        $address = $this->findParentByChildAddressCoordinates($child, $type);

        if (!$address) {
            return $this->findParentByChildAddressComponents($child, $type);
        }

        return $address;
    }

    /**
     * @param Address $child
     * @param string  $type
     *
     * @return Address
     */
    private function findParentByChildAddressCoordinates(Address $child, $type)
    {
        $query = [
            'latitude'   => $child->getLatitude(),
            'longitude'  => $child->getLongitude(),
            'language'   => $this->getDefaultLocale(),
            'resultType' => $type,
        ];

        $address = $this->findAddress($query, function (Address $child) {
            return !!$child->getCountry();
        });

        return $address;
    }

    /**
     * @param Address $child
     * @param string  $type
     *
     * @return Address
     */
    private function findParentByChildAddressComponents(Address $child, $type)
    {
        $formattedAddress = $this->formatAddress($this->getParentAddressComponents($child));

        $query = [
            'address'    => $formattedAddress,
            'language'   => $this->getDefaultLocale(),
            'components' => [
                'country' => $child->getCountryCode(),
            ],
        ];

        $address = $this->findAddress($query, function (Address $child) use ($type) {
            return !!$child->getCountry() && $child->hasType($type);
        });

        return $address;
    }

    /**
     * @param Address $address
     *
     * @return Area|null
     */
    private function createArea(Address $address)
    {
        $parent = $this->findAdministrativeAreaByChildAddress($address);

        if ($parent) {
            $country = $parent->getCountry();
        } else {
            $country = $this->findCountryByChildAddress($address);

            if (!$country) {
                return null;
            }
        }

        $area = new Area();
        $area->setCountry($country);
        $area->setTags($address->getTypes());

        // if parent exists
        if ($parent) {
            $area->setParent($parent);
        }

        $this->fillLocation($area, $address);
        $this->translateArea($area, $address, true, false);

        $this->entityManager->persist($area);

        return $area;
    }

    /**
     * @param Address $child
     *
     * @return Country|null
     * @throws \Exception
     */
    private function findCountryByChildAddress(Address $child)
    {
        $country = $this->entityManager
            ->getRepository(Country::class)
            ->findOneBy(['code' => strtolower($child->getCountryCode())])
        ;

        if (!$country) {
            $query = [
                'address'  => $child->getCountry()->getName(),
                'language' => $this->getDefaultLocale(),
            ];

            $address = $this->findAddress($query, function (Address $address) {
                return $address->getCountry() && !$address->getLocality();
            });

            if (!$address) {
                return null;
            }

            $country = $this->createCountry($address);
        }

        return $country;
    }

    /**
     * @param Address $address
     *
     * @return Country
     */
    private function createCountry(Address $address)
    {
        $country = new Country();
        $country->setCode(strtolower($address->getCountryCode()));

        $this->fillLocation($country, $address);
        $this->translateCountry($country, $address, true);

        $this->entityManager->persist($country);

        return $country;
    }

    /**
     * @param array    $query
     * @param callable $filter
     *
     * @return Address
     * @throws \Exception
     */
    private function findAddress(array $query, callable $filter = null)
    {
        $geocoder = $this->geocoder->createBuilder();
        $accessor = PropertyAccess::createPropertyAccessor();

        foreach ($query as $key => $value) {
            $accessor->setValue($geocoder, $key, $value);
        }

        try {
            $addresses = $geocoder->geocode();
        } catch (\Exception $e) {
            $addresses = [];
        }

        if ($filter) {
            foreach ($addresses as $address) {
                if ($filter($address)) {
                    return $address;
                }
            }
        } else {
            if ($addresses->count() > 0) {
                return $addresses->first();
            }
        }

        return null;
    }

    /**
     * @param string $placeId
     * @param string $locale
     *
     * @return Address
     */
    private function findAddressByPlaceId($placeId, $locale)
    {
        $query = [
            'placeId'  => $placeId,
            'language' => $locale,
        ];

        $address = $this->findAddress($query, function (Address $address) {
            return !!$address->getCountry();
        });

        return $address;
    }

    /**
     * @param AbstractLocation $location
     * @param Address          $address
     */
    private function fillLocation(AbstractLocation $location, Address $address)
    {
        $location
            ->setPlaceId($address->getPlaceId())
            ->setLat($address->getLatitude())
            ->setLng($address->getLongitude())
            ->setSouthwestLat($address->getBounds()->getSouth())
            ->setSouthwestLng($address->getBounds()->getWest())
            ->setNortheastLat($address->getBounds()->getNorth())
            ->setNortheastLng($address->getBounds()->getEast())
            ->setTimeZone($address->getTimezone())
        ;
    }

    /**
     * @param Location $location
     * @param Address  $address
     * @param bool     $fallbackToDefault
     */
    private function translateLocation(Location $location, Address $address, $fallbackToDefault = true, $propagate = true)
    {
        $translation = $location->translate(null, $fallbackToDefault);
        $translation->setFormattedAddress($address->getFormattedAddress());

        $location->mergeNewTranslations();

        if ($propagate) {
            $area        = $location->getArea();
            $areaAddress = $this->findAddressByPlaceId($area->getPlaceId(), $fallbackToDefault ? $this->getDefaultLocale() : $this->getLocale());

            if ($areaAddress) {
                $this->translateArea($area, $areaAddress, $fallbackToDefault, $propagate);

                $this->entityManager->persist($area);
            }
        }
    }

    /**
     * @param Area $area
     * @param Address  $address
     * @param bool     $fallbackToDefault
     */
    private function translateArea(Area $area, Address $address, $fallbackToDefault = true, $propagate = true)
    {
        $areaName = $address->getLocality() ?: $address->getAdminLevels()->getFirstLevelLessThan(AdminLevelCollection::MAX_LEVEL_DEPTH)->getCode();

        $translation = $area->translate(null, $fallbackToDefault);
        $translation
            ->setName($areaName)
            // @todo: set it if current $address has data EXACTLY about current area (not a child)
            // ->setFormattedAddress($address->getFormattedAddress())
        ;

        $area->mergeNewTranslations();

        if ($propagate) {
            $parent = $area;

            while ($parent = $parent->getParent()) {
                if (!$parent->getTranslations()->containsKey($translation->getLocale())) {
                    /** @var AdminLevel $adminLevel */
                    foreach ($address->getAdminLevels() as $adminLevel) {
                        $adminLevelTag = sprintf('administrative_area_level_%d', $adminLevel->getLevel());

                        if ($parent->hasTag($adminLevelTag)) {
                            $parentTranslation = $parent->translate(null, $fallbackToDefault);
                            $parentTranslation->setName($adminLevel->getCode());

                            $parent->mergeNewTranslations();

                            $this->entityManager->persist($parent);
                        }
                    }
                }
            }

            $country = $area->getCountry();

            $this->translateCountry($country, $address, $fallbackToDefault);

            $this->entityManager->persist($country);
        }
    }

    /**
     * @param Country $country
     * @param Address $address
     * @param bool    $fallbackToDefault
     */
    private function translateCountry(Country $country, Address $address, $fallbackToDefault = true)
    {
        $translation = $country->translate(null, $fallbackToDefault);
        $translation->setName($address->getCountry()->getName());

        $country->mergeNewTranslations();
    }

    /**
     * @return array
     */
    private function getAreaTypes()
    {
        return array_merge(self::$localityTypes, self::$administrativeAreaTypes);
    }

    /**
     * @param Address $address
     *
     * @return int|null
     */
    private function retreiveAdminLevel(Address $address)
    {
        foreach ($address->getTypes() as $type) {
            if (strpos($type, 'administrative_area_level_') === 0) {
                return intval(substr($type, -1));
            }
        }

        return null;
    }

    /**
     * @param Address $address
     *
     * @return AdminLevel[]
     */
    private function getAddressParentAdminLevels(Address $address)
    {
        $level = $this->retreiveAdminLevel($address);
        $level = $level ? $level - 1 : AdminLevelCollection::MAX_LEVEL_DEPTH;

        return $address->getAdminLevels()->getLevelsLessThan($level);
    }

    /**
     * @param Address $address
     *
     * @return AdminLevel
     */
    private function getAddressParentAdminLevel(Address $address)
    {
        $level = $this->retreiveAdminLevel($address);
        $level = $level ? $level - 1 : AdminLevelCollection::MAX_LEVEL_DEPTH;

        return $address->getAdminLevels()->getFirstLevelLessThan($level);
    }

    /**
     * @param Address $address
     *
     * @return array
     */
    private function getParentAddressComponents(Address $address)
    {
        $parents    = $this->getAddressParentAdminLevels($address);
        $components = [];

        foreach ($parents as $parent) {
            $components[] = $parent->getName();
        }

        return $components;
    }

    /**
     * @param array $components
     *
     * @return string
     */
    private function formatAddress(array $components)
    {
        return implode(', ', $components);
    }
}
