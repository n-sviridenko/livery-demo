<?php

namespace Livery\Bundle\CoreBundle\Geography;

/**
 * Class Bounds
 */
class Bounds
{
    /**
     * @var LatLng
     */
    private $southwest;

    /**
     * @var LatLng
     */
    private $northeast;

    /**
     * Bounds constructor.
     *
     * @param LatLng $southwest
     * @param LatLng $northeast
     */
    public function __construct(LatLng $southwest, LatLng $northeast)
    {
        $this->southwest = $southwest;
        $this->northeast = $northeast;
    }

    /**
     * @return LatLng
     */
    public function getSouthwest()
    {
        return $this->southwest;
    }

    /**
     * @return LatLng
     */
    public function getNortheast()
    {
        return $this->northeast;
    }

    /**
     * @return float
     */
    public function getDiagonal()
    {
        $width  = $this->getSouthwest()->getLat() - $this->getNortheast()->getLat();
        $height = $this->getSouthwest()->getLng() - $this->getNortheast()->getLng();

        return sqrt(pow($width, 2) + pow($height, 2));
    }
}
