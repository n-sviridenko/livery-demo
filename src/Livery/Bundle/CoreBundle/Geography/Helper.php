<?php

namespace Livery\Bundle\CoreBundle\Geography;

/**
 * Class Helper
 */
class Helper
{
    /**
     * @param LatLng $latLng
     * @param float  $distance The distance (in kilometers)
     *
     * @return Bounds
     */
    public static function getBoundsByLatLng(LatLng $latLng, $distance)
    {
        $earthRadius = 6371;

        $latDelta = ($distance/$earthRadius) * (180/M_PI);
        $lngDelta = $latDelta / cos($latLng->getLat() * M_PI/180);

        $southwest = new LatLng($latLng->getLat() - $latDelta, $latLng->getLng() - $lngDelta);
        $northeast = new LatLng($latLng->getLat() + $latDelta, $latLng->getLng() + $lngDelta);

        return new Bounds($southwest, $northeast);
    }
}
