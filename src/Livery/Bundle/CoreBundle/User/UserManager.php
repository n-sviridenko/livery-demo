<?php

namespace Livery\Bundle\CoreBundle\User;

use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Doctrine\UserManager as BaseUserManager;

/**
 * Class UserManager
 */
final class UserManager extends BaseUserManager
{
    /**
     * {@inheritdoc}
     */
    public function updateUser(UserInterface $user, $andFlush = true)
    {
        if ($user->getUsername() !== $user->getEmail()) {
            $user->setUsername($user->getEmail());
        }

        parent::updateUser($user, $andFlush);
    }
}
