<?php

namespace Livery\Bundle\CoreBundle\Trip;

use Symfony\Bridge\Doctrine\RegistryInterface;

use Livery\Bundle\EntityBundle\Entity\Trip\Trip;
use Livery\Bundle\EntityBundle\Repository\TripRepository;

/**
 * Class TripManager
 */
class TripManager
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * TripManager constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry) {
        $this->registry = $registry;
    }

    /**
     * @param Trip $trip
     */
    public function saveTrip(Trip $trip)
    {
        $this->getRepository()->add($trip);
    }

    /**
     * @return TripRepository
     */
    private function getRepository()
    {
        return $this->registry->getRepository(Trip::class);
    }
}
