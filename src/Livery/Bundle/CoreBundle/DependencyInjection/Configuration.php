<?php

namespace Livery\Bundle\CoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('livery_core');

        $rootNode
            ->children()
                ->arrayNode('settings')
                    ->children()
                        ->arrayNode('supported_locales')
                            ->prototype('scalar')->isRequired()->end()
                        ->end()
                        ->arrayNode('supported_measures')
                            ->children()
                                ->arrayNode('mass')
                                    ->prototype('scalar')->isRequired()->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
