<?php

namespace Livery\Bundle\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class LiveryCoreBundle
 */
class LiveryCoreBundle extends Bundle
{
}
