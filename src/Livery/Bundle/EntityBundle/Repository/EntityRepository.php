<?php

namespace Livery\Bundle\EntityBundle\Repository;

use Doctrine\ORM\EntityRepository as BaseEntityRepository;

use Livery\Component\Resource\Model\ResourceInterface;
use Livery\Component\Resource\Repository\RepositoryInterface;

/**
 * Class EntityRepository
 */
class EntityRepository extends BaseEntityRepository implements RepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function add(ResourceInterface $resource)
    {
        $this->_em->persist($resource);
        $this->_em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove(ResourceInterface $resource)
    {
        if (null !== $this->find($resource->getId())) {
            $this->_em->remove($resource);
            $this->_em->flush();
        }
    }
}
