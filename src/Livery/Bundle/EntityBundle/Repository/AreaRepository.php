<?php

namespace Livery\Bundle\EntityBundle\Repository;

use Doctrine\ORM\NonUniqueResultException;
use Gedmo\Tree\Entity\Repository\MaterializedPathRepository;

use Livery\Bundle\EntityBundle\Entity\Location\Area;

/**
 * Class AreaRepository
 */
class AreaRepository extends MaterializedPathRepository
{
    /**
     * @param string $areaSlug
     * @param string $countrySlug
     *
     * @return Area|null
     * @throws NonUniqueResultException
     */
    public function findOneBySlug($areaSlug, $countrySlug)
    {
        $builder = $this->createQueryBuilder('l');
        $builder
            ->join('l.country', 'c')
            ->where('l.slug = :areaSlug')
            ->andWhere('c.slug = :countrySlug')
            // prefere areas with higher depth
            ->orderBy('l.level', 'desc')
            ->setMaxResults(1)
            ->setParameters(compact('areaSlug', 'countrySlug'))
        ;

        return $builder->getQuery()->getOneOrNullResult();
    }
}
