<?php

namespace Livery\Bundle\EntityBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class LiveryEntityBundle
 */
class LiveryEntityBundle extends Bundle
{
}
