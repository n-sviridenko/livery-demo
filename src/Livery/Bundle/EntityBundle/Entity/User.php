<?php

namespace Livery\Bundle\EntityBundle\Entity;

use libphonenumber\PhoneNumber;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;

use Livery\Component\User\Model\UserInterface;
use Livery\Component\Media\Model\ImageInterface;
use Livery\Bundle\EntityBundle\Entity\Media\Image;
use Livery\Bundle\EntityBundle\Repository\UserRepository;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="user")
 * @UniqueEntity("phone")
 */
class User extends BaseUser implements UserInterface
{
    use ORMBehaviors\Timestampable\Timestampable;

    const SEX_MALE   = 'male';
    const SEX_FEMALE = 'female';

    /**
     * @var array
     */
    public static $sexes = [
        self::SEX_MALE,
        self::SEX_FEMALE,
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", columnDefinition="enum('male', 'female')", nullable=true)
     */
    private $sex;

    /**
     * @var PhoneNumber
     *
     * @ORM\Column(name="phone", type="phone_number", nullable=true, unique=true)
     * @AssertPhoneNumber()
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="about", type="text", nullable=true)
     */
    private $about;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="date")
     * @Assert\NotNull()
     * @Assert\LessThanOrEqual("-18 years")
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=2)
     * @Assert\NotBlank()
     */
    private $locale = 'en';

    /**
     * @var ImageInterface
     *
     * @ORM\ManyToOne(targetEntity=Image::class)
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $picture;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * {@inheritdoc}
     */
    public function setLastName($lastName = null)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * {@inheritdoc}
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * {@inheritdoc}
     */
    public function setPhone(PhoneNumber $phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * {@inheritdoc}
     */
    public function setAbout($about = null)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * {@inheritdoc}
     */
    public function setBirthdate(\DateTime $birthdate = null)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * {@inheritdoc}
     */
    public function getAge()
    {
        $now = new \DateTime();

        return $now->diff($this->getBirthdate())->y;
    }

    /**
     * {@inheritdoc}
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * {@inheritdoc}
     */
    public function setPicture(ImageInterface $picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPicture()
    {
        return $this->picture;
    }
}
