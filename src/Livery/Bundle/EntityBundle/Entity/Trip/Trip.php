<?php

namespace Livery\Bundle\EntityBundle\Entity\Trip;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

use Livery\Bundle\EntityBundle\Entity\User;
use Livery\Bundle\EntityBundle\Entity\EntityInterface;
use Livery\Bundle\EntityBundle\Entity\AbstractRouteable;
use Livery\Bundle\EntityBundle\Repository\TripRepository;
use Livery\Bundle\EntityBundle\Entity\Location\AbstractLocation;
use Livery\Bundle\EntityBundle\Validator\Constraints\Trip as TripAssert;
use Livery\Bundle\EntityBundle\Entity\Location\AbstractCountryAwareLocation;

/**
 * Class Trip
 *
 * @ORM\Entity(repositoryClass=TripRepository::class)
 * @ORM\Table(name="trip")
 * @TripAssert
 */
class Trip extends AbstractRouteable implements EntityInterface
{
    use ORMBehaviors\Timestampable\Timestampable;

    const VEHICLE_CAR   = 'car';
    const VEHICLE_TRAIN = 'train';
    const VEHICLE_PLANE = 'plane';
    const VEHICLE_SHIP  = 'ship';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="depart_at", type="datetime")
     * @Assert\NotNull
     * @Assert\DateTime
     */
    private $departAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="arrive_at", type="datetime")
     * @Assert\NotNull
     * @Assert\DateTime
     */
    private $arriveAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity=User::class)
     * @Assert\NotNull
     */
    private $traveller;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description = null;

    /**
     * @var int
     *
     * @ORM\Column(name="max_response_delay_hourly", type="integer")
     * @Assert\NotNull
     * @Assert\Choice(callback = {Trip::class, "getAllowedResponseDelaysHourly"})
     */
    private $maxResponseDelayHourly;

    /**
     * @var bool
     *
     * @ORM\Column(name="published", type="boolean")
     */
    private $published = true;

    /**
     * @var string
     *
     * @ORM\Column(name="vehicle", type="string")
     * @Assert\NotNull
     * @Assert\Choice(callback = {Trip::class, "getAllowedVehicles"})
     */
    private $vehicle;

    /**
     * @return string[]
     */
    public static function getAllowedVehicles()
    {
        return [
            self::VEHICLE_CAR,
            self::VEHICLE_TRAIN,
            self::VEHICLE_PLANE,
            self::VEHICLE_SHIP,
        ];
    }

    /**
     * @return int[]
     */
    public static function getAllowedResponseDelaysHourly()
    {
        return [
            1,
            3,
            6,
            12,
        ];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setFrom(AbstractLocation $from)
    {
        if (!$from instanceof AbstractCountryAwareLocation) {
            throw new \Exception(
                sprintf('Expected instance of %s, %s received.', AbstractCountryAwareLocation::class, get_class($from))
            );
        }

        return parent::setFrom($from);
    }

    /**
     * {@inheritdoc}
     */
    public function setTo(AbstractLocation $to)
    {
        if (!$to instanceof AbstractCountryAwareLocation) {
            throw new \Exception(
                sprintf('Expected instance of %s, %s received.', AbstractCountryAwareLocation::class, get_class($to))
            );
        }

        return parent::setTo($to);
    }

    /**
     * Set departAt
     *
     * @param \DateTime|null $departAt
     *
     * @return Trip
     */
    public function setDepartAt(\DateTime $departAt = null)
    {
        $this->departAt = $departAt;

        return $this;
    }

    /**
     * Get departAt
     *
     * @return \DateTime|null
     */
    public function getDepartAt()
    {
        return $this->departAt;
    }

    /**
     * Set arriveAt
     *
     * @param \DateTime|null $arriveAt
     *
     * @return Trip
     */
    public function setArriveAt(\DateTime $arriveAt = null)
    {
        $this->arriveAt = $arriveAt;

        return $this;
    }

    /**
     * Get arriveAt
     *
     * @return \DateTime|null
     */
    public function getArriveAt()
    {
        return $this->arriveAt;
    }

    /**
     * Set traveller
     *
     * @param User $traveller
     *
     * @return Trip
     */
    public function setTraveller(User $traveller)
    {
        $this->traveller = $traveller;

        return $this;
    }

    /**
     * Get traveller
     *
     * @return User
     */
    public function getTraveller()
    {
        return $this->traveller;
    }

    /**
     * Set description
     *
     * @param string|null $description
     *
     * @return Trip
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getMaxResponseDelayHourly()
    {
        return $this->maxResponseDelayHourly;
    }

    /**
     * @param int $maxResponseDelayHourly
     *
     * @return Trip
     */
    public function setMaxResponseDelayHourly($maxResponseDelayHourly)
    {
        $this->maxResponseDelayHourly = $maxResponseDelayHourly;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param bool $published
     *
     * @return Trip
     */
    public function setPublished($published)
    {
        $this->published = (bool) $published;

        return $this;
    }

    /**
     * @return string
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * @param string $vehicle
     *
     * @return Trip
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;

        return $this;
    }
}
