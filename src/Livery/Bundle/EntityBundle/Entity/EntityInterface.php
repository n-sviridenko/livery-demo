<?php

namespace Livery\Bundle\EntityBundle\Entity;

use Livery\Component\Resource\Model\ResourceInterface;

/**
 * Interface EntityInterface
 */
interface EntityInterface extends ResourceInterface
{
}
