<?php

namespace Livery\Bundle\EntityBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Livery\Bundle\EntityBundle\Repository\BaseLocationRepository;

/**
 * Class AbstractLocation
 *
 * @ORM\Entity(repositoryClass=BaseLocationRepository::class)
 * @ORM\Table(name="location")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "location" = Location::class,
 *     "area" = Area::class,
 *     "country" = Country::class
 * })
 * @UniqueEntity("placeId")
 */
abstract class AbstractLocation
{
    use ORMBehaviors\Translatable\Translatable;

    const TYPE_LOCATION = 'location';
    const TYPE_AREA     = 'area';
    const TYPE_COUNTRY  = 'country';

    /**
     * @var array
     */
    static public $types = [
        self::TYPE_LOCATION,
        self::TYPE_AREA,
        self::TYPE_COUNTRY,
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="decimal", precision=8, scale=6)
     * @Assert\NotBlank
     * @Assert\Range(min=-90, max=90)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="decimal", precision=9, scale=6)
     * @Assert\NotBlank
     * @Assert\Range(min=-180, max=180)
     */
    private $lng;

    /**
     * @var string
     *
     * @ORM\Column(name="southwest_lat", type="decimal", precision=8, scale=6)
     * @Assert\NotBlank
     * @Assert\Range(min=-90, max=90)
     */
    private $southwestLat;

    /**
     * @var string
     *
     * @ORM\Column(name="southwest_lng", type="decimal", precision=9, scale=6)
     * @Assert\NotBlank
     * @Assert\Range(min=-180, max=180)
     */
    private $southwestLng;

    /**
     * @var string
     *
     * @ORM\Column(name="northeast_lat", type="decimal", precision=8, scale=6)
     * @Assert\NotBlank
     * @Assert\Range(min=-90, max=90)
     */
    private $northeastLat;

    /**
     * @var string
     *
     * @ORM\Column(name="northeast_lng", type="decimal", precision=9, scale=6)
     * @Assert\NotBlank
     * @Assert\Range(min=-180, max=180)
     */
    private $northeastLng;

    /**
     * @var string
     *
     * @ORM\Column(name="time_zone", type="string", length=255, nullable=true)
     */
    private $timeZone;

    /**
     * @var string
     *
     * @ORM\Column(name="place_id", type="string", length=255, unique=true)
     */
    private $placeId;

    /**
     * {@inheritdoc}
     */
    public static function getTranslationEntityClass()
    {
        return LocationTranslation::class;
    }

    /**
     * Location constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    abstract public function getType();

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lat
     *
     * @param string $lat
     *
     * @return AbstractLocation
     */
    public function setLat($lat)
    {
        $this->lat = round($lat, 6);

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param string $lng
     *
     * @return AbstractLocation
     */
    public function setLng($lng)
    {
        $this->lng = round($lng, 6);

        return $this;
    }

    /**
     * Get lng
     *
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set southwestLat
     *
     * @param string $southwestLat
     *
     * @return AbstractLocation
     */
    public function setSouthwestLat($southwestLat)
    {
        $this->southwestLat = round($southwestLat, 6);

        return $this;
    }

    /**
     * Get southwestLat
     *
     * @return string
     */
    public function getSouthwestLat()
    {
        return $this->southwestLat;
    }

    /**
     * Set southwestLng
     *
     * @param string $southwestLng
     *
     * @return AbstractLocation
     */
    public function setSouthwestLng($southwestLng)
    {
        $this->southwestLng = round($southwestLng, 6);

        return $this;
    }

    /**
     * Get southwestLng
     *
     * @return string
     */
    public function getSouthwestLng()
    {
        return $this->southwestLng;
    }

    /**
     * Set northeastLat
     *
     * @param string $northeastLat
     *
     * @return AbstractLocation
     */
    public function setNortheastLat($northeastLat)
    {
        $this->northeastLat = round($northeastLat, 6);

        return $this;
    }

    /**
     * Get northeastLat
     *
     * @return string
     */
    public function getNortheastLat()
    {
        return $this->northeastLat;
    }

    /**
     * Set northeastLng
     *
     * @param string $northeastLng
     *
     * @return AbstractLocation
     */
    public function setNortheastLng($northeastLng)
    {
        $this->northeastLng = round($northeastLng, 6);

        return $this;
    }

    /**
     * Get northeastLng
     *
     * @return string
     */
    public function getNortheastLng()
    {
        return $this->northeastLng;
    }

    /**
     * Set timeZone
     *
     * @param string|null $timeZone
     *
     * @return AbstractLocation
     */
    public function setTimeZone($timeZone = null)
    {
        $this->timeZone = $timeZone;

        return $this;
    }

    /**
     * Get timeZone
     *
     * @return string|null
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * Set placeId
     *
     * @param string $placeId
     *
     * @return AbstractLocation
     */
    public function setPlaceId($placeId)
    {
        $this->placeId = $placeId;

        return $this;
    }

    /**
     * Get placeId
     *
     * @return string
     */
    public function getPlaceId()
    {
        return $this->placeId;
    }

    /**
     * Set area
     *
     * @param Area|null $area
     *
     * @return AbstractLocation
     */
    public function setArea(Area $area)
    {
        $this->area = $area;

        return $this;
    }
}
