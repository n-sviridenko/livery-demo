<?php

namespace Livery\Bundle\EntityBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

use Livery\Bundle\EntityBundle\Entity\EntityInterface;
use Livery\Bundle\EntityBundle\Repository\EntityRepository;

/**
 * Class LocationTranslation
 *
 * @ORM\Entity(repositoryClass=EntityRepository::class)
 * @ORM\Table(name="location_translation")
 */
class LocationTranslation implements EntityInterface
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="formatted_address", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private $formattedAddress;

    /**
     * {@inheritdoc}
     */
    public static function getTranslatableEntityClass()
    {
        return AbstractLocation::class;
    }

    /**
     * Set name
     *
     * @param string|null $name
     *
     * @return LocationTranslation
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set formattedAddress
     *
     * @param string|null $formattedAddress
     *
     * @return LocationTranslation
     */
    public function setFormattedAddress($formattedAddress = null)
    {
        $this->formattedAddress = $formattedAddress;

        return $this;
    }

    /**
     * Get formattedAddress
     *
     * @return string|null
     */
    public function getFormattedAddress()
    {
        return $this->formattedAddress;
    }
}
