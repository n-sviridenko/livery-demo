<?php

namespace Livery\Bundle\EntityBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Livery\Bundle\EntityBundle\Entity\EntityInterface;
use Livery\Bundle\EntityBundle\Repository\CountryRepository;

/**
 * @todo: code in uppercase
 *
 * Class Country
 *
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 */
class Country extends AbstractLocation implements EntityInterface
{
    use ORMBehaviors\Sluggable\Sluggable;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     * @Assert\NotBlank
     */
    private $code;

    /**
     * @var ArrayCollection|Area[]
     *
     * @ORM\OneToMany(targetEntity=Area::class, mappedBy="country")
     */
    private $areas;

    /**
     * Country constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->areas = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return self::TYPE_COUNTRY;
    }

    /**
     * {@inheritdoc}
     */
    public function getSluggableFields()
    {
        return ['name'];
    }

    /**
     * {@inheritdoc}
     */
    public function getRegenerateSlugOnUpdate()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->translate($this->getDefaultLocale())->getName();
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return ArrayCollection|Area[]
     */
    public function getAreas()
    {
        return $this->areas;
    }
}
