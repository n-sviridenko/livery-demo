<?php

namespace Livery\Bundle\EntityBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Tree\Traits\MaterializedPath;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

use Livery\Bundle\EntityBundle\Entity\EntityInterface;
use Livery\Bundle\EntityBundle\Repository\AreaRepository;

/**
 * Class Area
 *
 * @ORM\Entity(repositoryClass=AreaRepository::class)
 * @Gedmo\Tree(type="materializedPath")
 */
class Area extends AbstractCountryAwareLocation implements EntityInterface
{
    use MaterializedPath;
    use ORMBehaviors\Sluggable\Sluggable;

    /**
     * @var string
     *
     * @Gedmo\TreePathSource
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(length=3000, nullable=true)
     * @Gedmo\TreePath
     */
    protected $path;

    /**
     * @var Area|null
     *
     * @ORM\ManyToOne(targetEntity=Area::class, inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     * @Gedmo\TreeParent
     */
    protected $parent;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Gedmo\TreeLevel
     */
    protected $level;

    /**
     * @var ArrayCollection|Area[]
     *
     * @ORM\OneToMany(targetEntity=Area::class, mappedBy="parent")
     */
    protected $children;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="areas")
     * @Assert\NotNull
     */
    private $country;

    /**
     * @var ArrayCollection|Location[]
     *
     * @ORM\OneToMany(targetEntity=Location::class, mappedBy="area")
     */
    private $locations;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $tags = [];

    /**
     * Country constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->children  = new ArrayCollection();
        $this->locations = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return self::TYPE_AREA;
    }

    /**
     * {@inheritdoc}
     */
    public function getSluggableFields()
    {
        return ['name'];
    }

    /**
     * {@inheritdoc}
     */
    public function getRegenerateSlugOnUpdate()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->translate($this->getDefaultLocale())->getName();
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return Area
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return ArrayCollection|Location[]
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * @param array $tags
     */
    public function setTags(array $tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param string $tag
     *
     * @return bool
     */
    public function hasTag($tag)
    {
        return in_array($tag, $this->tags);
    }
}
