<?php

namespace Livery\Bundle\EntityBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;

use Livery\Bundle\EntityBundle\Repository\BaseLocationRepository;

/**
 * Class AbstractCountryAwareLocation
 *
 * @ORM\Entity(repositoryClass=BaseLocationRepository::class)
 */
abstract class AbstractCountryAwareLocation extends AbstractLocation
{
    /**
     * @return Country
     */
    abstract public function getCountry();
}
