<?php

namespace Livery\Bundle\EntityBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Livery\Bundle\EntityBundle\Entity\EntityInterface;
use Livery\Bundle\EntityBundle\Repository\LocationRepository;

/**
 * Class Location
 *
 * @ORM\Entity(repositoryClass=LocationRepository::class)
 */
class Location extends AbstractCountryAwareLocation implements EntityInterface
{
    /**
     * @var Area
     *
     * @ORM\ManyToOne(targetEntity=Area::class, inversedBy="locations")
     * @Assert\NotNull
     */
    private $area;

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return self::TYPE_LOCATION;
    }

    /**
     * Get area
     *
     * @return Area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param Area $area
     *
     * @return Location
     */
    public function setArea(Area $area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->getArea()->getCountry();
    }
}
