<?php

namespace Livery\Bundle\EntityBundle\Entity\Media;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Livery\Bundle\EntityBundle\Entity\User;
use Livery\Bundle\EntityBundle\Repository\MediaRepository;

/**
 * Class AbstractMedia
 *
 * @ORM\Entity(repositoryClass=MediaRepository::class)
 * @ORM\Table(name="media")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "document" = Document::class,
 *     "image" = Image::class
 * })
 * @UniqueEntity({"category", "path"})
 */
abstract class AbstractMedia
{
    use ORMBehaviors\Timestampable\Timestampable;

    const CATEGORY_PROFILE_PICTURE = 'profile_picture';
    const CATEGORY_ITEM_PICTURE    = 'item_picture';

    const STATUS_ALLOWED = 'allowed';
    const STATUS_REFUSED = 'refused';

    /**
     * @var array
     */
    public static $categories = [
        self::CATEGORY_PROFILE_PICTURE,
        self::CATEGORY_ITEM_PICTURE,
    ];

    /**
     * @var array
     */
    public static $statuses = [
        self::STATUS_ALLOWED,
        self::STATUS_REFUSED,
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=255, nullable=true)
     */
    private $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string", length=255, nullable=true)
     */
    private $mimeType;

    /**
     * @var string
     *
     * @ORM\Column(name="original_name", type="string", length=255, nullable=true)
     */
    private $originalName;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=255)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", columnDefinition="enum('allowed', 'refused')", nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="sourceUrl", type="string", nullable=true)
     */
    private $sourceUrl;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity=User::class)
     * @Assert\NotNull
     */
    private $owner;

    /**
     * @return string
     */
    abstract public function getType();

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return static
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set extension
     *
     * @param string|null $extension
     *
     * @return static
     */
    public function setExtension($extension = null)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string|null
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set mimeType
     *
     * @param string|null $mimeType
     *
     * @return static
     */
    public function setMimeType($mimeType = null)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string|null
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Set originalName
     *
     * @param string|null $originalName
     *
     * @return static
     */
    public function setOriginalName($originalName = null)
    {
        $this->originalName = $originalName;

        return $this;
    }

    /**
     * Get originalName
     *
     * @return string|null
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * Set size
     *
     * @param integer $size
     *
     * @return static
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set status
     *
     * @param string|null $status
     *
     * @return static
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return static
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string|null $sourceUrl
     *
     * @return static
     */
    public function setSourceUrl($sourceUrl)
    {
        $this->sourceUrl = $sourceUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSourceUrl()
    {
        return $this->sourceUrl;
    }

    /**
     * @return string|null
     */
    public function getFullPath()
    {
        $path = $this->getPath();

        if (!$path) {
            return null;
        }

        $category  = $this->getCategory();
        $extension = $this->getExtension();
        $path      = "{$category}/{$path}";

        return $extension ? "{$path}.{$extension}" : $path;
    }

    /**
     * Set owner
     *
     * @param User $owner
     *
     * @return static
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }
}
