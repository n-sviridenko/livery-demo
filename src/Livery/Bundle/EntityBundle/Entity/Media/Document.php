<?php

namespace Livery\Bundle\EntityBundle\Entity\Media;

use Doctrine\ORM\Mapping as ORM;

use Livery\Component\Media\Model\DocumentInterface;
use Livery\Bundle\EntityBundle\Entity\EntityInterface;
use Livery\Bundle\EntityBundle\Repository\MediaRepository;

/**
 * Class Document
 *
 * @ORM\Entity(repositoryClass=MediaRepository::class)
 */
class Document extends AbstractMedia implements EntityInterface, DocumentInterface
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return 'document';
    }
}
