<?php

namespace Livery\Bundle\EntityBundle\Entity\Media;

use Doctrine\ORM\Mapping as ORM;

use Livery\Component\Media\Model\ImageInterface;
use Livery\Bundle\EntityBundle\Entity\EntityInterface;
use Livery\Bundle\EntityBundle\Repository\MediaRepository;

/**
 * Class Image
 *
 * @ORM\Entity(repositoryClass=MediaRepository::class)
 */
class Image extends AbstractMedia implements EntityInterface, ImageInterface
{
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return 'image';
    }
}
