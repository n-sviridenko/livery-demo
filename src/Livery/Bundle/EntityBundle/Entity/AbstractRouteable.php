<?php

namespace Livery\Bundle\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Livery\Bundle\EntityBundle\Entity\Location\Area;
use Livery\Bundle\EntityBundle\Entity\Location\Country;
use Livery\Bundle\EntityBundle\Entity\Location\Location;
use Livery\Bundle\EntityBundle\Entity\Location\AbstractLocation;
use Livery\Bundle\EntityBundle\Entity\Location\AbstractCountryAwareLocation;

/**
 * Class AbstractRouteable
 *
 * @ORM\MappedSuperclass
 */
abstract class AbstractRouteable
{
    /**
     * @var Area
     *
     * @ORM\ManyToOne(targetEntity=Area::class)
     */
    protected $from;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity=Country::class)
     */
    protected $fromCountry;

    /**
     * @var Area
     *
     * @ORM\ManyToOne(targetEntity=Area::class)
     */
    protected $to;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity=Country::class)
     */
    protected $toCountry;

    /**
     * Set from
     *
     * @param AbstractLocation $from
     *
     * @return static
     */
    public function setFrom(AbstractLocation $from)
    {
        if ($from instanceof AbstractCountryAwareLocation) {
            $this->from        = $from instanceof Location ? $from->getArea() : $from;
            $this->fromCountry = $from->getCountry();
        } else {
            $this->from        = null;
            $this->fromCountry = $from;
        }

        return $this;
    }

    /**
     * Get from
     *
     * @Assert\NotNull
     *
     * @return AbstractLocation
     */
    public function getFrom()
    {
        return $this->from ?: $this->fromCountry;
    }

    /**
     * Get from country
     *
     * @return Country
     */
    public function getFromCountry()
    {
        return $this->fromCountry;
    }

    /**
     * Set to
     *
     * @param AbstractLocation $to
     *
     * @return static
     */
    public function setTo(AbstractLocation $to)
    {
        if ($to instanceof AbstractCountryAwareLocation) {
            $this->to        = $to instanceof Location ? $to->getArea() : $to;
            $this->toCountry = $to->getCountry();
        } else {
            $this->to        = null;
            $this->toCountry = $to;
        }

        return $this;
    }

    /**
     * Get to
     *
     * @Assert\NotNull
     *
     * @return AbstractLocation
     */
    public function getTo()
    {
        return $this->to ?: $this->toCountry;
    }

    /**
     * Get to country
     *
     * @return Country
     */
    public function getToCountry()
    {
        return $this->toCountry;
    }
}
