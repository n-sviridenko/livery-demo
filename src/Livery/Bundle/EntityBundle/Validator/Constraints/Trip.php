<?php

namespace Livery\Bundle\EntityBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
class Trip extends Constraint
{
    const TO_ERROR                 = 'livery.core.constraint.trip.to';
    const ARRIVE_AT_ERROR          = 'livery.core.constraint.trip.arrive_at';
    const DEPART_AT_ERROR          = 'livery.core.constraint.trip.depart_at';

    /**
     * {@inheritdoc}
     */
    protected static $errorNames = [
        self::TO_ERROR                 => 'TO_ERROR',
        self::ARRIVE_AT_ERROR          => 'ARRIVE_AT_ERROR',
        self::DEPART_AT_ERROR          => 'DEPART_AT_ERROR',
    ];

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
