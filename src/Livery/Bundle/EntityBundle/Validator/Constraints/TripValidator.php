<?php

namespace Livery\Bundle\EntityBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

use Livery\Bundle\CoreBundle\Measure\MeasureManager;

/**
 * Class TripValidator
 *
 * @todo: test it
 * @todo: resolve departAt < utc time + 1 hour (somebody that has a time that's for 23 hours less than utc will be unhappy)
 */
class TripValidator extends ConstraintValidator
{
    /**
     * @var MeasureManager
     */
    private $measureManager;

    /**
     * TripSearchValidator constructor.
     *
     * @param MeasureManager $measureManager
     */
    public function __construct(MeasureManager $measureManager)
    {
        $this->measureManager = $measureManager;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($object, Constraint $constraint)
    {
        if ($object->getFrom() &&
            $object->getTo() &&
            $object->getFrom()->getId() === $object->getTo()->getId()
        ) {
            $this->context
                ->buildViolation(Trip::TO_ERROR)
                ->setCode(Trip::TO_ERROR)
                ->addViolation()
            ;
        }

        if ($object->getDepartAt()) {
            if ($object->getDepartAt() < new \DateTime('+1 hour')) {
                $this->context
                    ->buildViolation(Trip::DEPART_AT_ERROR)
                    ->setCode(Trip::DEPART_AT_ERROR)
                    ->addViolation()
                ;
            }

            if ($object->getArriveAt() && $object->getDepartAt() >= $object->getArriveAt()) {
                $this->context
                    ->buildViolation(Trip::ARRIVE_AT_ERROR)
                    ->setCode(Trip::ARRIVE_AT_ERROR)
                    ->addViolation()
                ;
            }
        }
    }
}
