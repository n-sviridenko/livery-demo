<?php

namespace Livery\Behat\Context\Transform;

use Behat\Behat\Context\Context;
use FOS\UserBundle\Model\UserManagerInterface;

/**
 * Class UserContext
 */
final class UserContext implements Context
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * UserContext constructor.
     *
     * @param UserManagerInterface $userManager
     */
    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @Transform :user
     * @Transform /^user "([^"]+)"$/
     */
    public function getUserByEmail($email)
    {
        return $this->userManager->findUserByEmail($email);
    }
}
