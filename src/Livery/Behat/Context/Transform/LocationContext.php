<?php

namespace Livery\Behat\Context\Transform;

use Assert\Assertion;
use Behat\Behat\Context\Context;

use Livery\Bundle\CoreBundle\Location\LocationManager;

/**
 * Class LocationContext
 */
final class LocationContext implements Context
{
    /**
     * @var LocationManager
     */
    private $locationManager;

    /**
     * LocationContext constructor.
     *
     * @param LocationManager $locationManager
     */
    public function __construct(LocationManager $locationManager)
    {
        $this->locationManager = $locationManager;
    }

    /**
     * @Transform /^"([^"]+)" location$/
     * @Transform :location
     */
    public function getLocationByPlaceId($placeId)
    {
        $location = $this->locationManager->getLocationByPlaceId($placeId);

        Assertion::notNull($location, sprintf('Cannot find location with place id %s.', $placeId));

        return $location;
    }
}
