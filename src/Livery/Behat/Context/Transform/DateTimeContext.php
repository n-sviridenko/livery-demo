<?php

namespace Livery\Behat\Context\Transform;

use Behat\Behat\Context\Context;

/**
 * Class DateTimeContext
 */
final class DateTimeContext implements Context
{
    /**
     * @Transform :date
     * @Transform :startDate
     * @Transform :endDate
     */
    public function getDate($date)
    {
        return new \DateTime($date);
    }
}
