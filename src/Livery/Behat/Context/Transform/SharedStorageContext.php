<?php

namespace Livery\Behat\Context\Transform;

use Behat\Behat\Context\Context;

use Livery\Behat\Service\SharedStorage;
use Livery\Behat\Formatter\StringInflector;

/**
 * @author Mateusz Zalewski <mateusz.zalewski@lakion.com>
 */
final class SharedStorageContext implements Context
{
    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    /**
     * @param SharedStorage $sharedStorage
     */
    public function __construct(SharedStorage $sharedStorage)
    {
        $this->sharedStorage = $sharedStorage;
    }

    /**
     * @Transform /^(it|its|theirs|them)$/
     */
    public function getLatestResource()
    {
        return $this->sharedStorage->getLatestResource();
    }

    /**
     * @Transform /^(?:this|that|the) ([^"]+)$/
     */
    public function getResource($resource)
    {
        return $this->sharedStorage->get(StringInflector::nameToCode($resource));
    }
}
