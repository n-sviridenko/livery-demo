<?php

namespace Livery\Behat\Context;

use Assert\Assertion;
use Sanpi\Behatch\Json\Json;
use Behat\Gherkin\Node\PyStringNode;
use Sanpi\Behatch\Json\JsonInspector;
use Sanpi\Behatch\Context\BaseContext;
use Sanpi\Behatch\HttpCall\HttpCallResultPool;

/**
 * Class JsonContext
 */
class JsonContext extends BaseContext
{
    /**
     * @var JsonInspector
     */
    protected $inspector;

    /**
     * @var HttpCallResultPool
     */
    protected $httpCallResultPool;

    /**
     * JsonContext constructor.
     *
     * @param HttpCallResultPool $httpCallResultPool
     * @param string             $evaluationMode
     */
    public function __construct(HttpCallResultPool $httpCallResultPool, $evaluationMode = 'javascript')
    {
        $this->inspector          = new JsonInspector($evaluationMode);
        $this->httpCallResultPool = $httpCallResultPool;
    }

    /**
     * @todo: use or remove
     *
     * @Then the JSON response should be like:
     */
    public function theJsonNodeShouldBeEqualToTheJsonNode(PyStringNode $like)
    {
        $expected = json_decode($like->getRaw(), true);
        $expected = $this->getDottedArray($expected);

        $json = $this->getJson();

        foreach ($expected as $path => $expectedValue) {
            $value = $this->inspector->evaluate($json, $path);

            if ($value !== $expectedValue) {
                throw new \Exception(
                    sprintf(
                        "The value '%s' of node '%s' is not equal to '%s'",
                        $value,
                        $path,
                        $expectedValue
                    )
                );
            }
        }

        return true;
    }

    /**
     * @Then the JSON node :path should be equal to the node :comparePath
     */
    public function theJsonNodeShouldBeEqualToTheNodeByPath($path, $comparePath)
    {
        $json     = $this->getJson();
        $value    = $this->inspector->evaluate($json, $path);
        $expected = $this->inspector->evaluate($json, $comparePath);

        Assertion::same(json_encode($value), json_encode($expected));
    }

    /**
     * @return Json
     */
    private function getJson()
    {
        return new Json($this->httpCallResultPool->getResult()->getValue());
    }

    /**
     * @param array  $array
     * @param string $prepend
     *
     * @return array
     */
    private function getDottedArray(array $array, $prepend = '')
    {
        $results = [];

        foreach ($array as $key => $value) {
            if (is_array($value) && !empty($value)) {
                $results = array_merge($results, $this->getDottedArray($value, $prepend.$key.'.'));
            } else {
                $results[$prepend.$key] = $value;
            }
        }

        return $results;
    }
}
