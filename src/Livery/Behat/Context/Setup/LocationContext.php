<?php

namespace Livery\Behat\Context\Setup;

use Behat\Behat\Context\Context;
use Livery\Behat\Formatter\StringInflector;

use Livery\Behat\Service\SharedStorage;
use Livery\Bundle\EntityBundle\Entity\Location\AbstractLocation;

/**
 * Class LocationContext
 */
final class LocationContext implements Context
{
    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    /**
     * LocationContext constructor.
     *
     * @param SharedStorage $sharedStorage
     */
    public function __construct(SharedStorage $sharedStorage)
    {
        $this->sharedStorage = $sharedStorage;
    }

    /**
     * @Given /^there is a ("[^"]+" location) named "([^"]+)"$/
     */
    public function thereIsLocationNamedAs(AbstractLocation $location, $locationName)
    {
        $storageKey = sprintf('%s_location', StringInflector::nameToCode($locationName));

        $this->sharedStorage->set($storageKey, $location);
    }
}
