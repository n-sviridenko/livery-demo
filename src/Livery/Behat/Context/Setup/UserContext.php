<?php

namespace Livery\Behat\Context\Setup;

use Behat\Behat\Context\Context;
use FOS\UserBundle\Model\UserManagerInterface;

use Livery\Behat\Service\SharedStorage;
use Livery\Bundle\EntityBundle\Entity\Media\Image;
use Livery\Bundle\EntityBundle\Entity\User;

/**
 * Class UserContext
 */
final class UserContext implements Context
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    /**
     * UserContext constructor.
     *
     * @param UserManagerInterface $userManager
     * @param SharedStorage        $sharedStorage
     */
    public function __construct(UserManagerInterface $userManager, SharedStorage $sharedStorage)
    {
        $this->userManager   = $userManager;
        $this->sharedStorage = $sharedStorage;
    }

    /**
     * @Given there is a user :name with email :email identified by :password and born :date
     */
    public function thereIsUserWithEmailAndPassword($name, $email, $password, \DateTime $date)
    {
        $this->createUser($name, $email, $password, $date);
    }

    /**
     * @Given /^this user has (the [^"]+) picture$/
     */
    public function theUserHasPicture(Image $picture)
    {
        $user = $this->getUser();
        $user->setPicture($picture);

        $this->userManager->updateUser($user);
    }

    /**
     * @param string    $name
     * @param string    $email
     * @param string    $password
     * @param \DateTime $birthdate
     */
    private function createUser($name, $email, $password, \DateTime $birthdate)
    {
        list ($firstName, $lastName) = explode(' ', $name);

        /** @var User $user */
        $user = $this->userManager->createUser();
        $user
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setEmail($email)
            ->setEnabled(true)
            ->setPlainPassword($password)
            ->setBirthdate($birthdate)
        ;

        $this->userManager->updateUser($user);

        $this->sharedStorage->set('user', $user);
    }

    /**
     * @return User
     */
    private function getUser()
    {
        return $this->sharedStorage->get('user');
    }
}
