<?php

namespace Livery\Behat\Context\Setup;

use Behat\Behat\Context\Context;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesser;

use Livery\Behat\Service\SharedStorage;
use Livery\Behat\Formatter\StringInflector;
use Livery\Bundle\EntityBundle\Entity\User;
use Livery\Bundle\EntityBundle\Entity\Media\Image;
use Livery\Bundle\EntityBundle\Entity\Media\Document;

/**
 * Class MediaContext
 */
final class MediaContext implements Context
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    /**
     * MediaContext constructor.
     *
     * @param RegistryInterface $registry
     * @param SharedStorage     $sharedStorage
     */
    public function __construct(RegistryInterface $registry, SharedStorage $sharedStorage)
    {
        $this->registry      = $registry;
        $this->sharedStorage = $sharedStorage;
    }

    /**
     * @Given /^there is a "([^"]+)" (image|document) named "([^"]+)" from "([^"]+)" category and owned by (user "[^"]+")$/
     */
    public function thereIsMediaNamedHasCategoryOwnedBy($mimeType, $type, $originalName, $category, User $user)
    {
        $extension = ExtensionGuesser::getInstance()->guess($mimeType);
        $size      = strlen($originalName);
        $path      = Urlizer::urlize($originalName);

        if ($type === 'image') {
            $media = new Image();
        } else {
            $media = new Document();
        }

        $media
            ->setPath($path)
            ->setExtension($extension)
            ->setMimeType($mimeType)
            ->setOriginalName($originalName)
            ->setSize($size)
            ->setCategory(StringInflector::nameToCode($category))
            ->setOwner($user)
        ;

        $storageKey = sprintf('%s_media', StringInflector::nameToCode($originalName));

        $this->sharedStorage->set($storageKey, $media);

        $manager = $this->registry->getManager();
        $manager->persist($media);
        $manager->flush();
    }
}
