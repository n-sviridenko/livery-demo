<?php

namespace Livery\Behat\Context\Setup;

use Behat\Behat\Context\Context;

use Livery\Behat\Service\SharedStorage;
use Livery\Bundle\EntityBundle\Entity\User;
use Livery\Bundle\CoreBundle\Trip\TripManager;
use Livery\Bundle\EntityBundle\Entity\Trip\Trip;
use Livery\Bundle\EntityBundle\Entity\Location\AbstractLocation;

/**
 * Class TripContext
 */
final class TripContext implements Context
{
    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    /**
     * @var TripManager
     */
    private $tripManager;

    /**
     * TripContext constructor.
     *
     * @param SharedStorage $sharedStorage
     * @param TripManager   $tripManager
     */
    public function __construct(SharedStorage $sharedStorage, TripManager $tripManager)
    {
        $this->sharedStorage = $sharedStorage;
        $this->tripManager   = $tripManager;
    }

    /**
     * @Given /^there is a (.+?) trip of (user "[^"]+") from (the [^"]+) to (the [^"]+)$/
     */
    public function thereIsTripOfFromTo($vehicle, User $user, AbstractLocation $from, AbstractLocation $to)
    {
        $trip = new Trip();
        $trip
            ->setVehicle($vehicle)
            ->setTraveller($user)
            ->setFrom($from)
            ->setTo($to)
        ;

        $this->sharedStorage->set('trip', $trip);
    }

    /**
     * @Given this trip departs at :startDate and arrives at :endDate
     */
    public function theTripDepartsAtAndArrivesAt(\DateTime $startDate, \DateTime $endDate)
    {
        $trip = $this->getTrip();
        $trip
            ->setDepartAt($startDate)
            ->setArriveAt($endDate)
        ;
    }

    /**
     * @Given this trip traveller will respond in :maxResponseDelayHourly hour(s)
     */
    public function theTripTravellerWillRespondIn($maxResponseDelayHourly)
    {
        $trip = $this->getTrip();
        $trip->setMaxResponseDelayHourly($maxResponseDelayHourly);
    }

    /**
     * @Given this trip is saved
     */
    public function theTripIsSaved()
    {
        $trip = $this->getTrip();

        $this->tripManager->saveTrip($trip);
    }

    /**
     * @return Trip
     */
    private function getTrip()
    {
        return $this->sharedStorage->get('trip');
    }
}
