<?php

namespace Livery\Behat\Context\Setup;

use Behat\Behat\Context\Context;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManagerInterface;

use Livery\Behat\Service\SharedStorage;
use Livery\Bundle\EntityBundle\Entity\User;

/**
 * Class ApiContext
 */
class ApiContext implements Context
{
    /**
     * @var JWTManagerInterface
     */
    private $jwtManager;

    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    /**
     * ApiContext constructor.
     *
     * @param JWTManagerInterface $jwtManager
     * @param SharedStorage       $sharedStorage
     */
    public function __construct(JWTManagerInterface $jwtManager, SharedStorage $sharedStorage)
    {
        $this->jwtManager    = $jwtManager;
        $this->sharedStorage = $sharedStorage;
    }

    /**
     * @Given /^I am authorized in api as (user "[^"]+")$/
     */
    public function iAmAuthorizedInApiAsUser(User $user)
    {
        $jwt = $this->jwtManager->create($user);

        $this->sharedStorage->set('jwt_token', $jwt);
    }
}
