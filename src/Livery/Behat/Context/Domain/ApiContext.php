<?php

namespace Livery\Behat\Context\Domain;

use Assert\Assertion;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\Gherkin\Node\PyStringNode;
use Symfony\Component\Routing\Router;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpKernel\KernelInterface;

use Livery\Behat\Service\SharedStorage;
use Livery\Behat\Service\Resolver\LinkedArrayResolver;
use Livery\Behat\Service\Resolver\Api\ParameterResolverContext;
use Livery\Behat\Service\Resolver\Api\ParameterResolverAggregator;

/**
 * Class ApiContext
 */
class ApiContext implements Context
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    /**
     * @var ParameterResolverAggregator
     */
    private $parameterResolver;

    /**
     * @var LinkedArrayResolver
     */
    private $arrayResolver;

    /**
     * ApiContext constructor.
     *
     * @param RouterInterface             $router
     * @param RegistryInterface           $registry
     * @param KernelInterface             $kernel
     * @param SharedStorage               $sharedStorage
     * @param ParameterResolverAggregator $parameterResolver
     * @param LinkedArrayResolver         $arrayResolver
     */
    public function __construct(
        RouterInterface $router,
        RegistryInterface $registry,
        KernelInterface $kernel,
        SharedStorage $sharedStorage,
        ParameterResolverAggregator $parameterResolver,
        LinkedArrayResolver $arrayResolver
    ) {
        $this->router            = $router;
        $this->registry          = $registry;
        $this->kernel            = $kernel;
        $this->sharedStorage     = $sharedStorage;
        $this->parameterResolver = $parameterResolver;
        $this->arrayResolver     = $arrayResolver;
    }

    /**
     * @When I send a :method request to :routeName
     */
    public function iSendRequestToRoute($method, $routeName)
    {
        $this->sendRequest($method, $routeName);
    }

    /**
     * @When I send a :method request to :routeName with parameters:
     */
    public function iSendRequestToRouteWithParameters($method, $routeName, TableNode $node)
    {
        $parameters = $node->getRowsHash();

        $this->sendRequest($method, $routeName, $parameters);
    }

    /**
     * @When I send a :method request to :routeName with body:
     */
    public function iSendRequestToRouteWithBody($method, $routeName, PyStringNode $node)
    {
        $data = json_decode($node->getRaw(), true);

        $this->sendRequest($method, $routeName, [], $data);
    }

    /**
     * @When I prepare a :method request to :routeName with parameters:
     */
    public function iPrepareRequestToRouteWithParameters($method, $routeName, TableNode $node)
    {
        $parameters = $node->getRowsHash();

        $this->createRequest($method, $routeName, $parameters);
    }

    /**
     * @When I send it with body:
     */
    public function iSendItWithBody(PyStringNode $node)
    {
        $request = $this->getRequest();

        $context = new ParameterResolverContext($request->get('_route'), ParameterResolverContext::BODY_LOCATION);
        $data    = json_decode($node->getRaw(), true);
        $data    = $this->resolveParameters($data, $context);

        $request->request->replace($data);

        $this->processRequest($request);
    }

    /**
     * @Then the response status code should be :code
     */
    public function theResponseStatusCodeShouldBe($code)
    {
        Assertion::eq($this->getResponse()->getStatusCode(), $code);
    }

    /**
     * @Then print current URL
     */
    public function printCurrentUrl()
    {
        echo $this->getRequest()->getUri();
    }

    /**
     * @Then print last response
     */
    public function printLastResponse()
    {
        echo (
            $this->getRequest()->getUri()."\n\n".
            $this->getResponse()->getContent()
        );
    }

    /**
     * @param string $method
     * @param string $routeName
     * @param array  $parameters
     * @param array  $data
     *
     * @return Response
     */
    private function sendRequest($method, $routeName, array $parameters = [], array $data = [])
    {
        $context = new ParameterResolverContext($routeName, ParameterResolverContext::BODY_LOCATION);
        $data    = $this->resolveParameters($data, $context);

        $request = $this->createRequest($method, $routeName, $parameters, $data);

        return $this->processRequest($request);
    }

    /**
     * @param string $method
     * @param string $routeName
     * @param array  $parameters
     * @param array  $data
     *
     * @return Request
     */
    private function createRequest($method, $routeName, array $parameters = [], array $data = [])
    {
        $uri = $this->getUri($routeName, $parameters);

        return $this->createRawRequest($method, $uri, $data);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $data
     *
     * @return Request
     */
    private function createRawRequest($method, $uri, array $data = [])
    {
        $headers = ['Authorization' => sprintf('Bearer %s', $this->getJwtToken())];

        $request = Request::create($uri, $method, $data);
        $request->headers->add($headers);

        $this->sharedStorage->set('api_request', $request);

        return $request;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    private function processRequest(Request $request)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->registry->getManager();
        $entityManager->clear(); // to force all param converters etc. to use a fresh data

        $response = $this->kernel->handle($request);

        $this->sharedStorage->set('api_response', $response);
        $this->sharedStorage->set('json', $response->getContent());

        return $response;
    }

    /**
     * @param string $routeName
     * @param array  $parameters
     *
     * @return string
     */
    private function getUri($routeName, array $parameters = [])
    {
        $parameters = ['_locale' => $this->getLocale()] + $parameters;

        $context    = new ParameterResolverContext($routeName, ParameterResolverContext::URI_LOCATION);
        $parameters = $this->resolveParameters($parameters, $context);

        return $this->router->generate($routeName, $parameters, Router::ABSOLUTE_URL);
    }

    /**
     * @param array                    $parameters
     * @param ParameterResolverContext $context
     *
     * @return array
     */
    private function resolveParameters(array $parameters, ParameterResolverContext $context)
    {
        $parameters = $this->arrayResolver->resolve($parameters);

        return $this->mapParameters($parameters, $context);
    }

    /**
     * @param array                    $parameters
     * @param ParameterResolverContext $context
     *
     * @return array
     */
    private function mapParameters(array $parameters, ParameterResolverContext $context)
    {
        return array_map(function ($item) use ($context) {
            if (is_array($item)) {
                return $this->mapParameters($item, $context);
            }

            if (is_object($item)) {
                return $this->parameterResolver->resolve($item, $context);
            }

            return $item;
        }, $parameters);
    }

    /**
     * @return string
     */
    private function getJwtToken()
    {
        return $this->sharedStorage->get('jwt_token');
    }

    /**
     * @return Request
     */
    private function getRequest()
    {
        return $this->sharedStorage->get('api_request');
    }

    /**
     * @todo: take fallback locale from config
     *
     * @return string
     */
    private function getLocale()
    {
        return $this->sharedStorage->has('api_locale')
            ? $this->sharedStorage->get('api_locale')
            : 'en'
        ;
    }

    /**
     * @return Response
     */
    private function getResponse()
    {
        return $this->sharedStorage->get('api_response');
    }
}
