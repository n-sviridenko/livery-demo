<?php

namespace Livery\Behat\Context;

use Behat\Gherkin\Node\TableNode;
use Sanpi\Behatch\HttpCall\Request;
use Sanpi\Behatch\Context\RestContext;
use Symfony\Component\Filesystem\Filesystem;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Context\CustomSnippetAcceptingContext;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class ApiContext
 */
class ApiContext implements CustomSnippetAcceptingContext
{
    /**
     * @var RestContext
     */
    private $restContext;

    /**
     * @var Request
     */
    private $request;

    /**
     * ApiContext constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public static function getAcceptedSnippetType()
    {
        return 'turnip';
    }

    /**
     * @param BeforeScenarioScope $scope
     *
     * @BeforeScenario
     */
    public function gatherContexts(BeforeScenarioScope $scope)
    {
        $environment = $scope->getEnvironment();

        $this->restContext = $environment->getContext(RestContext::class);
    }

    /**
     * @Given I am logged in API as :username with the password :password
     */
    public function loginInApi($username, $password)
    {
        $data = [
            ['key', 'value'],
            ['_username', $username],
            ['_password', $password],
        ];

        $this->restContext->iSendARequestToWithParameters('POST', '/login_check', new TableNode($data));

        $response = $this->restContext->getSession()->getPage()->getContent();
        $response = json_decode($response, true);

        if (json_last_error() != JSON_ERROR_NONE) {
            throw new \Exception('The JSON is not a valid');
        }

        if (!array_key_exists('token', $response)) {
            throw new \Exception(
                sprintf('Mising key "token" in the response "%s"', json_encode($response))
            );
        }

        $this->restContext->iAddHeaderEqualTo('Authorization', sprintf('Bearer %s', $response['token']));
    }

    /**
     * @Given I am logged out of API
     */
    public function logoutFromApi()
    {
        $this->restContext->iAddHeaderEqualTo('Authorization', null);
    }

    /**
     * @todo: resolve it by browserkit
     * 
     * Sends a HTTP request with a file
     *
     * @Given I upload the media located on :path as :category
     */
    public function iSendARequestToWithParameters($path, $category)
    {
        $path = rtrim($this->restContext->getMinkParameter('files_path'), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.$path;

        $filesystem = new Filesystem();

        // @todo: generate safe temp name (but not $filesystem->tempname - couldn't override this file)
        $tempname = $path.rand();
        $filesystem->copy($path, $tempname);

        $file = new UploadedFile($tempname, basename($path));

        return $this->request->send(
            'POST',
            $this->restContext->locatePath('/en/media'),
            ['media' => ['category' => $category]],
            ['media' => ['file' => $file]]
        );
    }
}
