<?php

namespace Livery\Behat\Service\Resolver;

use Livery\Behat\Service\SharedStorage;

/**
 * Class LinkedArrayResolver
 */
class LinkedArrayResolver
{
    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    /**
     * RelationResolver constructor.
     *
     * @param SharedStorage $sharedStorage
     */
    public function __construct(SharedStorage $sharedStorage)
    {
        $this->sharedStorage = $sharedStorage;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function resolve(array $data)
    {
        return $this->mapLinks($data);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function mapLinks(array $data)
    {
        return array_map(function ($item) {
            if (is_array($item)) {
                return $this->mapLinks($item);
            }

            if ($this->isLink($item)) {
                return $this->getValueByLink($item);
            }

            return $item;
        }, $data);
    }

    /**
     * @param mixed $data
     *
     * @return bool
     */
    private function isLink($data)
    {
        return is_string($data) && strpos($data, '@') === 0;
    }

    /**
     * @param string $link
     *
     * @return mixed
     */
    private function getValueByLink($link)
    {
        return $this->sharedStorage->get(substr($link, 1));
    }
}
