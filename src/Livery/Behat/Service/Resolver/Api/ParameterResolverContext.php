<?php

namespace Livery\Behat\Service\Resolver\Api;

/**
 * Class ParameterResolverContext
 */
class ParameterResolverContext
{
    const URI_LOCATION  = 'uri';
    const BODY_LOCATION = 'body';

    /**
     * @var string
     */
    private $routeName;

    /**
     * @var string
     */
    private $location;

    /**
     * ParameterResolverContext constructor.
     *
     * @param string $routeName
     * @param string $location
     */
    public function __construct($routeName, $location)
    {
        $this->routeName = $routeName;
        $this->location  = $location;
    }

    /**
     * @return string
     */
    public function getRouteName()
    {
        return $this->routeName;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }
}
