<?php

namespace Livery\Behat\Service\Resolver\Api;

/**
 * Class ParameterResolverAggregator
 */
class ParameterResolverAggregator
{
    /**
     * @var ParameterResolverInterface[]
     */
    private $resolvers;

    /**
     * ParameterResolverAggregator constructor.
     *
     * @param ParameterResolverInterface[] $resolvers
     */
    public function __construct(array $resolvers)
    {
        $this->resolvers = $resolvers;
    }

    /**
     * @param object                   $object
     * @param ParameterResolverContext $context
     *
     * @return mixed
     */
    public function resolve($object, ParameterResolverContext $context)
    {
        return $this
            ->getSupportedResolver($object, $context)
            ->resolve($object, $context)
        ;
    }

    /**
     * @param object                   $object
     * @param ParameterResolverContext $context
     *
     * @return ParameterResolverInterface
     */
    private function getSupportedResolver($object, ParameterResolverContext $context)
    {
        foreach ($this->resolvers as $resolver) {
            if ($resolver->supports($object, $context)) {
                return $resolver;
            }
        }

        throw new \LogicException(sprintf('Could not find a resolver for "%s" object.', get_class($object)));
    }
}
