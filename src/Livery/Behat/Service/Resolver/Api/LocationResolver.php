<?php

namespace Livery\Behat\Service\Resolver\Api;

use Livery\Bundle\EntityBundle\Entity\Location\AbstractLocation;

/**
 * Class LocationResolver
 */
class LocationResolver implements ParameterResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve($object, ParameterResolverContext $context)
    {
        return $object->getPlaceId();
    }

    /**
     * {@inheritdoc}
     */
    public function supports($object, ParameterResolverContext $context)
    {
        return ($object instanceof AbstractLocation);
    }
}
