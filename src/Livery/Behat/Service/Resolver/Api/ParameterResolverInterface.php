<?php

namespace Livery\Behat\Service\Resolver\Api;

/**
 * Interface ParameterResolverInterface
 */
interface ParameterResolverInterface
{
    /**
     * @param object                   $object
     * @param ParameterResolverContext $context
     *
     * @return mixed
     */
    public function resolve($object, ParameterResolverContext $context);

    /**
     * @param object                   $object
     * @param ParameterResolverContext $context
     *
     * @return bool
     */
    public function supports($object, ParameterResolverContext $context);
}
