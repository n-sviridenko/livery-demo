<?php

namespace Livery\Behat\Service\Resolver\Api;

use Livery\Component\Resource\Model\ResourceInterface;

/**
 * Class EntityResolver
 */
class EntityResolver implements ParameterResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve($object, ParameterResolverContext $context)
    {
        return $object->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function supports($object, ParameterResolverContext $context)
    {
        return ($object instanceof ResourceInterface);
    }
}
