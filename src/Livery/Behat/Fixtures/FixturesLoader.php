<?php

namespace Livery\Behat\Fixtures;

use Nelmio\Alice\Fixtures;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberUtil;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\Tests\Fixtures\ContainerAwareFixture;

/**
 * Class FixturesLoader
 */
class FixturesLoader extends ContainerAwareFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $fixtures = [
            __DIR__.'/user.yml',
            __DIR__.'/media.yml',
            __DIR__.'/trip.yml',
        ];

        $options = [
            'providers' => [$this],
        ];

        Fixtures::load($fixtures, $manager, $options);
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function constant($name)
    {
        return constant($name);
    }

    /**
     * @param string $datetime
     *
     * @return \DateTime
     */
    public function customDatetime($datetime = '')
    {
        return new \DateTime($datetime);
    }

    /**
     * @param string $string
     *
     * @return PhoneNumber
     */
    public function phoneNumber($string)
    {
        return $this->container
            ->get('libphonenumber.phone_number_util')
            ->parse($string, PhoneNumberUtil::UNKNOWN_REGION)
        ;
    }

    /**
     * @param string $placeId
     *
     * @return mixed
     */
    public function location($placeId)
    {
        return $this->container
            ->get('livery.core.location.location_manager')
            ->getLocationByPlaceId($placeId)
        ;
    }
}
