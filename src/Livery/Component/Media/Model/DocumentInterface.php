<?php

namespace Livery\Component\Media\Model;

/**
 * Interface DocumentInterface
 */
interface DocumentInterface extends MediaInterface
{
}
