<?php

namespace Livery\Component\User\Model;

use libphonenumber\PhoneNumber;

use Livery\Component\Media\Model\ImageInterface;
use Livery\Component\Resource\Model\ResourceInterface;
use Livery\Component\Resource\Model\TimestampableInterface;

/**
 * Interface UserInterface
 */
interface UserInterface extends BaseUserInterface, TimestampableInterface, ResourceInterface
{
    /**
     * @return string
     */
    public function getFirstName();

    /**
     * @param string $firstName
     *
     * @return static
     */
    public function setFirstName($firstName);

    /**
     * @return string|null
     */
    public function getLastName();

    /**
     * @param string|null $lastName
     *
     * @return static
     */
    public function setLastName($lastName);

    /**
     * @return PhoneNumber|null
     */
    public function getPhone();

    /**
     * @param PhoneNumber|null $phone
     *
     * @return static
     */
    public function setPhone(PhoneNumber $phone = null);

    /**
     * @return string|null
     */
    public function getSex();

    /**
     * @param string|null $sex
     *
     * @return static
     */
    public function setSex($sex);

    /**
     * @return string|null
     */
    public function getAbout();

    /**
     * @param string|null $about
     *
     * @return static
     */
    public function setAbout($about);

    /**
     * @return \DateTime
     */
    public function getBirthdate();

    /**
     * @param \DateTime $birthdate
     *
     * @return static
     */
    public function setBirthdate(\DateTime $birthdate);

    /**
     * @return int
     */
    public function getAge();

    /**
     * @return string
     */
    public function getLocale();

    /**
     * @param string $locale
     *
     * @return static
     */
    public function setLocale($locale);

    /**
     * @return ImageInterface|null
     */
    public function getPicture();

    /**
     * @param ImageInterface|null $picture
     *
     * @return static
     */
    public function setPicture(ImageInterface $picture = null);
}
