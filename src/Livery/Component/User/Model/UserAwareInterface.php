<?php

namespace Livery\Component\User\Model;

/**
 * Interface UserAwareInterface
 *
 * @deprecated: implement this methods without this interface
 */
interface UserAwareInterface
{
    /**
     * @return UserInterface
     */
    public function getUser();

    /**
     * @param UserInterface $user
     *
     * @return static
     */
    public function setUser(UserInterface $user);
}
