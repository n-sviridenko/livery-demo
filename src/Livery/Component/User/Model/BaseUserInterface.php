<?php

namespace Livery\Component\User\Model;

use FOS\UserBundle\Model\UserInterface as FOSUserInterface;

/**
 * Interface BaseUserInterface
 *
 * Adds fields missing in FOSUserInterface
 */
interface BaseUserInterface extends FOSUserInterface
{
    /**
     * @return \DateTime|null
     */
    public function getLastLogin();
}
