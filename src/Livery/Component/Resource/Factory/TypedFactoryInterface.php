<?php

namespace Livery\Component\Resource\Factory;

use Livery\Component\Resource\Model\ResourceInterface;

/**
 * Interface TypedFactoryInterface
 */
interface TypedFactoryInterface
{
    /**
     * @param string $type
     *
     * @return ResourceInterface
     */
    public function createTyped($type);
}
