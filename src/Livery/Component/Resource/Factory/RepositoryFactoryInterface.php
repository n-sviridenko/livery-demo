<?php

namespace Livery\Component\Resource\Factory;

use Livery\Component\Resource\Repository\RepositoryInterface;

/**
 * Interface RepositoryFactoryInterface
 *
 * @deprecated: Use resolve target entities
 */
interface RepositoryFactoryInterface
{
    /**
     * @return RepositoryInterface
     */
    public function create();
}
