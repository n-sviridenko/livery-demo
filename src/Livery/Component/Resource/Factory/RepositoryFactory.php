<?php

namespace Livery\Component\Resource\Factory;

use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class RepositoryFactory
 *
 * @deprecated: Use resolve target entities
 */
class RepositoryFactory implements RepositoryFactoryInterface
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var string
     */
    private $persistentObject;

    /**
     * RepositoryFactory constructor.
     *
     * @param RegistryInterface $registry
     * @param string            $persistentObject
     */
    public function __construct(RegistryInterface $registry, $persistentObject)
    {
        $this->registry         = $registry;
        $this->persistentObject = $persistentObject;
    }

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        return $this->registry->getRepository($this->persistentObject);
    }
}
