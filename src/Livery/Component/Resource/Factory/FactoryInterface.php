<?php

namespace Livery\Component\Resource\Factory;

use Livery\Component\Resource\Model\ResourceInterface;

/**
 * Interface FactoryInterface
 */
interface FactoryInterface
{
    /**
     * @return ResourceInterface
     */
    public function create();
}
