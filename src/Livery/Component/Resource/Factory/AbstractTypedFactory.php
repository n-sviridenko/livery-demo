<?php

namespace Livery\Component\Resource\Factory;

/**
 * Class AbstractTypedFactory
 *
 * @deprecated: Use TypedFactory
 */
abstract class AbstractTypedFactory implements TypedFactoryInterface
{
    /**
     * @var array
     */
    protected $classNames = [];

    /**
     * @param string $type
     * @param string $className
     */
    public function registerClassName($type, $className)
    {
        $this->classNames[$type] = $className;
    }

    /**
     * {@inheritdoc}
     */
    public function createTyped($type)
    {
        $className = $this->classNames[$type];

        return new $className();
    }
}
