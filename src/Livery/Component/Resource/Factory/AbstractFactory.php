<?php

namespace Livery\Component\Resource\Factory;

/**
 * Class AbstractFactory
 */
abstract class AbstractFactory implements FactoryInterface
{
    /**
     * @var string
     */
    protected $className;

    /**
     * @param string $className
     */
    public function setClassName($className)
    {
        $this->className = $className;
    }

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $className = $this->className;

        return new $className();
    }
}
