<?php

namespace Livery\Component\Resource\Model;

use Livery\Bundle\EntityBundle\Entity\Location\Country;
use Livery\Bundle\EntityBundle\Entity\Location\AbstractLocation;

/**
 * Interface RouteableInterface
 *
 * @todo: make and use interfaces from locations
 */
interface RouteableInterface
{
    /**
     * @return AbstractLocation
     */
    public function getFrom();

    /**
     * @return Country
     */
    public function getFromCountry();

    /**
     * @param AbstractLocation $from
     *
     * @return static
     */
    public function setFrom(AbstractLocation $from);

    /**
     * @return AbstractLocation
     */
    public function getTo();

    /**
     * @return Country
     */
    public function getToCountry();

    /**
     * @param AbstractLocation $to
     *
     * @return static
     */
    public function setTo(AbstractLocation $to);
}
