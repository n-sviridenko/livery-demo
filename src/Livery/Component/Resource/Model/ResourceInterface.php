<?php

namespace Livery\Component\Resource\Model;

/**
 * Interface ResourceInterface
 */
interface ResourceInterface
{
    /**
     * @return mixed
     */
    public function getId();
}
