<?php

namespace Livery\Component\Resource\Model;

/**
 * Interface TypedResourceInterface
 */
interface TypedResourceInterface extends ResourceInterface
{
    /**
     * @return string
     */
    public function getType();
}
