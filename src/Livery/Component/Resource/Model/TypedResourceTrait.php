<?php

namespace Livery\Component\Resource\Model;

/**
 * Trait TypedResourceTrait
 */
trait TypedResourceTrait
{
    /**
     * @return string
     */
    public function getType()
    {
        return self::TYPE;
    }
}
