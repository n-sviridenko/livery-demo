<?php

namespace Livery\Component\Resource\Model;

/**
 * Interface ResourceAwareInterface
 */
interface ResourceAwareInterface
{
    /**
     * @return string
     */
    public function getResourceClass();

    /**
     * @param string $resourceClass
     *
     * @return static
     */
    public function setResourceClass($resourceClass);

    /**
     * @return mixed
     */
    public function getResourceId();

    /**
     * @param mixed $resourceId
     *
     * @return static
     */
    public function setResourceId($resourceId);
}
