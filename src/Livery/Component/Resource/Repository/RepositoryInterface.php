<?php

namespace Livery\Component\Resource\Repository;

use Doctrine\Common\Persistence\ObjectRepository;

use Livery\Component\Resource\Model\ResourceInterface;

/**
 * Interface RepositoryInterface
 */
interface RepositoryInterface extends ObjectRepository
{
    /**
     * @param ResourceInterface $resource
     */
    public function add(ResourceInterface $resource);

    /**
     * @param ResourceInterface $resource
     */
    public function remove(ResourceInterface $resource);
}
