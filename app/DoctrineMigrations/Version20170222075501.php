<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170222075501 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, area_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, country_id INT DEFAULT NULL, lat NUMERIC(8, 6) NOT NULL, lng NUMERIC(9, 6) NOT NULL, southwest_lat NUMERIC(8, 6) NOT NULL, southwest_lng NUMERIC(9, 6) NOT NULL, northeast_lat NUMERIC(8, 6) NOT NULL, northeast_lng NUMERIC(9, 6) NOT NULL, time_zone VARCHAR(255) DEFAULT NULL, place_id VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, path VARCHAR(3000) DEFAULT NULL, level INT DEFAULT NULL, tags LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', slug VARCHAR(255) DEFAULT NULL, code VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_5E9E89CBDA6A219 (place_id), INDEX IDX_5E9E89CBBD0F409C (area_id), INDEX IDX_5E9E89CB727ACA70 (parent_id), INDEX IDX_5E9E89CBF92F3E70 (country_id), UNIQUE INDEX UNIQ_5E9E89CB77153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, formatted_address VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_A2BCA22B2C2AC5D3 (translatable_id), UNIQUE INDEX location_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, path VARCHAR(255) NOT NULL, extension VARCHAR(255) DEFAULT NULL, mime_type VARCHAR(255) DEFAULT NULL, original_name VARCHAR(255) DEFAULT NULL, size INT NOT NULL, category VARCHAR(255) NOT NULL, status enum(\'allowed\', \'refused\'), sourceUrl VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, type VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_6A2CA10CB548B0F (path), INDEX IDX_6A2CA10C7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trip (id INT AUTO_INCREMENT NOT NULL, traveller_id INT DEFAULT NULL, from_id INT DEFAULT NULL, from_country_id INT DEFAULT NULL, to_id INT DEFAULT NULL, to_country_id INT DEFAULT NULL, depart_at DATETIME NOT NULL, arrive_at DATETIME NOT NULL, description LONGTEXT DEFAULT NULL, max_response_delay_hourly INT NOT NULL, published TINYINT(1) NOT NULL, vehicle VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_7656F53BE58489A0 (traveller_id), INDEX IDX_7656F53B78CED90B (from_id), INDEX IDX_7656F53BD28BF877 (from_country_id), INDEX IDX_7656F53B30354A65 (to_id), INDEX IDX_7656F53BDE1CDC0D (to_country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, picture_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, locked TINYINT(1) NOT NULL, expired TINYINT(1) NOT NULL, expires_at DATETIME DEFAULT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', credentials_expired TINYINT(1) NOT NULL, credentials_expire_at DATETIME DEFAULT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) DEFAULT NULL, sex enum(\'male\', \'female\'), phone VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', about LONGTEXT DEFAULT NULL, birthdate DATE NOT NULL, locale VARCHAR(2) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D64992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_8D93D649A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_8D93D649444F97DD (phone), INDEX IDX_8D93D649EE45BDBF (picture_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CBBD0F409C FOREIGN KEY (area_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB727ACA70 FOREIGN KEY (parent_id) REFERENCES location (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CBF92F3E70 FOREIGN KEY (country_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE location_translation ADD CONSTRAINT FK_A2BCA22B2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES location (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53BE58489A0 FOREIGN KEY (traveller_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B78CED90B FOREIGN KEY (from_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53BD28BF877 FOREIGN KEY (from_country_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B30354A65 FOREIGN KEY (to_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53BDE1CDC0D FOREIGN KEY (to_country_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649EE45BDBF FOREIGN KEY (picture_id) REFERENCES media (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CBBD0F409C');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CB727ACA70');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CBF92F3E70');
        $this->addSql('ALTER TABLE location_translation DROP FOREIGN KEY FK_A2BCA22B2C2AC5D3');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53B78CED90B');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53BD28BF877');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53B30354A65');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53BDE1CDC0D');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649EE45BDBF');
        $this->addSql('ALTER TABLE media DROP FOREIGN KEY FK_6A2CA10C7E3C61F9');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53BE58489A0');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE location_translation');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE trip');
        $this->addSql('DROP TABLE user');
    }
}
