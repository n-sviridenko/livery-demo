@old @api @user
Feature: User
  In order to view users
  As anonymous
  I must to be able to have correct access

  Background:
    Given I am logged out of API

  Scenario: I can show information about active user
    Given I send a "GET" request to "/en/users/2"
    Then the response status code should be 200

  Scenario: I can't show information about locked user
    Given I send a "GET" request to "/en/users/3"
    Then the response status code should be 401

  Scenario: I can't show information about expired user
    Given I send a "GET" request to "/en/users/4"
    Then the response status code should be 401

  Scenario: I can't show information about disabled user
    Given I send a "GET" request to "/en/users/5"
    Then the response status code should be 401
