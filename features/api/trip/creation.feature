@api @trip
Feature: Trip creation

  Background:
    Given there is a user "Dave Terry" with email "user@livery.dev" identified by "user" and born "10.11.1993"

    Given there is a "ChIJl4foalHq9EcR8CG75CqrCAQ" location named "Lyon"
    And there is a "ChIJOwg_06VPwokRYv534QaPC8g" location named "New York"

  Scenario: I can create a trip
    Given I am authorized in api as user "user@livery.dev"
    When I send a "POST" request to "api_post_trips" with body:
      """
      {
        "trip": {
          "from": "@Lyon_location",
          "to": "@New_York_location",
          "departAt": "2026-03-04T10:00:00.000",
          "arriveAt": "2026-03-05T00:00:00.000",
          "description": "My grand trip",
          "maxWeight": 45.7,
          "maxWeightMeasure": "lb",
          "maxResponseDelayHourly": 3,
          "vehicle": "plane"
        }
      }
      """
    Then the response status code should be 201
    And the JSON node "from.name" should be equal to the string "Lyon"
    And the JSON node "to.name" should be equal to the string "New York"
    And the JSON node "departAt.date" should be equal to the string "2026-03-04 10:00:00.000000"
    And the JSON node "arriveAt.date" should be equal to the string "2026-03-05 00:00:00.000000"
    And the JSON node "description" should be equal to the string "My grand trip"
    And the JSON node "maxWeight" should be equal to the string "45.7"
    And the JSON node "maxWeightMeasure" should be equal to the string "lb"
    And the JSON node "maxResponseDelayHourly" should be equal to the number 3
    And the JSON node "vehicle" should be equal to the string "plane"
