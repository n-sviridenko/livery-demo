@old @api @trip
Feature: Trip
  In order to manage trips
  As user
  I must to be able to create and view trips

  Background:
    Given I am logged in API as "user@livery.dev" with the password "user"

  Scenario: I can create a simple trip
    When I send a "POST" request to "/en/trips" with parameters:
      | key                          | value                       |
      | trip[from]                   | ChIJl4foalHq9EcR8CG75CqrCAQ |
      | trip[to]                     | ChIJOwg_06VPwokRYv534QaPC8g |
      | trip[departAt]               | 2026-03-04T10:00:00.000     |
      | trip[arriveAt]               | 2026-03-05T00:00:00.000     |
      | trip[description]            | My grand trip               |
      | trip[maxWeight]              | 45.7                        |
      | trip[maxWeightMeasure]       | lb                          |
      | trip[maxResponseDelayHourly] | 3                           |
      | trip[vehicle]                | plane                       |
    Then the response status code should be 201
    And the JSON node "id" should be equal to the number 6

  Scenario: I can view this trip
    When I send a "GET" request to "/en/trips/6"
    Then the response status code should be 200
    And the JSON node "from.name" should be equal to the string "Lyon"
    And the JSON node "to.name" should be equal to the string "New York"
    And the JSON node "departAt.date" should be equal to the string "2026-03-04 10:00:00.000000"
    And the JSON node "arriveAt.date" should be equal to the string "2026-03-05 00:00:00.000000"
    And the JSON node "description" should be equal to the string "My grand trip"
    And the JSON node "maxWeight" should be equal to the string "45.7"
    And the JSON node "maxWeightMeasure" should be equal to the string "lb"
    And the JSON node "maxResponseDelayHourly" should be equal to the number 3

  Scenario: I can't create a country-related trip or trip that doesn't have a departure/arival point
    When I send a "POST" request to "/en/trips" with parameters:
      | key                  | value                       |
      | trip[from]           | ChIJu-SH28MJxkcRnwq9_851obM |
      | trip[departAt]       | 2026-03-04T10:00:00.000     |
      | trip[arriveAt]       | 2026-03-05T00:00:00.000     |
      | trip[description]    | My grand trip               |
    Then the response status code should be 400
    And the JSON node "errors.children.from.errors[0]" should contain "This value is not valid"
    And the JSON node "errors.children.to.errors[0]" should contain "This value should not be null"

  Scenario: I can't create a trip that has the same departure and arrival
    When I send a "POST" request to "/en/trips" with parameters:
      | key        | value                       |
      | trip[from] | ChIJl4foalHq9EcR8CG75CqrCAQ |
      | trip[to]   | ChIJl4foalHq9EcR8CG75CqrCAQ |
    Then the response status code should be 400
    And the JSON node "errors.errors[0]" should be equal to the string "livery.core.constraint.trip.to"

  Scenario: I can't create a trip that was already started
    When I send a "POST" request to "/en/trips" with parameters:
      | key            | value                   |
      | trip[departAt] | 2016-09-26T00:00:00.000 |
    Then the response status code should be 400
    And the JSON node "errors.errors[0]" should be equal to the string "livery.core.constraint.trip.depart_at"

  Scenario: I can't create a trip that has wrong time interval
    When I send a "POST" request to "/en/trips" with parameters:
      | key                  | value                   |
      | trip[departAt]       | 2026-09-26T10:00:00.000 |
      | trip[arriveAt]       | 2026-09-26T10:00:00.000 |
    Then the response status code should be 400
    And the JSON node "errors.errors[0]" should be equal to the string "livery.core.constraint.trip.arrive_at"
