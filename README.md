Demo
====

Technologies
------------

* Client: Javascript, Angular 2
* Server: PHP, Symfony 3
* Deployment: Ansible
* Virtualization: Vagrant

Requirements
------------

* VirtualBox ≥ 5.1.14 r112924 (Qt5.6.2)
* Vagrant ≥ 1.9.1
* `vagrant-hostmanager` plugin ≥ 1.8.5

Installation
------------

* `$ git clone https://gitlab.com/n-sviridenko/livery-demo.git`
* create `database` folder and put [livery.sql.gz](https://drive.google.com/open?id=0B8bVUg4xVr9kdzZSd0pBSWF0OGs) into it 
* `$ make setup`
* set `google_api_key: AIzaSyCwWg5cTg1XXFLNoHhhzlTnxN2poYMo0Gc` in `config/parameters.yml`

Usage
-----

Visit [http://livery.dev](http://livery.dev)

To login use the following users:

* admin@livery.dev / admin
* user@livery.dev / user
