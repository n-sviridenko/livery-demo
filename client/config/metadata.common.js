const helpers = require('./helpers');

module.exports = {
  title: 'Livery',
  baseUrl: '/',
  isDevServer: helpers.isWebpackDevServer()
};
