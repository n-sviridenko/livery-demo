import { config } from 'config/config';

const scripts = [
  `https://maps.googleapis.com/maps/api/js?libraries=places&key=${config.credentials.google.key}`,
  'https://apis.google.com/js/api.js',
];

const loadScripts = (resolve) => {
  require('scriptjs')(scripts, () => {
    resolve();
  });
};

const initGapi = (resolve) => {
  gapi.load('client', () => {
    gapi.client.load('customsearch', 'v1').then(() => {
      resolve();
    });
  });
};

const loadIntl = (resolve) => {
  if (global.Intl) {
    resolve();
  } else {
    (require as any).ensure([], function (require) {
      require('intl');
      require('intl/locale-data/jsonp/en.js');
      require('intl/locale-data/jsonp/ru.js');
      require('intl/locale-data/jsonp/fr.js');

      resolve();
    });
  }
};

export function loadAsyncScripts(): Promise<any> {
  return new Promise(loadScripts)
    .then(() => new Promise(initGapi))
    .then(() => new Promise(loadIntl))
  ;
}
