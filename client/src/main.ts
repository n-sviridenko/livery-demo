import * as moment from 'moment';
import { bootloader } from '@angularclass/hmr';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from 'app/app.module';
import { loadAsyncScripts } from './async-scripts';
import { getCurrentLocale, getTranslationProviders } from './i18n-providers';

import 'app/style/common.scss';

moment.locale(getCurrentLocale());

const translationProviders = getTranslationProviders();
const compilerOptions = {
  providers: [
    ...translationProviders,
  ],
};

export function main(): Promise<any> {
  return loadAsyncScripts().then(
    () => platformBrowserDynamic().bootstrapModule(AppModule, compilerOptions)
  );
}

bootloader(main);
