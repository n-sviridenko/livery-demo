export const config = {
  core: {
    appHost: APP_HOST,
  },
  api: {
    baseUrl: API_BASE_URL,
    globalHeaders: [],
  },
  auth: {
    baseUrl: API_BASE_URL,
    tokenKeyName: 'authToken',
  },
  credentials: {
    google: {
      key: 'AIzaSyBPwzzsSY65GOIGIKsFXrEfxwHGcSm2Q2I',
    },
  },
  user: {
    maxRating: 5,
  },
  session: {
    locales: ['ru', 'fr', 'en'],
    defaultLocale: 'en',
    currencies: ['USD', 'EUR', 'RUB'],
    defaultCurrency: 'USD',
  },
  intl: {
    massMeasures: ['kg', 'lb'],
    defaultMassMeasure: 'kg',
  },
};
