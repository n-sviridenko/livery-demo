import { TRANSLATIONS, TRANSLATIONS_FORMAT, LOCALE_ID } from '@angular/core';

import { config } from 'config/config';
import { LOCALE_KEY_NAME } from 'app/core';

// @todo: use more elegant solution
export function getCurrentLocale(): string {
  const locale = localStorage.getItem(LOCALE_KEY_NAME);
  const sessionConfig = config.session;

  return sessionConfig.locales.indexOf(locale) !== -1 ? locale : sessionConfig.defaultLocale;
}

export function getTranslationProviders(): any[] {
  const locale = getCurrentLocale();
  // @todo: uncomment it and use some cli to generate messages.*.xlf (and also append new) from the ../../messages.xlf
  // const translations = require(`./messages.${locale}.xlf`);
  const translations = require(`./messages.xlf`);

  return [
    { provide: TRANSLATIONS, useValue: translations },
    { provide: TRANSLATIONS_FORMAT, useValue: 'xlf' },
    { provide: LOCALE_ID, useValue: locale },
  ];
}
