import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/forms';
import '@angular/http';
import '@angular/router';

import '@angularclass/hmr';

import 'rxjs';
import 'lodash';
import 'inflection';

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';

import 'photoswipe';
import 'photoswipe/dist/photoswipe-ui-default';
import 'photoswipe/dist/photoswipe.css';
import 'photoswipe/dist/default-skin/default-skin.css';

import 'swiper';
import 'swiper/dist/css/swiper.css';

import 'tether-drop';
import 'app/style/vendor/tether-drop/drop-theme.scss';

import 'ng2-slim-loading-bar';
import 'ng2-slim-loading-bar/style.css';

import 'braintree-web';

if ('prod' === ENV) {
  // Production
} else {
  // Development
}
