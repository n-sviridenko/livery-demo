import { Component, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'l-app',
  templateUrl: './app.component.html',
})
export class AppComponent {
  public constructor(public viewContainerRef: ViewContainerRef) {}
}
