import { Routes, RouterModule } from '@angular/router';

import { GuestGuard } from 'app/core';
import { LoginComponent, PageNotFoundComponent } from 'app/base';

export function loadTrips() {
  return new Promise((resolve) => {
    (require as any).ensure([], (require) => {
      resolve(require('./trip').TripModule);
    });
  });
}

export const routes: Routes = [
  {
    path: 'trips',
    loadChildren: loadTrips,
  },
  {
    path: 'login',
    canActivate: [GuestGuard],
    component: LoginComponent,
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

export const routing = RouterModule.forRoot(routes);
