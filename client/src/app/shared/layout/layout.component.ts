import { Subscription } from 'rxjs';
import { ModalDirective } from 'ng2-bootstrap';
import {
  OnInit,
  Component,
  OnDestroy,
  ViewChild,
} from '@angular/core';

import { User } from 'app/model';
import {
  SharedService,
  SessionService,
  SharedMessageInterface,
} from 'app/core';

const defaultCss = require('./layout-default.component.scss');
const fixedCss = require('./layout-fixed.component.scss');

const css = {
  'default': defaultCss,
  fixed: fixedCss,
};

@Component({
  selector: 'l-layout',
  templateUrl: './layout.component.html',
})
export class LayoutComponent implements OnInit, OnDestroy {
  @ViewChild('authModal')
  public authModal: ModalDirective;

  public user: User;

  public pageOptions;

  private messageListener: Subscription;

  private userChangeListener: Subscription;

  private changesListener: Subscription;

  public constructor(
    private sharedService: SharedService,
    private sessionService: SessionService
  ) {}

  public get css() {
    return css[this.pageOptions.scheme];
  }

  public ngOnInit() {
    this.changesListener = this.sharedService.changes.subscribe(this.onChanges.bind(this));
    this.messageListener = this.sharedService.message.subscribe(this.onMessage.bind(this));
    this.userChangeListener = this.sessionService.userChangeEvent.subscribe(this.onUserChange.bind(this));
  }

  public ngOnDestroy() {
    this.messageListener.unsubscribe();
    this.userChangeListener.unsubscribe();
    this.changesListener.unsubscribe();
  }

  private onChanges(data: any) {
    this.pageOptions = data.pageOptions;
  }

  private onMessage(message: SharedMessageInterface) {
    if (message.action === 'show_auth_form') {
      this.authModal.show();
    }
  }

  private onUserChange(user: User) {
    this.user = user;
  }
}
