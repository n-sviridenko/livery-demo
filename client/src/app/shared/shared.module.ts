import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MomentModule } from 'angular2-moment';
import { TabsModule } from 'ng2-bootstrap/tabs';
import { AlertModule } from 'ng2-bootstrap/alert';
import { ModalModule } from 'ng2-bootstrap/modal';
import { TooltipModule } from 'ng2-bootstrap/tooltip';
import { HttpModule, JsonpModule } from '@angular/http';
import { DropdownModule } from 'ng2-bootstrap/dropdown';
import { TypeaheadModule } from 'ng2-bootstrap/typeahead';
import { PaginationModule } from 'ng2-bootstrap/pagination';
import { ProgressbarModule } from 'ng2-bootstrap/progressbar';

import { LayoutComponent } from './layout';
import { ThumbnailComponent } from './image';
import { LocationAutocompleteComponent } from './location';
import {
  SafePipe,
  CurrencyPipe,
  MultilinePipe,
  NumberFormatPipe,
} from './pipes';
import {
  DropComponent,
  FlashComponent,
  FooterComponent,
  HeaderComponent,
  LoadingComponent,
  UserbarComponent,
  LoadableComponent,
  SettingsComponent,
  FlashListComponent,
  HeaderHomeComponent,
  StepActionsComponent,
  SmallAuthFormComponent,
  LoadingWrapperComponent,
} from './partial';
import {
  FloatComponent,
  RequiredComponent,
  FormErrorComponent,
  DatepickerDirective,
  DatepickerComponent,
  DateSelectComponent,
  ControlErrorComponent,
  ResponseErrorComponent,
  ControlWrapperDirective,
  DatetimepickerComponent,
} from './form';

const bootstrap = [
  // angular
  CommonModule,
  FormsModule,
  RouterModule,
  HttpModule,
  JsonpModule,
  // bootstrap
  TabsModule,
  AlertModule,
  ModalModule,
  TooltipModule,
  DropdownModule,
  TypeaheadModule,
  PaginationModule,
  ProgressbarModule,
  // etc
  MomentModule,
];

const pipes = [
  SafePipe,
  CurrencyPipe,
  MultilinePipe,
  NumberFormatPipe,
];

const components = [
  // image
  ThumbnailComponent,
  // partial
  DropComponent,
  FlashComponent,
  FooterComponent,
  HeaderComponent,
  LoadingComponent,
  UserbarComponent,
  LoadableComponent,
  SettingsComponent,
  FlashListComponent,
  HeaderHomeComponent,
  StepActionsComponent,
  SmallAuthFormComponent,
  LoadingWrapperComponent,
  // form
  FloatComponent,
  RequiredComponent,
  FormErrorComponent,
  DatepickerDirective,
  DatepickerComponent,
  DateSelectComponent,
  ControlErrorComponent,
  ResponseErrorComponent,
  ControlWrapperDirective,
  DatetimepickerComponent,
  // other
  LayoutComponent,
  LocationAutocompleteComponent,
];

@NgModule({
  imports: [
    ...bootstrap,
  ],
  declarations: [
    ...pipes,
    ...components,
  ],
  exports: [
    ...pipes,
    ...components,
    ...bootstrap,
  ],
})
export class SharedModule {}
