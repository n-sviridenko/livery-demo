import * as _ from 'lodash';
import * as moment from 'moment';
import * as Pikaday from 'pikaday';
import {
  Input,
  Output,
  Directive,
  OnDestroy,
  ElementRef,
  EventEmitter,
  HostListener
} from '@angular/core';

import { SessionService } from 'app/core';

import './datepicker.directive.scss';

@Directive({
  selector: '[lDatepicker]',
})
export class DatepickerDirective implements OnDestroy {
  @Output('lDatepickerChange') // tslint:disable-line:no-output-rename
  public change = new EventEmitter();

  private el: HTMLInputElement;

  private datepicker: Pikaday;

  private _value: moment.Moment;

  private _options: Pikaday.PikadayOptions = {};

  public constructor(
    el: ElementRef,
    private sessionService: SessionService
  ) {
    this.el = el.nativeElement;

    this.initDatepicker();
  }

  @Input('lDatepicker')
  public set value(value: moment.Moment) {
    this._value = value;

    const date = value ? new Date(value.format()) : null;

    this.datepicker.setDate(date, false);
  }

  @Input('lDatepickerOptions')
  public set options(options: Pikaday.PikadayOptions) {
    if (!options || _.isEqual(options, this._options)) {
      return;
    }

    this._options = options;

    this.datepicker.config(options);
  }

  public ngOnDestroy() {
    this.datepicker.destroy();
  }

  @HostListener('keyup.Backspace')
  public cleanDate() {
    this.datepicker.hide();

    setTimeout(() => {
      this.el.blur();
    }, 0);

    this.change.emit(null);
  }

  private initDatepicker() {
    const options = {
      i18n: this.generateI18n(),
      field: this.el,
      firstDay: this.getLocaleData().firstDayOfWeek(),
      onSelect: this.handleChange.bind(this),
    };

    this.datepicker = new Pikaday(Object.assign({}, this._options, options));
  }

  // @todo: translate also previousMonth and nextMonth
  private generateI18n(): Pikaday.PikadayI18nConfig {
    const localeData = this.getLocaleData();

    const months = [];
    const weekdays = [];
    const weekdaysShort = [];

    _.range(0, 12).forEach((i) => {
      const datetime = moment().month(i).startOf('month');

      months.push(localeData.months(datetime));
    });

    _.range(0, 7).forEach((i) => {
      const datetime = moment().weekday(i);

      weekdays.push(localeData.weekdays(datetime));
      weekdaysShort.push(localeData.weekdaysMin(datetime));
    });

    return {
      months,
      weekdays,
      weekdaysShort,
      previousMonth: null,
      nextMonth: null,
    };
  }

  private getLocaleData(): moment.Locale {
    return moment().locale(this.sessionService.getLocale()).localeData();
  }

  private handleChange() {
    const date = this.datepicker.getDate();

    let value;

    if (this._value) {
      // we need to keep timezone of the value
      // @todo: find more elegant solution
      // `moment(date).utcOffset(this._value.utcOffset())` doesn't work
      value = moment(this._value)
        .hour(date.getHours())
        .minute(date.getMinutes())
        .second(date.getSeconds())
        .date(date.getDate())
        .month(date.getMonth())
        .year(date.getFullYear())
      ;
    } else {
      value = moment(date);
    }

    if (`${value}` !== `${this._value}`) {
      this.change.emit(value);
    }
  }
}
