import * as moment from 'moment';
import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

const controlValueAccessor: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DateSelectComponent),
  multi: true,
};

// @todo: locale dependent date and input positions
@Component({
  selector: 'l-date-select',
  templateUrl: './date-select.component.html',
  providers: [controlValueAccessor],
})
export class DateSelectComponent implements ControlValueAccessor {
  @Input() public inputId: string;
  public years: Array<number>;
  public months: Array<number>;
  public monthNames: Array<string>;
  public days: Array<number>;

  private date: moment.Moment = null;

  public constructor() {
    const year = moment().year();

    this.years = _.range(year - 18, year - 101, -1);
    this.months = _.range(1, 12);
    this.monthNames = this.months.map(month => moment([year, month, 1]).format('MMMM'));
    this.days = _.range(1, 31);
  }

  public get value(): moment.Moment {
    return this.date;
  };

  public set value(rawValue: moment.Moment) {
    const value = this.normalizeValue(rawValue);

    if (moment.isMoment(value) ? !value.isSame(this.date) : value !== this.date) {
      this.date = value;
      this.onChangeCallback(value);
    }
  }

  public get year(): number {
    return moment.isMoment(this.date) ? this.date.year() : 0;
  }

  public set year(year: number) {
    if (moment.isMoment(this.date)) {
      this.date.year(year);
    }
  }

  public get month(): number {
    return moment.isMoment(this.date) ? this.date.month() : 0;
  }

  public set month(month: number) {
    if (moment.isMoment(this.date)) {
      this.date.month(month);
    }
  }

  public get day(): number {
    return moment.isMoment(this.date) ? this.date.date() : 0;
  }

  public set day(day: number) {
    if (moment.isMoment(this.date)) {
      this.date.date(day);
    }
  }

  public writeValue(rawValue: moment.Moment) {
    const value = this.normalizeValue(rawValue);

    if (value !== this.date) {
      this.date = value;
    }
  }

  public registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  public registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  private normalizeValue(rawValue: moment.Moment) {
    return moment.isMoment(rawValue) ? rawValue : moment([this.years[0], this.months[0], this.days[0]]);
  }

  private onTouchedCallback: () => void = () => undefined;
  private onChangeCallback: (_: any) => void = () => undefined;
}
