import { Component, Input } from '@angular/core';

import { ApiErrorResponse } from 'app/core';

@Component({
  selector: 'l-response-error',
  templateUrl: './response-error.component.html',
})
export class ResponseErrorComponent {
  @Input()
  public error: ApiErrorResponse;
}
