import * as _ from 'lodash';
import * as moment from 'moment';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import {
  Input,
  Component,
  ViewChild,
  forwardRef,
  ElementRef,
} from '@angular/core';

import { SessionService } from 'app/core';

const controlValueAccessor: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DatetimepickerComponent),
  multi: true,
};

const css = require('./datetimepicker.component.scss');

@Component({
  selector: 'l-datetimepicker',
  templateUrl: './datetimepicker.component.html',
  providers: [controlValueAccessor],
})
export class DatetimepickerComponent implements ControlValueAccessor {
  @Input()
  public inputId: string;

  @Input()
  public className: string = '';

  @Input()
  public minDate: moment.Moment;

  @ViewChild('inputEl')
  public inputEl: ElementRef;

  public date: moment.Moment = null;

  public hours: Array<number>;

  public minutes: Array<number>;

  public css = css;

  private _hour: number = 0;

  private _minute: number = 0;

  public constructor(private sessionService: SessionService) {
    this.hours = _.range(24);
    this.minutes = _.range(0, 59, 5);
  }

  public get value(): moment.Moment {
    if (!this.date) {
      return null;
    }

    return moment(this.date)
      .hour(this._hour)
      .minute(this._minute)
    ;
  };

  public set value(value: moment.Moment) {
    if (`${value}` === `${this.value}`) {
      return;
    }

    if (!value) {
      this.resetValue();
    } else {
      this.date = moment(value).startOf('day');
      this._hour = value.hour();
      this._minute = value.minute();
    }
  }

  public get hour(): string {
    return `${this._hour}`;
  }

  public set hour(rawHour: string) {
    const hour = parseInt(rawHour, 10);

    if (this._hour === hour) {
      return;
    }

    this._hour = hour;

    this.emitChanges();
  }

  public get minute(): string {
    return `${this._minute}`;
  }

  public set minute(rawMinute: string) {
    const minute = parseInt(rawMinute, 10);

    if (this._minute === minute) {
      return;
    }

    this._minute = minute;

    this.emitChanges();
  }

  public get dateFormat(): string {
    const locale = this.sessionService.getLocale();

    return moment()
      .locale(locale)
      .localeData()
      .longDateFormat('L')
    ;
  }

  public get minDateNormalized(): Date {
    return this.minDate ? this.minDate.toDate() : null;
  }

  public focus() {
    this.inputEl.nativeElement.focus();
  }

  public onBlur() {
    this.onTouchedCallback();
  }

  public writeValue(value: moment.Moment) {
    this.value = value;
  }

  public registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  public registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  public onDateChange(date: moment.Moment) {
    if (`${this.date}` !== `${date}`) {
      if (!date) {
        this.resetValue();
      } else {
        this.date = date;
      }

      this.emitChanges();
    }
  }

  private resetValue() {
    this.date = null;
    this._hour = 0;
    this._minute = 0;
  }

  private emitChanges() {
    this.onChangeCallback(this.value);
  }

  private onTouchedCallback: () => void = () => undefined;

  private onChangeCallback: (_: any) => void = () => undefined;
}
