import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import { FloatMessage, SessionService } from 'app/core';

const controlValueAccessor: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FloatComponent),
  multi: true,
};

const css = require('./float.component.scss');

@Component({
  selector: 'l-float',
  templateUrl: './float.component.html',
  providers: [controlValueAccessor],
})
export class FloatComponent implements ControlValueAccessor {
  @Input()
  public inputId: string;

  @Input()
  public className: string = '';

  @Input()
  public inputClassName: string = '';

  public value: FloatMessage = new FloatMessage(); // default value

  public css = css;

  public constructor(private sessionService: SessionService) {}

  public get integer(): string {
    return this.value.integer;
  }

  public set integer(integer: string) {
    this.value.integer = integer;

    this.onChangeCallback(this.value);
  }

  public get fraction(): string {
    return this.value.fraction;
  }

  public set fraction(fraction: string) {
    this.value.fraction = fraction;

    this.onChangeCallback(this.value);
  }

  // @todo: find a more elegant solution
  public get separator(): string {
    const formatter = new Intl.NumberFormat(this.sessionService.getLocale(), { style: 'decimal' });
    const formatted = formatter.format(1.1);

    return formatted.replace(/[^\.\,]/g, '');
  }

  public writeValue(value: FloatMessage) {
    if (value) {
      this.value = value;
    }
  }

  public registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  public registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  private onTouchedCallback: () => void = () => undefined;
  private onChangeCallback: (_: any) => void = () => undefined;
}
