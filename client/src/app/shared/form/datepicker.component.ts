import * as moment from 'moment';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import {
  Input,
  Component,
  ViewChild,
  ElementRef,
  forwardRef,
} from '@angular/core';

import { SessionService } from 'app/core';

const controlValueAccessor: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DatepickerComponent),
  multi: true,
};

@Component({
  selector: 'l-datepicker',
  templateUrl: './datepicker.component.html',
  providers: [controlValueAccessor],
})
export class DatepickerComponent implements ControlValueAccessor {
  @Input()
  public inputId: string;

  @Input()
  public minDate: moment.Moment;

  @Input()
  public className: string = '';

  @ViewChild('inputEl')
  public inputEl: ElementRef;

  public date: moment.Moment = null;

  public constructor(private sessionService: SessionService) {}

  public get value(): moment.Moment {
    return this.date;
  };

  public set value(value: moment.Moment) {
    if (moment.isMoment(value) ? !value.isSame(this.date) : value !== this.date) {
      this.date = value;
      this.onChangeCallback(value);
    }
  }

  public get minDateNormalized(): Date {
    return this.minDate ? this.minDate.toDate() : null;
  }

  public get dateFormat(): string {
    const locale = this.sessionService.getLocale();

    return moment()
      .locale(locale)
      .localeData()
      .longDateFormat('L')
    ;
  }

  public onBlur() {
    this.onTouchedCallback();
  }

  public writeValue(value: moment.Moment) {
    if (value !== this.date) {
      this.date = value;
    }
  }

  public registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  public registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  public focus() {
    this.inputEl.nativeElement.focus();
  }

  public onDateChange(date: moment.Moment) {
    this.value = date ? moment(date) : null;
  }

  private onTouchedCallback: () => void = () => undefined;
  private onChangeCallback: (_: any) => void = () => undefined;
}
