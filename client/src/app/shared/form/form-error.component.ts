import { Component, Input } from '@angular/core';

import { FormErrorList } from 'app/core';

@Component({
  selector: 'l-form-error',
  templateUrl: './form-error.component.html',
})
export class FormErrorComponent {
  @Input()
  public list: FormErrorList;

  public get errors(): string[] {
    return this.list ? this.list.errors : [];
  }
}
