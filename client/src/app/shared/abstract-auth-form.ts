import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';

import { AbstractForm } from './abstract-form';
import {
  AuthService,
  AuthRequest,
  ApiErrorResponse,
} from 'app/core';

// @todo: move into core, refactor using core form
export abstract class AbstractAuthForm extends AbstractForm {
  public model: AuthRequest;

  protected authService: AuthService;

  protected newModel(): Observable<AuthRequest> {
    return Observable.of({
      username: '',
      password: '',
    });
  }

  protected getRequest(): Observable<any> {
    return this.authService.login(this.model);
  }

  protected handleError(error: ApiErrorResponse, form: FormGroup) {
    this.resetFlashes();
    this.handleErrorFlash(error);

    this.model.password = '';
  }
}
