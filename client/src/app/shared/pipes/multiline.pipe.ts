import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lMultiline',
})
export class MultilinePipe implements PipeTransform {
  public transform(value: string): string {
    return `${value}`.replace(/\r\n|\n\r|\r|\n/g, '<br>');
  }
}
