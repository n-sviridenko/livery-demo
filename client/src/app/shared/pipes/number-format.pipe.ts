import { Pipe, PipeTransform } from '@angular/core';

import { SessionService } from 'app/core';

@Pipe({
  name: 'lNumberFormat',
})
export class NumberFormatPipe implements PipeTransform {
  public constructor(private sessionService: SessionService) {}

  public transform(value: number): string {
    const options = { style: 'decimal' };
    const formatter = new Intl.NumberFormat(this.sessionService.getLocale(), options);

    return formatter.format(value);
  }
}
