import { Pipe, PipeTransform } from '@angular/core';

import { SessionService } from 'app/core';

@Pipe({
  name: 'lCurrency',
})
export class CurrencyPipe implements PipeTransform {
  public constructor(private sessionService: SessionService) {}

  public transform(value: number, currencyCode: string, options: any = {}): string {
    const resolvedOptions = Object.assign({ style: 'currency', currency: currencyCode }, options);
    const formatter = new Intl.NumberFormat(this.sessionService.getLocale(), resolvedOptions);

    return formatter.format(value);
  }
}
