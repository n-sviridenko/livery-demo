import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'lSafe',
})
export class SafePipe implements PipeTransform {
  public constructor(private sanitizer: DomSanitizer) {}

  public transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
