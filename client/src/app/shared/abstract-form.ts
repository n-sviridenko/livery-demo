import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, NgForm, ValidatorFn } from '@angular/forms';

import {
  FlashTypes,
  FormManager,
  FlashService,
  ApiErrorResponse,
} from 'app/core';

/**
 * @deprecated: use AbstractForm from core instead
 * @todo: move flashes and other messages stuff outside into subscribers
 */
export abstract class AbstractForm implements OnInit, AfterViewInit {
  public model: any;

  public sending: boolean = false;

  public form: NgForm;

  protected flashService: FlashService;

  public ngOnInit() {
    this.reset();
  }

  public ngAfterViewInit() {
    // I don't know why, but I can't access to form controls before
    setTimeout(() => {
      _.each(this.rules, (validators: ValidatorFn[], controlName: string) => {
        this.form.form.get(controlName).setValidators(validators);
      });
    }, 0);
  }

  public onSubmit() {
    this.sending = true;

    this.getRequest()
      .subscribe(
        (data: any) => {
          this.sending = false;

          this.handleResponse(data, this.form.form);
        },
        (error: ApiErrorResponse) => {
          this.sending = false;

          this.handleError(error, this.form.form);
        }
      )
    ;
  }

  protected get rules(): any {
    return {};
  }

  // @todo: remove reset and replace by afterSubmit without resetting
  protected reset() {
    this.model = null;

    this.newModel().subscribe(
      (model: any) => {
        this.model = model;
      },
      () => {
        // @todo: handle error
      }
    );
  }

  protected handleError(error: ApiErrorResponse, form: FormGroup) {
    if (error.code === 401) {
      return;
    }

    this.resetFlashes();
    this.handleErrorFlash(error);

    if (error.errors) {
      FormManager.applyError(form, error);
    }
  }

  protected handleErrorFlash(error: ApiErrorResponse) {
    const message = this.getErrorMessage(error);

    if (message) {
      this.addErrorFlash(message);
    }
  }

  protected getErrorMessage(error: ApiErrorResponse): string {
    return _.get(error, 'errors.errors.0', error.message);
  }

  protected addErrorFlash(message: string) {
    this.flashService.add(FlashTypes.FLASH_TYPE_ERROR, message);
  }

  protected handleResponse(data: any, form: FormGroup) {
    this.resetForm(data, form);

    this.resetFlashes();
    this.handleSuccessFlash(data);
  }

  protected resetForm(data: any, form: FormGroup) {
    // for errors
    form.reset();
    // for clear model
    this.reset();
  }

  protected handleSuccessFlash(data: any) {
    const message = this.getSuccessMessage(data);

    if (message) {
      this.addSuccessFlash(message);
    }
  }

  protected getSuccessMessage(data: any): string {
    return null;
  }

  protected addSuccessFlash(message: string) {
    this.flashService.add(FlashTypes.FLASH_TYPE_SUCCESS, message);
  }

  protected resetFlashes() {
    this.flashService.clear(FlashTypes.FLASH_TYPE_SUCCESS);
    this.flashService.clear(FlashTypes.FLASH_TYPE_ERROR);
  }

  protected abstract newModel(): Observable<any>;
  protected abstract getRequest(): Observable<any>;
}
