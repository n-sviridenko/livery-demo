import * as _ from 'lodash';
import { Component, Input } from '@angular/core';

import { Image } from 'app/model';

@Component({
  selector: 'l-thumbnail',
  templateUrl: './thumbnail.component.html',
})
export class ThumbnailComponent {
  @Input()
  public image: Image;

  @Input()
  public type: string;

  @Input()
  public size: number;

  public get url() {
    const url = this.image ? this.image.getThumbnailUrl(this.type) : null;

    return url || this.getNoImageUrl();
  }

  private getNoImageUrl() {
    const name = _.kebabCase(this.type);

    return `/assets/images/no-image/${name}.png`;
  }
}
