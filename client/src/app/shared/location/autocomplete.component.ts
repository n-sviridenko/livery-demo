import { Observable } from 'rxjs';
import { TypeaheadMatch } from 'ng2-bootstrap/typeahead';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import {
  Input,
  Component,
  ViewChild,
  ElementRef,
  forwardRef,
  ApplicationRef,
} from '@angular/core';

import { LocationPrediction } from 'app/model';

const controlValueAccessor: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => LocationAutocompleteComponent),
  multi: true,
};

const css = require('./autocomplete.component.scss');

@Component({
  selector: 'l-location-autocomplete',
  templateUrl: './autocomplete.component.html',
  providers: [controlValueAccessor],
})
export class LocationAutocompleteComponent implements ControlValueAccessor {
  @Input()
  public inputId: string;

  @Input()
  public placeholder: string;

  @Input()
  public className: string = '';

  @ViewChild('inputEl')
  public inputEl: ElementRef;

  public input: string = '';

  public dataSource: Observable<any>;

  public typeaheadLoading: boolean = false;

  public typeaheadNoResults: boolean = false;

  public css: any = css;

  private autocompleteService: google.maps.places.AutocompleteService;

  private selected: LocationPrediction;

  public get value(): LocationPrediction {
    return this.selected;
  };

  public set value(selected: LocationPrediction) {
    if (selected !== this.selected) {
      this.selected = selected;
      this.onChangeCallback(selected);
    }
  }

  public constructor(private appRef: ApplicationRef) {
    this.autocompleteService = new google.maps.places.AutocompleteService();
    this.dataSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.input);
    }).mergeMap(
      (token:string) => this.getSource(token)
    );
  }

  public getSource(token: string): Observable<any> {
    return Observable.create((observer: any) => {
      // @todo: move it into the location service
      this.autocompleteService.getQueryPredictions(
        { input: token },
        (predictions: google.maps.places.AutocompletePrediction[], status: google.maps.places.PlacesServiceStatus) => {
          const results = [];

          if (status === google.maps.places.PlacesServiceStatus.OK) {
            predictions.forEach((prediction) => {
              if (prediction.place_id) {
                const data = {
                  placeId: prediction.place_id,
                  name: prediction.description,
                  types: prediction.types,
                };

                results.push(new LocationPrediction(data));
              }
            });
          }

          observer.next(results);

          // because angular doesn't know it
          this.appRef.tick();
        }
      );
    });
  }

  // @todo: display it into view
  public changeTypeaheadLoading(e: boolean) {
    this.typeaheadLoading = e;
  }

  // @todo: display it into view
  public changeTypeaheadNoResults(e: boolean) {
    this.typeaheadNoResults = e;
  }

  public typeaheadOnSelect(e: TypeaheadMatch) {
    this.value = e.item;
  }

  public inputChange(input: string) {
    if (input !== this.input) {
      if (this.value !== null) {
        this.value = null;
      }

      this.input = input;
    }
  }

  public focus() {
    this.inputEl.nativeElement.focus();
  }

  public onBlur() {
    this.onTouchedCallback();
  }

  public writeValue(selected: LocationPrediction) {
    if (selected !== this.selected) {
      this.selected = selected;
      this.input = selected ? selected.name : '';
    }
  }

  public registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  public registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  private onTouchedCallback: () => void = () => undefined;
  private onChangeCallback: (_: any) => void = () => undefined;
}
