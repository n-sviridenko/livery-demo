import { Component, Input, Output, EventEmitter } from '@angular/core';

import { FlashListItem } from 'app/core';

@Component({
  selector: 'l-flash',
  templateUrl: './flash.component.html',
})
export class FlashComponent {
  @Input()
  public type: string;

  @Input()
  public messages: FlashListItem;

  @Output()
  public clear: EventEmitter<string> = new EventEmitter<string>();

  public close() {
    this.clear.emit(this.type);
  }
}
