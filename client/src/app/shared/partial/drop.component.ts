import * as Drop from 'tether-drop';
import {
  Component,
  AfterViewInit,
  ViewChild,
  ElementRef,
  OnDestroy,
  Input,
} from '@angular/core';

@Component({
  selector: 'l-drop',
  templateUrl: './drop.component.html',
})
export class DropComponent implements AfterViewInit, OnDestroy {
  @ViewChild('content')
  public content: ElementRef;

  @Input()
  public target: HTMLElement;

  @Input()
  public options: Drop.IDropOptions = {};

  private drop: Drop;

  public ngAfterViewInit() {
    const options: Drop.IDropOptions = Object.assign({
      openOn: 'click',
      classes: 'drop-theme-arrows',
      content: this.content.nativeElement,
      target: this.target,
    }, this.options);

    this.drop = new Drop(options);
  }

  public ngOnDestroy() {
    this.drop.destroy();
  }

  public open() {
    this.drop.open();
  }

  public close() {
    this.drop.close();
  }
}
