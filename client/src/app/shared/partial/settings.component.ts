import { Component, OnInit } from '@angular/core';

import { SessionService } from 'app/core';
import { Locale, Currency } from 'app/model';

@Component({
  selector: 'l-settings',
  templateUrl: './settings.component.html',
})
export class SettingsComponent implements OnInit {
  public locales: Locale[];
  public currencies: Currency[];

  constructor(private sessionService: SessionService) {}

  public ngOnInit() {
    this.locales = this.sessionService.getLocales();
    this.currencies = this.sessionService.getCurrencies();
  }

  get locale(): string {
    return this.sessionService.getLocale();
  }

  set locale(locale: string) {
    this.sessionService.setLocale(locale);

    location.reload();
  }

  get currency(): string {
    return this.sessionService.getCurrency();
  }

  set currency(currency: string) {
    this.sessionService.setCurrency(currency);

    location.reload();
  }
}
