import {
  Component,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'l-step-actions',
  templateUrl: './step-actions.component.html',
})
export class StepActionsComponent {
  @Input()
  public stepsCount: number;

  @Input()
  public step: number;

  @Input()
  public isNextStepAvailable: boolean;

  @Output()
  public stepChange: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  public finish: EventEmitter<any> = new EventEmitter<any>();

  public get isFirstStep(): boolean {
    return this.step <= 1;
  }

  public get isLastStep(): boolean {
    return this.step >= this.stepsCount;
  }

  public prev() {
    this.stepChange.emit(this.step - 1);
  }

  public next() {
    this.stepChange.emit(this.step + 1);
  }

  public doFinish() {
    this.finish.emit();
  }
}
