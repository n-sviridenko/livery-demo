import * as _ from 'lodash';
import { Component } from '@angular/core';

import { FlashService } from 'app/core';

@Component({
  selector: 'l-flash-list',
  templateUrl: './flash-list.component.html',
})
export class FlashListComponent {
  public constructor(private flashService: FlashService) {}

  public get flashes(): Array<any> {
    const flashes = this.flashService.peekAll();

    return _.map(<any>flashes, (messages, type) => ({type, messages}));
  }

  public clear(type: string) {
    this.flashService.clear(type);
  }
}
