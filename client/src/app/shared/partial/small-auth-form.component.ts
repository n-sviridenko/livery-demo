import { FormGroup, NgForm } from '@angular/forms';
import {
  Component,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';

import { AuthService, FlashService } from 'app/core';
import { AbstractAuthForm } from '../abstract-auth-form';

@Component({
  selector: 'l-small-auth-form',
  templateUrl: './small-auth-form.component.html'
})
export class SmallAuthFormComponent extends AbstractAuthForm {
  @ViewChild('form')
  public form: NgForm;

  @Output()
  public logged = new EventEmitter();

  public error: string;

  public constructor(
    protected authService: AuthService,
    protected flashSerivce: FlashService
  ) {
    super();
  }

  protected handleResponse(data: any, form: FormGroup) {
    super.handleResponse(data, form);

    this.logged.emit();
  }

  protected addErrorFlash(message: string) {
    this.error = message;
  }

  protected resetFlashes() {
    this.error = null;
  }
}
