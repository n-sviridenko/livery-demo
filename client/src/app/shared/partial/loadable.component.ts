import {
  Component,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'l-loadable',
  templateUrl: './loadable.component.html',
})
export class LoadableComponent {
  @Input()
  public loading: boolean;

  @Input()
  public displayContent: boolean = false;

  @Input()
  public fullSize: boolean = false;

  @Input()
  public error: any;

  @Input()
  public allowReload: boolean = false;

  @Output()
  public reload: EventEmitter<any> = new EventEmitter<any>();

  public triggerReload() {
    this.reload.emit();
  }
}
