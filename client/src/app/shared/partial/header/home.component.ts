import { Input, Component } from '@angular/core';

import { User } from 'app/model';

const css = require('./home.component.scss');

@Component({
  selector: 'l-header-home',
  templateUrl: './home.component.html',
})
export class HeaderHomeComponent {
  @Input()
  public user: User;

  public css = css;
}
