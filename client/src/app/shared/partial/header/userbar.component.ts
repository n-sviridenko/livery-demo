import { Router } from '@angular/router';
import { Input, Component } from '@angular/core';

import { User } from 'app/model';
import { AuthService, SharedService } from 'app/core';

const css = require('./userbar.component.scss');

@Component({
  selector: 'l-userbar',
  templateUrl: './userbar.component.html',
})
export class UserbarComponent {
  @Input()
  public user: User;

  public css = css;

  public constructor(
    private router: Router,
    private authService: AuthService,
    private sharedService: SharedService
  ) {}

  public canLogin() {
    return !this.router.isActive('/login', false);
  }

  public logout() {
    this.authService.logout();
  }

  public showAuthForm() {
    this.sharedService.sendMessage('show_auth_form');
  }
}
