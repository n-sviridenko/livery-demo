import { Component, Input } from '@angular/core';

import { User } from 'app/model';

@Component({
  selector: 'l-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  @Input()
  public user: User;

  @Input()
  public type: string;
}
