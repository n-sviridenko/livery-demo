import { Component, Input } from '@angular/core';

@Component({
  selector: 'l-loading-wrapper',
  templateUrl: './loading-wrapper.component.html',
})
export class LoadingWrapperComponent {
  @Input() public ready: boolean;
}
