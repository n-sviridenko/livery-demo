// @todo: use a 3rd-party lib or find an easier native solution
export function unescapeHtml(html: string): string {
  var el = document.createElement('div');
  el.innerHTML = html;

  return el.childNodes.length === 0 ? '' : el.childNodes[0].nodeValue;
}
