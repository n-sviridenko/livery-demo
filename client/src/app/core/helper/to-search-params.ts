import * as _ from 'lodash';
import { URLSearchParams } from '@angular/http';

// @todo: implement using qs

function prepareQuery(data: any, prefix: string = null): Array<any> {
  const parts = _.toPairs(data)
    .filter(([, value]) => !_.isNil(value) && value !== false)
    .map(([key, value]) => {
      let safeKey = encodeURIComponent(key);
      safeKey = prefix ? `${prefix}[${safeKey}]` : safeKey;

      if (_.isObject(value) && !(value instanceof File)) {
        return prepareQuery(value, safeKey);
      }

      const safeValue = value === true ? '1' : _.toString(value);

      return [safeKey, safeValue];
    })
  ;

  return _.flatten(parts);
}

export function toSearchParams(data: any): URLSearchParams {
  const query = prepareQuery(data);
  const params = new URLSearchParams();

  for (let i = 0; i < query.length; i += 2) {
    const key = query[i];
    const value = query[i + 1];

    params.set(key, value);
  }

  return params;
}

export function toFormData(data: any): FormData {
  const query = prepareQuery(data);
  const formData = new FormData();

  for (let i = 0; i < query.length; i += 2) {
    formData.append(query[i], query[i + 1]);
  }

  return formData;
}
