import { Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';

import { User } from 'app/model';
import { APP_CONFIG } from '../core';
import { UserHydrator } from './hydrator';
import { toSearchParams } from '../helper';
import { UserConfig } from './user.config';
import { ApiService, UserServiceInterface } from '../core';
import { UserGetMessage } from './message';

@Injectable()
export class UserService implements UserServiceInterface {
  private config: UserConfig;

  public constructor(
    @Inject(APP_CONFIG) config: any,
    private apiService: ApiService
  ) {
    this.config = new UserConfig(config.user);
  }

  public getMaxRating(): number {
    return this.config.maxRating;
  }

  public getUserById(id: number): Observable<User> {
    const message = new UserGetMessage();
    message.id = id;

    return this.getUser(message);
  }

  public getUser(message: UserGetMessage): Observable<User> {
    return this.apiService
      .get(`users/${message.id}`, { search: toSearchParams(message.build()) })
      .map(data => UserHydrator.hydrate(new User(), data))
    ;
  }
}
