import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { Trip } from 'app/model';
import { ApiService } from '../core';
import { TripMessage } from './message';
import { TripHydrator } from './hydrator';

@Injectable()
export class TripService {
  public constructor(private apiService: ApiService) {}

  public getTrip(id: number): Observable<Trip> {
    return this.apiService
      .get(`trips/${id}`)
      .map(data => TripHydrator.hydrate(new Trip(), data))
    ;
  }

  public postTrip(message: TripMessage): Observable<Trip> {
    return this.apiService
      .post('trips', { trip: message.build() })
      .map(data => TripHydrator.hydrate(new Trip(), data))
    ;
  }
}
