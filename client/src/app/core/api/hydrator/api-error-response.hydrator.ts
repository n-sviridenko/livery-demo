import * as _ from 'lodash';

import { FormErrorList } from '../../form';
import { ApiErrorResponse } from '../../core';

export class ApiErrorResponseHydrator {
  public static hydrate(object: ApiErrorResponse, data): ApiErrorResponse {
    object.code = data.code || 0;
    object.message = data.message || '';

    if (data.errors) {
      object.errors = ApiErrorResponseHydrator.createFormErrorList(data.errors);
    }

    return object;
  }

  private static createFormErrorList(data: any): FormErrorList {
    const list = new FormErrorList();
    list.errors = data.errors || [];

    _.forEach(data.children, (child, fieldName) => {
      if (!_.isEmpty(child)) {
        list.addChild(fieldName, ApiErrorResponseHydrator.createFormErrorList(child));
      }
    });

    return list;
  }
}
