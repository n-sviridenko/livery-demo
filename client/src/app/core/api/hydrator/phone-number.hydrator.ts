import { PhoneNumber } from 'app/model';

export class PhoneNumberHydrator {
  public static hydrate(object: PhoneNumber, data): PhoneNumber {
    object.countryCode = data.countryCode;
    object.nationalNumber = data.nationalNumber;
    object.localized = data.localized;

    return object;
  }
}
