import {
  Area,
  Country,
  Location,
  AbstractLocation,
  CountryAwareLocation,
} from 'app/model';

export class LocationHydrator {
  public static hydrate(object, data): any {
    if (object instanceof AbstractLocation) {
      LocationHydrator.hydrateAbstractLocation(object, data);
    }

    if (object instanceof CountryAwareLocation) {
      LocationHydrator.hydrateCountryAwareLocation(object, data);
    }

    switch (true) {
      case (object instanceof Location):
        LocationHydrator.hydrateLocation(object, data);
        break;
      case (object instanceof Area):
        LocationHydrator.hydrateArea(object, data);
        break;
      case (object instanceof Country):
        LocationHydrator.hydrateCountry(object, data);
        break;
    }

    return object;
  }

  private static hydrateAbstractLocation(object: AbstractLocation, data) {
    object.id = data.id;
    object.type = data.type;
    object.placeId = data.placeId;
  }

  private static hydrateCountryAwareLocation(object: CountryAwareLocation, data) {
    object.lat = data.lat;
    object.lng = data.lng;
    object.formattedAddress = data.formattedAddress;
  }

  private static hydrateLocation(object: Location, data) {
    object.area = data.area ? LocationHydrator.hydrate(new Area(), data.area) : null;
  }

  private static hydrateArea(object: Area, data) {
    object.name = data.name;
    object.slug = data.slug;
    object.country = data.country ? LocationHydrator.hydrate(new Country(), data.country) : null;
  }

  private static hydrateCountry(object: Country, data) {
    object.code = data.code;
    object.name = data.name;
    object.slug = data.slug;
  }
}
