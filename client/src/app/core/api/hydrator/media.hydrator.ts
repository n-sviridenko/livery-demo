import { ThumbnailHydrator } from './thumbnail.hydrator';
import {
  Image,
  Thumbnail,
  AbstractMedia,
} from 'app/model';

export class MediaHydrator {
  public static hydrate(object, data): any {
    if (object instanceof AbstractMedia) {
      MediaHydrator.hydrateAbstractMedia(object, data);
    }

    switch (true) {
      case (object instanceof Image):
        MediaHydrator.hydrateImage(object, data);
        break;
    }

    return object;
  }

  private static hydrateAbstractMedia(object: AbstractMedia, data) {
    object.id = data.id;
    object.type = data.type;
    object.url = data.url;
  }

  private static hydrateImage(object: Image, data) {
    // @todo: implement width and height in the backend part
    object.width = 0;
    object.height = 0;
    object.thumbnails = (data.thumbnails || []).map(thumbnail => ThumbnailHydrator.hydrate(new Thumbnail(), thumbnail));
  }
}
