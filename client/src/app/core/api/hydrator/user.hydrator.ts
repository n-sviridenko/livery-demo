import * as moment from 'moment';

import { MediaHydrator } from './media.hydrator';
import { PhoneNumberHydrator } from './phone-number.hydrator';
import {
  User,
  Image,
  PhoneNumber,
} from 'app/model';

export class UserHydrator {
  public static hydrate(object: User, data): User {
    object.id = data.id;
    object.firstName = data.firstName;
    object.lastName = data.lastName;
    object.locale = data.locale;
    object.picture = data.picture ? MediaHydrator.hydrate(new Image(), data.picture) : null;
    object.age = data.age;
    object.createdAt = data.createdAt ? moment.utc(data.createdAt.date) : null;
    object.lastLogin = data.lastLogin ? moment.utc(data.lastLogin.date) : null;
    object.about = data.about;
    object.email = data.email;
    object.phone = data.phone ? PhoneNumberHydrator.hydrate(new PhoneNumber(), data.phone) : null;
    object.sex = data.sex;
    object.birthdate = data.birthdate ? moment.utc(data.birthdate.date) : null;

    return object;
  }
}
