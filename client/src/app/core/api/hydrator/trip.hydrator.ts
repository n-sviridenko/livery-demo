import * as moment from 'moment';

import { Trip, User } from 'app/model';
import { LocationFactory } from '../factory';
import { UserHydrator } from './user.hydrator';
import { LocationHydrator } from './location.hydrator';

export class TripHydrator {
  public static hydrate(object: Trip, data): Trip {
    object.id = data.id;
    object.from = data.from ? LocationHydrator.hydrate(LocationFactory.create(data.from.type), data.from) : null;
    object.to = data.to ? LocationHydrator.hydrate(LocationFactory.create(data.to.type), data.to) : null;
    object.departAt = data.departAt ? moment.utc(data.departAt.date) : null;
    object.arriveAt = data.arriveAt ? moment.utc(data.arriveAt.date) : null;
    object.maxResponseDelayHourly = data.maxResponseDelayHourly;
    object.traveller = data.traveller ? UserHydrator.hydrate(new User(), data.traveller) : null;
    object.description = data.description;
    object.vehicle = data.vehicle;
    object.createdAt = data.createdAt ? moment(data.createdAt.date) : null;

    return object;
  }
}
