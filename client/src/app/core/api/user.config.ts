export interface UserConfigInterface {
  maxRating: number;
}

export class UserConfig {
  public maxRating: number;

  public constructor(config: UserConfigInterface) {
    this.maxRating = config.maxRating;
  }
}
