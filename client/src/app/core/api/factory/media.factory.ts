import { Image, Document, AbstractMedia } from 'app/model';

export class MediaFactory {
  public static create(type: string): AbstractMedia {
    switch (type) {
      case 'image':
        return new Image();
      case 'document':
        return new Document();
      default:
        throw new Error(`Could not find a media with type '${type}'.`);
    }
  }
}
