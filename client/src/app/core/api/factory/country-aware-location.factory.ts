import { CountryAwareLocation } from 'app/model';
import { LocationFactory } from './location.factory';

export class CountryAwareLocationFactory extends LocationFactory {
  public static create(type: string): CountryAwareLocation {
    if (type === 'country') {
      throw new Error(`The 'country' type isn't supported by the 'CountryAwareLocation'.`);
    }

    return <CountryAwareLocation> super.create(type);
  }
}
