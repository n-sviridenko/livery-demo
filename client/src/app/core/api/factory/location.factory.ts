import {
  Area,
  Country,
  Location,
  AbstractLocation,
} from 'app/model';

export class LocationFactory {
  public static create(type: string): AbstractLocation {
    switch (type) {
      case 'location':
        return new Location();
      case 'area':
        return new Area();
      case 'country':
        return new Country();
      default:
        throw new Error(`Could not find a location with type '${type}'.`);
    }
  }
}
