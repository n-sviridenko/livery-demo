import { MessageInterface } from './message.interface';

export const UserGetMessageFields = {
  REVIEWS: 'reviews',
  REVIEWS_STAT: 'reviews_stat',
  PROTECTED: 'protected',
};

export class UserGetMessage implements MessageInterface {
  public id: number;

  public fields: string[] = [];

  public build(): any {
    return {
      fields: this.fields,
    };
  }
}
