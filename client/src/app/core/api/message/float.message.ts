import * as _ from 'lodash';

import { MessageInterface } from './message.interface';

export class FloatMessage implements MessageInterface {
  public integer: string;

  public fraction: string;

  public static create(value: string): FloatMessage {
    const message = new FloatMessage();
    message.value = value;

    return message;
  }

  public get value(): string {
    const integer = parseInt(this.integer, 10);
    const fraction = parseInt(this.fraction, 10);

    if (isNaN(integer)) {
      return null;
    }

    return !isNaN(fraction) ? `${integer}.${fraction}` : `${integer}`;
  }

  public set value(value: string) {
    [this.integer = null, this.fraction = null] = !_.isEmpty(value) ? value.split('.', 2) : [];
  }

  public build(): any {
    return this.value;
  }
}
