import * as moment from 'moment';

import { LocationPrediction } from 'app/model';
import { MessageInterface } from './message.interface';

export class TripMessage implements MessageInterface {
  public from: LocationPrediction;

  public to: LocationPrediction;

  public departAt: moment.Moment;

  public arriveAt: moment.Moment;

  public maxResponseDelayHourly: number;

  public description?: string;

  public vehicle: string;

  public build(): any {
    return {
      from: this.from.placeId,
      to: this.to.placeId,
      departAt: this.departAt.format('YYYY-MM-DD\\THH:mm:ss'),
      arriveAt: this.arriveAt.format('YYYY-MM-DD\\THH:mm:ss'),
      maxResponseDelayHourly: this.maxResponseDelayHourly,
      description: this.description,
      vehicle: this.vehicle,
    };
  }
}
