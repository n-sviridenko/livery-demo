import * as validator from 'validator';

function alias(str: string): boolean {
  return validator.isAlphanumeric(str.replace(/-/g, ''));
}

function locationAlias(str: string): boolean {
  const errors = str.split('--').map(part => !alias(part));

  return str.length > 0 && errors.length <= 2 && _.filter(errors).length === 0;
}

export const LocationExtension = {
  name: 'location',
  language: {
    alias: 'needs to be an alias',
    locationAlias: 'needs to be an location alias',
  },
  rules: [
    {
      name: 'alias',
      validate(params, value, state, options) {
        if (!alias(value)) {
          return this.createError('location.alias', { v: value }, state, options);
        }

        return value;
      },
    },
    {
      name: 'locationAlias',
      validate(params, value, state, options) {
        if (!locationAlias(value)) {
          return this.createError('location.locationAlias', { v: value }, state, options);
        }

        return value;
      },
    },
  ]
};
