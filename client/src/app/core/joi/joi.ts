import * as _ from 'lodash';
import * as BaseJoi from 'joi';

import * as extensions from './extension';

export const Joi = _.values(extensions).reduce((joi: any, extension) => joi.extend(extension), BaseJoi);
