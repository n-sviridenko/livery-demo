import { Injectable } from '@angular/core';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { ApiService } from '../core';

@Injectable()
export class LoadingBarSubscriber {
  private requestCount = 0;

  public constructor(
    private apiService: ApiService,
    private loadingBarService: SlimLoadingBarService
  ) {
    this.apiService.beforeRequest.subscribe(this.onRequest.bind(this));
    this.apiService.afterResponse.subscribe(this.onResponse.bind(this));
    this.apiService.afterError.subscribe(this.onError.bind(this));
  }

  private onRequest(r) {
    this.incrementRequestCount();
  }

  private onResponse(r) {
    this.decrementRequestCount();
  }

  private onError() {
    this.decrementRequestCount();
  }

  private incrementRequestCount() {
    if (this.requestCount === 0) {
      this.loadingBarService.start();
    }

    this.requestCount ++;
  }

  private decrementRequestCount() {
    this.requestCount --;

    if (this.requestCount === 0) {
      this.loadingBarService.complete();
    }
  }
}
