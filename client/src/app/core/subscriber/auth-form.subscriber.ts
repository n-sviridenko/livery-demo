import { Injectable } from '@angular/core';
import { RequestMethod } from '@angular/http';

import {
  ApiService,
  SharedService,
  UnauthorizedEvent,
} from '../core';

@Injectable()
export class AuthFormSubscriber {
  public constructor(
    private apiService: ApiService,
    private sharedService: SharedService
  ) {
    this.apiService.unauthorizedEvent.subscribe(this.onUnauthorized.bind(this));
  }

  private onUnauthorized(event: UnauthorizedEvent) {
    if (event.request.method !== RequestMethod.Get) {
      this.showAuthForm();
    }
  }

  private showAuthForm() {
    this.sharedService.sendMessage('show_auth_form');
  }
}
