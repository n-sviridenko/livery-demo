import { Injectable } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';

import { FlashService } from '../core';

@Injectable()
export class FlashSubscriber {
  public constructor(
    private router: Router,
    private flashService: FlashService
  ) {
    this.router.events.subscribe(this.onRoute.bind(this));
  }

  private onRoute(event: Event) {
    if (event instanceof NavigationEnd) {
      this.flashService.clear();
    }
  }
}
