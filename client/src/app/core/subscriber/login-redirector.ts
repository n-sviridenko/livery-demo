import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { RequestMethod } from '@angular/http';
import {
  Event,
  Router,
  NavigationEnd,
  ActivatedRoute,
  DefaultUrlSerializer,
} from '@angular/router';

import { AuthGuard } from '../guard';
import {
  ApiService,
  AuthService,
  RouteGuesser,
  UnauthorizedEvent,
} from '../core';

@Injectable()
export class LoginRedirector {
  private _returnUrl: string;

  private isNavigated: boolean = true;

  private static hasAuthGuard(activatedRoute: ActivatedRoute) {
    let parent = activatedRoute;

    while (parent) {
      const canActivate: any = _.get(parent, 'routeConfig.canActivate');

      if (canActivate && canActivate.indexOf(AuthGuard) !== -1) {
        return true;
      }

      parent = parent.parent;
    }

    return false;
  }

  public constructor(
    private router: Router,
    private apiService: ApiService,
    private authService: AuthService,
    private routeGuesser: RouteGuesser
  ) {
    this.router.events.subscribe(this.onRoute.bind(this));
    this.apiService.unauthorizedEvent.subscribe(this.onUnauthorized.bind(this));
    this.authService.loginEvent.subscribe(this.onLoggedIn.bind(this));
    this.authService.logoutEvent.subscribe(this.onLoggedOut.bind(this));
  }

  public get returnUrl(): string {
    return this._returnUrl;
  }

  public set returnUrl(returnUrl: string) {
    this._returnUrl = returnUrl;

    if (returnUrl) {
      this.isNavigated = false;
    }
  }

  public redirectToLogin(returnUrl: string = null) {
    this.returnUrl = returnUrl || this.router.url;

    this.router.navigate(['/login']);
  }

  private onRoute(event: Event) {
    if (event instanceof NavigationEnd) {
      if (this.isNavigated) {
        this.returnUrl = null;
      } else {
        this.isNavigated = true;
      }
    }
  }

  private onUnauthorized(event: UnauthorizedEvent) {
    if (event.request.method === RequestMethod.Get) {
      this.redirectToLogin();
    }
  }

  private onLoggedIn() {
    if (this.returnUrl) {
      const urlTree = new DefaultUrlSerializer().parse(this.returnUrl);

      this.router.navigateByUrl(urlTree);
    }
  }

  private onLoggedOut() {
    if (LoginRedirector.hasAuthGuard(this.routeGuesser.activatedRoute)) {
      this.redirectToLogin();
    }
  }
}
