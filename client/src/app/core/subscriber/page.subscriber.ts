import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';

import { SharedService } from '../core';

const defaultPageOptions = {
  scheme: 'default',
  header: 'default',
};

@Injectable()
export class PageSubscriber {
  private initialized: boolean = false;

  public constructor(
    private router: Router,
    private sharedService: SharedService
  ) {
    this.router.events.subscribe(this.onRoute.bind(this));

    this.resetPageOptions();
  }

  private onRoute(event: Event) {
    if (event instanceof NavigationEnd) {
      if (!this.initialized) {
        this.initialized = true; // skip first event, because the options have been reset in constructor
      } else {
        this.resetPageOptions();
      }
    }
  }

  private resetPageOptions() {
    this.sharedService.setData('pageOptions', _.clone(defaultPageOptions));
  }
}
