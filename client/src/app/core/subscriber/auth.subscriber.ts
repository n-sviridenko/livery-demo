import { Injectable, Inject } from '@angular/core';

import { User } from 'app/model';
import {
  AppState,
  AuthService,
  USER_SERVICE,
  SessionService,
  UserServiceInterface,
} from '../core';

@Injectable()
export class AuthSubscriber {
  public constructor(
    @Inject(USER_SERVICE) private userService: UserServiceInterface,
    private authService: AuthService,
    private sessionService: SessionService,
    private appState: AppState
  ) {
    this.authService.loginEvent.subscribe(this.onLogin.bind(this));
    this.authService.logoutEvent.subscribe(this.onLogout.bind(this));

    this.refreshUser();
  }

  private onLogin() {
    this.refreshUser();
  }

  private onLogout() {
    this.sessionService.unsetUser();
    this.appState.set('user', null);
  }

  private refreshUser() {
    const userId = this.authService.getUserId();

    if (userId) {
      this.loadUserById(userId);
    } else {
      this.sessionService.unsetUser();
      this.appState.set('user', null);
    }
  }

  private loadUserById(userId: number) {
    this.userService
      .getUserById(userId)
      .subscribe(
        this.onUserLoaded.bind(this),
        this.onUserError.bind(this)
      )
    ;
  }

  private onUserLoaded(user: User) {
    this.sessionService.setUser(user);
    this.appState.set('user', user);
  }

  private onUserError() {
    this.sessionService.unsetUser();
    this.appState.set('user', null);
  }
}
