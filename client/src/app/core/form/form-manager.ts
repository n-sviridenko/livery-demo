import * as _ from 'lodash';
import { FormGroup, AbstractControl } from '@angular/forms';

import { ApiErrorResponse } from 'app/core';
import { FormErrorList } from './form-error-list';

// @todo: implement through FormErrorList (the same way, but with its typings)
export class FormManager {
  /**
   * @deprecated: use applyErrorList() instead
   */
  public static applyError(form: FormGroup, error: ApiErrorResponse) {
    const errors = _.get(error, 'errors.children');

    if (errors) {
      FormManager.applyChildErrors(form, errors);
    }
  }

  public static applyErrorList(form: FormGroup, list: FormErrorList) {
    FormManager.applyChildErrors(form, list.children);
  }

  public static resetCustomErrors(form: FormGroup) {
    _.forEach<AbstractControl>(form.controls, (control) => {
      if (control.hasError('custom')) {
        const restErrors = _.omit(control.errors, 'custom');

        control.setErrors(restErrors);
      }
    });
  }

  /**
   * @deprecated: use ApiErrorResponse.firstMessage instead
   */
  public static getFirstErrorMessage(error: ApiErrorResponse): string {
    const errors = _.get(error, 'errors.children');

    if (errors) {
      const firstKey = _.keys(errors)[0];
      const firstMessage = _.get(errors, `${firstKey}.errors[0]`);

      return _.toString(firstMessage);
    }

    if (error.message) {
      return error.message;
    }

    return null;
  }

  private static applyChildErrors(group: FormGroup, errors: any) {
    _.each(errors, (error: any, name: string) => {
      const control = group.get(name);

      if (control) {
        FormManager.applyChildError(control, error);
      }
    });
  }

  private static applyChildError(control: AbstractControl, error: any) {
    control.markAsDirty();

    // @todo: when error will be alwsys FormErrorList, check just length
    if (error.errors && error.errors.length) {
      const controlErrors = {
        custom: error.errors.join('\n'),
      };

      control.setErrors(controlErrors);
    } else {
      control.setErrors(null);

      if (control instanceof FormGroup) {
        if (error.children) {
          FormManager.applyChildErrors(control, error.children);
        }
      }
    }
  }
}
