import { AbstractForm } from './abstract-form';
import { FormErrorList } from './form-error-list';

export type StepControls = {
  [step: number]: string[];
};

export abstract class AbstractStepperForm extends AbstractForm {
  protected abstract getStepControls(): StepControls;

  protected isStepValid(step: number): boolean {
    const controlNames = this.getStepControls()[step];

    if (controlNames !== undefined) {
      return this.isControlsValid(controlNames);
    }

    return true;
  }

  protected getFirstErredStep(errors: FormErrorList): number {
    const rawStep = _.findKey(this.getStepControls(), (names: string[]) => {
      const erredNames = Object.keys(errors.children);

      return _.intersection(erredNames, names).length > 0;
    });

    return rawStep !== undefined ? parseInt(rawStep, 10) : null;
  }

  private isControlsValid(names: string[]): boolean {
    return names
      .map(name => this.form.get(name))
      .filter(control => !control || !control.valid) // before view init control is null
      .length === 0
    ;
  }
}
