import * as _ from 'lodash';
import * as moment from 'moment';
import * as isoCountries from 'i18n-iso-countries';
import { Injectable, Inject } from '@angular/core';

import { IntlCountry } from 'app/model';
import { APP_CONFIG } from './app.config';
import { IntlConfig } from './intl.config';
import { SessionService } from './session.service';

@Injectable()
export class IntlService {
  private config: IntlConfig;

  public constructor(
    @Inject(APP_CONFIG) config: any,
    private sessionService: SessionService
  ) {
    this.config = new IntlConfig(config.intl);
  }

  public getCountries(): IntlCountry[] {
    const locale = this.sessionService.getLocale();
    const countryNames = isoCountries.getNames(locale);

    return _.map<any, IntlCountry>(countryNames, <DictionaryIterator> (name, code) => IntlCountry.create(name, code));
  }

  public getMassMeasures(): string[] {
    return this.config.massMeasures;
  }

  public getDefaultMassMeasure(): string {
    return this.config.defaultMassMeasure;
  }

  /**
   * `dateTime` should be provided in UTC timezone
   */
  public formatDateTime(source: moment.Moment, format: string, asLocal: boolean = false): string {
    let dateTime = source.clone();

    if (asLocal) {
      dateTime = dateTime.local();
    }

    const locale = this.sessionService.getLocale();

    return dateTime.locale(locale).format(format);
  }
}
