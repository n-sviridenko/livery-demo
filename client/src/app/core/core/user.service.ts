import { Observable } from 'rxjs';
import { OpaqueToken } from '@angular/core';

import { User } from 'app/model';

export interface UserServiceInterface {
  getUserById(id: number): Observable<User>;
}

export const USER_SERVICE = new OpaqueToken('livery.user.service.user');
