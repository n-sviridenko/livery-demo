export interface IntlConfigInterface {
  massMeasures: string[];
  defaultMassMeasure: string;
}

export class IntlConfig {
  public massMeasures: string[];
  public defaultMassMeasure: string;

  public constructor(config: IntlConfigInterface) {
    this.massMeasures = config.massMeasures;
    this.defaultMassMeasure = config.defaultMassMeasure;
  }
}
