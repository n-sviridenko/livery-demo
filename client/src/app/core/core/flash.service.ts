import { Injectable } from '@angular/core';

export const FLASH_TYPE_INFO = 'info';
export const FLASH_TYPE_SUCCESS = 'success';
export const FLASH_TYPE_WARNING = 'warning';
export const FLASH_TYPE_ERROR = 'danger';

export const FlashTypes = {
  FLASH_TYPE_INFO,
  FLASH_TYPE_SUCCESS,
  FLASH_TYPE_WARNING,
  FLASH_TYPE_ERROR,
};

export type FlashListItem = Array<string>;

export interface FlashList {
  [key: number]: FlashListItem;
}

@Injectable()
export class FlashService {
  private flashes: FlashList = {};

  public has(type: string): boolean {
    return !!this.flashes[type];
  }

  public peek(type: string, defaultValue: FlashListItem = []): FlashListItem {
    return this.has(type) ? this.flashes[type] : defaultValue;
  }

  public peekAll(): FlashList {
    return this.flashes;
  }

  public get(type: string, defaultValue: FlashListItem = []): FlashListItem {
    if (!this.has(type)) {
      return defaultValue;
    }

    const value = this.flashes[type];

    delete this.flashes[type];

    return value;
  }

  public all(): FlashList {
    const value = this.peekAll();

    this.flashes = {};

    return value;
  }

  public add(type: string, message: string) {
    if (!(type in this.flashes)) {
      this.flashes[type] = [];
    }

    this.flashes[type].push(message);
  }

  public set(type: string, messages: FlashListItem) {
    this.flashes[type] = messages;
  }

  public setAll(flashes: FlashList) {
    this.flashes = flashes;
  }

  public clear(type: string = null): FlashList | FlashListItem {
    if (type) {
      return this.get(type);
    }

    return this.all();
  }
}
