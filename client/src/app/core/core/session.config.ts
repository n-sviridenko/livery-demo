export interface SessionConfigInterface {
  locales: Array<string>;
  defaultLocale: string;
  currencies: Array<string>;
  defaultCurrency: string;
}

export class SessionConfig {
  public locales: Array<string>;
  public defaultLocale: string;
  public currencies: Array<string>;
  public defaultCurrency: string;

  public constructor(config: SessionConfigInterface) {
    this.locales = config.locales;
    this.defaultLocale = config.defaultLocale;
    this.currencies = config.currencies;
    this.defaultCurrency = config.defaultCurrency;
  }
}
