import * as Joi from 'joi';
import { Observable } from 'rxjs/Rx';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';

export abstract class AbstractRouteResolver<T> implements Resolve<T> {
  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<T> {
    return this.getValidDataObservable(route, state);
  }

  protected abstract getDataObservable(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<T>;

  protected getRouteSchemaMap(route: ActivatedRouteSnapshot): Joi.SchemaMap {
    return null;
  }

  protected getValidDataObservable(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<T> {
    const routeSchemaMap = this.getRouteSchemaMap(route);

    if (routeSchemaMap) {
      const routeSchema = Joi.object().unknown(true).keys(routeSchemaMap);
      const result = Joi.validate(route, routeSchema);

      if (result.error) {
        return Observable.throw(result.error);
      }
    }

    return this.getDataObservable(route, state);
  }
}
