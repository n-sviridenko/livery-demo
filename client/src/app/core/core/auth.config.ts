// @todo: move tokenKeyName into exported const of the service
export interface AuthConfigInterface {
  baseUrl: string;
  tokenKeyName: string;
}

export class AuthConfig {
  public baseUrl: string;
  public tokenKeyName: string;

  constructor(config: AuthConfigInterface) {
    this.baseUrl = config.baseUrl || '';
    this.tokenKeyName = config.tokenKeyName || 'id_token';
  }
}
