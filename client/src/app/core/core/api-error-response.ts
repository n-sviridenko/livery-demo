import { FormErrorList } from '../form';

export class ApiErrorResponse {
  public code: number = 0;

  public message: string = '';

  public errors: FormErrorList;

  public static create(code: number, message: string, errors: FormErrorList = null): ApiErrorResponse {
    const response = new ApiErrorResponse();
    response.code = code;
    response.message = message;
    response.errors = errors;

    return response;
  }

  // @todo: replace external analogs on it
  public get firstMessage(): string {
    if (this.errors && this.errors.errors.length > 0) {
      return this.errors.errors[0];
    }

    if (this.message) {
      return this.message;
    }

    return null;
  }
}
