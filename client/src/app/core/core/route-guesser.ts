import { Injectable } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

/**
 * Activated route returns expected results just when it's injected in a component.
 * In order to be able to retreive the activated route everywhere, we need to create
 * a guesser that will know which route we need.
 *
 * @see https://github.com/angular/angular/issues/11023
 */

@Injectable()
export class RouteGuesser {
  public activatedRoute: ActivatedRoute;

  public constructor(private router: Router) {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map(() => this.router.routerState.root)
      .subscribe((route) => {
        let activatedRoute = route;

        while (activatedRoute.firstChild) {
          activatedRoute = activatedRoute.firstChild;
        }

        this.activatedRoute = activatedRoute;
      })
    ;
  }
}
