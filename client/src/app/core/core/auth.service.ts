import * as jwt from 'jwt-simple';
import { Observable, Subject } from 'rxjs';
import { Http, Response } from '@angular/http';
import { Injectable, Inject } from '@angular/core';

import { APP_CONFIG } from './app.config';
import { AuthConfig } from './auth.config';

export interface AuthRequest {
  username: string;
  password: string;
}

export interface AuthResponse {
  token: string;
}

export interface AuthTokenPayload {
  username: string;
  userId: number;
  exp: number;
  iat: number;
}

@Injectable()
export class AuthService {
  public loginEvent = new Subject();

  public logoutEvent = new Subject();

  private config: AuthConfig;

  public constructor(
    @Inject(APP_CONFIG) config: any,
    private http: Http
  ) {
    this.config = new AuthConfig(config.auth);
  }

  public getToken(): string {
    return localStorage.getItem(this.getTokenKeyName());
  }

  public getTokenPayload(): AuthTokenPayload {
    let payload = null;

    try {
      payload = jwt.decode(this.getToken(), null, true);
    } catch (e) {} // tslint:disable-line

    return payload;
  }

  public isTokenExpired(): boolean {
    const payload = this.getTokenPayload();

    return !payload || payload.exp && Date.now() > payload.exp * 1000;
  }

  public isLogged(): boolean {
    return !this.isTokenExpired();
  }

  public getUserId(): number {
    const payload = this.isLogged() ? this.getTokenPayload() : null;

    return payload ? payload.userId : null;
  }

  public login(request: AuthRequest): Observable<AuthResponse> {
    const baseUrl = this.getBaseUrl();
    const body = {
      _username: request.username,
      _password: request.password,
    };

    const loginObserver = this.http.post(`${baseUrl}/login_check`, body)
      .map((response: Response) => response.json())
      .catch((response: Response) => Observable.throw(response.json()))
    ;

    loginObserver.subscribe(
      (response: AuthResponse) => {
        this.setToken(response.token);
        this.loginEvent.next();
      }
    );

    return loginObserver;
  }

  public logout() {
    if (this.getToken()) {
      this.unsetToken();
      this.logoutEvent.next();
    }
  }

  private getTokenKeyName() {
    return this.config.tokenKeyName;
  }

  private getBaseUrl() {
    return this.config.baseUrl;
  }

  private setToken(token: string) {
    localStorage.setItem(this.getTokenKeyName(), token);
  }

  private unsetToken() {
    localStorage.removeItem(this.getTokenKeyName());
  }
}
