import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

export interface SharedMessageInterface {
  action: string;
  payload: any;
}

@Injectable()
export class SharedService {
  public changes = new BehaviorSubject({});

  public message = new Subject<SharedMessageInterface>();

  public setData(path: string, value) {
    const data = _.cloneDeep(this.changes.getValue());
    _.set(data, path, value);

    this.changes.next(data);
  }

  public sendMessage(action: string, payload = null) {
    const message: SharedMessageInterface = { action, payload };

    this.message.next(message);
  }
}
