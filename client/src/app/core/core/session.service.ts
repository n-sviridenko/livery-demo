import * as languages from 'langmap';
import { ReplaySubject } from 'rxjs';
import { Inject, Injectable } from '@angular/core';

import { APP_CONFIG } from './app.config';
import { SessionConfig } from './session.config';
import { Locale, Currency, User } from 'app/model';

export const LOCALE_KEY_NAME = 'locale';
export const CURRENCY_KEY_NAME = 'currency';

// @todo: move currency, locale lists into intl

@Injectable()
export class SessionService {
  public userChangeEvent = new ReplaySubject<User>();

  private user: User;

  private config: SessionConfig;

  public constructor(
    @Inject(APP_CONFIG) config: any
  ) {
    this.config = new SessionConfig(config.session);
  }

  public getUser(): User {
    return this.user;
  }

  public setUser(user: User) {
    this.user = user;
    this.userChangeEvent.next(user);
  }

  public unsetUser() {
    this.setUser(null);
  }

  public getLocales(): Locale[] {
    return _(languages)
      .pick(this.config.locales)
      .map((item, code) => Locale.create(item.nativeName, code))
      .value()
    ;
  }

  public getLocale(): string {
    let locale = localStorage.getItem(LOCALE_KEY_NAME);

    return this.config.locales.indexOf(locale) !== -1 ? locale : this.config.defaultLocale;
  }

  public setLocale(locale: string) {
    localStorage.setItem(LOCALE_KEY_NAME, locale);
  }

  // @todo: use just codes without symbols
  public getCurrencies(): Currency[] {
    return [
      Currency.create('$', 'USD'),
      Currency.create('€', 'EUR'),
      Currency.create('₽', 'RUB'),
    ];
  }

  public getCurrency(): string {
    let currency = localStorage.getItem(CURRENCY_KEY_NAME);

    return this.config.currencies.indexOf(currency) !== -1 ? currency : this.config.defaultCurrency;
  }

  public setCurrency(currency: string) {
    localStorage.setItem(CURRENCY_KEY_NAME, currency);
  }
}
