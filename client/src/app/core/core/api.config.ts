export interface ApiConfigInterface {
  baseUrl: string;
  headerName: string;
  headerPrefix: string;
  noTokenScheme: boolean;
  globalHeaders: Array<Object>;
}

export class ApiConfig {
  public baseUrl: string;
  public headerName: string;
  public headerPrefix: string;
  public globalHeaders: Array<Object>;

  constructor(config: ApiConfigInterface) {
    this.baseUrl = config.baseUrl || '';
    this.headerName = config.headerName || 'Authorization';

    if (config.headerPrefix) {
      this.headerPrefix = config.headerPrefix + ' ';
    } else if (config.noTokenScheme) {
      this.headerPrefix = '';
    } else {
      this.headerPrefix = 'Bearer ';
    }

    this.globalHeaders = config.globalHeaders || [];
  }
}
