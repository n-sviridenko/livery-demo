import * as _ from 'lodash';
import { Injectable, Inject } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  Http,
  Headers,
  Request,
  RequestOptions,
  RequestOptionsArgs,
  RequestMethod,
  Response,
} from '@angular/http';

import { ApiConfig } from './api.config';
import { APP_CONFIG } from './app.config';
import { AuthService } from './auth.service';
import { SessionService } from './session.service';
import { ApiErrorResponse } from './api-error-response';
import { ApiErrorResponseHydrator } from '../api/hydrator'; // @todo: move api into api because this a cyclic

function decodeJsonResponse(response: Response, defaultValue = null): any {
  let data = defaultValue;

  try {
    data = response.json();
  } catch (e) {} // tslint:disable-line

  return data;
}

export interface ApiResponseEvent {
  request: Request;
  response: Response;
}

export interface ApiErrorEvent extends ApiResponseEvent {
  error: ApiErrorResponse;
}

export interface UnauthorizedEvent extends ApiErrorEvent {}

@Injectable()
export class ApiService {
  public beforeRequest = new Subject<Request>();

  public afterResponse = new Subject<ApiResponseEvent>();

  public afterError = new Subject<ApiErrorEvent>();

  public unauthorizedEvent = new Subject<UnauthorizedEvent>();

  private config: ApiConfig;

  public constructor(
    @Inject(APP_CONFIG) config: any,
    private authService: AuthService,
    private sessionService: SessionService,
    private http: Http
  ) {
    this.config = new ApiConfig(config.api);
  }

  public request(url: string | Request, options?: RequestOptionsArgs) : Observable<any> {
    if (typeof url === 'string') {
      return this.get(url, options);
    }

    const request: Request = <Request>url;
    const authHeaderValue = this.getAuthHeaderValue();

    if (authHeaderValue) {
      request.headers.set(this.getAuthHeaderName(), authHeaderValue);
    }

    this.beforeRequest.next(request);

    return this.http.request(request)
      .map(response => this.handleResponse(request, response))
      .catch(response => this.handleError(request, response))
    ;
  }

  public get(endpoint: string, options?: RequestOptionsArgs) : Observable<any> {
    return this.requestHelper({ url: this.getEndpointUrl(endpoint), method: RequestMethod.Get }, options);
  }

  public post(endpoint: string, body: any, options?: RequestOptionsArgs) : Observable<any> {
    return this.requestHelper({ url: this.getEndpointUrl(endpoint), body: body, method: RequestMethod.Post }, options);
  }

  public put(endpoint: string, body: any, options ?: RequestOptionsArgs) : Observable<any> {
    return this.requestHelper({ url: this.getEndpointUrl(endpoint), body: body, method: RequestMethod.Put }, options);
  }

  public delete(endpoint: string, options ?: RequestOptionsArgs) : Observable<any> {
    return this.requestHelper({ url: this.getEndpointUrl(endpoint), method: RequestMethod.Delete }, options);
  }

  public patch(endpoint: string, body: any, options?: RequestOptionsArgs) : Observable<any> {
    return this.requestHelper({ url: this.getEndpointUrl(endpoint), body: body, method: RequestMethod.Patch }, options);
  }

  public head(endpoint: string, options?: RequestOptionsArgs) : Observable<any> {
    return this.requestHelper({ url: this.getEndpointUrl(endpoint), method: RequestMethod.Head }, options);
  }

  public getBaseUrl(): string {
    return this.config.baseUrl;
  }

  public getEndpointUrl(endpoint: string): string {
    const baseUrl = this.getBaseUrl();
    const locale = this.sessionService.getLocale();
    const path = _.trim(endpoint, '/');

    return `${baseUrl}/${locale}/${path}`;
  }

  public getAuthHeaderName(): string {
    return this.config.headerName;
  }

  public getAuthHeaderValue(): string {
    return !this.authService.isTokenExpired()
      ? this.config.headerPrefix + this.authService.getToken()
      : null
    ;
  }

  private requestHelper(requestArgs: RequestOptionsArgs, additionalOptions: RequestOptionsArgs): Observable<any> {
    let options = new RequestOptions(requestArgs);

    if (additionalOptions) {
      options = options.merge(additionalOptions);
    }

    const request = new Request(this.mergeOptions(options));

    return this.request(request);
  }

  private mergeOptions(providedOpts: RequestOptionsArgs) {
    if (this.config.globalHeaders) {
      this.setGlobalHeaders(this.config.globalHeaders, providedOpts);
    }

    return new RequestOptions(providedOpts);
  }

  private setGlobalHeaders(headers: Array<Object>, request: Request | RequestOptionsArgs) {
    if (!request.headers) {
      request.headers = new Headers();
    }

    headers.forEach((header: Object) => {
      const key: string = Object.keys(header)[0];
      const headerValue: string = (<any>header)[key];

      request.headers.set(key, headerValue);
    });
  }

  private handleResponse(request: Request, response: Response): any {
    const event: ApiResponseEvent = { request, response };

    this.afterResponse.next(event);

    return decodeJsonResponse(response);
  }

  private handleError(request: Request, response: Response): Observable<ApiErrorResponse> {
    const data = decodeJsonResponse(response, {});
    const error = ApiErrorResponseHydrator.hydrate(new ApiErrorResponse(), data);
    const event: ApiErrorEvent = { request, response, error };

    this.afterError.next(event);

    // @todo: move to listener of `afterError`
    if (response.status === 401) {
      // we need to unset the token
      this.authService.logout();
      this.unauthorizedEvent.next({ request, response, error });
    }

    return Observable.throw(error);
  }
}
