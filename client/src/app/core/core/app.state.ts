import * as _ from 'lodash';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

export type AppStateType = {
  [key: string]: any
};

// @todo: move user, page parameters etc. here

@Injectable()
export class AppState {
  public stateChanges = new Subject();

  private _state: AppStateType = {};

  // already return a clone of the current state
  public get state(): AppStateType {
    return _.clone(this._state);
  }

  public get(prop?: any) {
    // use our state getter for the clone
    const state = this.state;

    return _.has(state, prop) ? _.get(state, prop) : state;
  }

  public set(prop: string, value: any) {
    // internally mutate our state
    _.set(this._state, prop, value);

    this.stateChanges.next(this.state);
  }

  public replaceState(state: AppStateType) {
    this._state = state;

    this.stateChanges.next(this.state);
  }
}
