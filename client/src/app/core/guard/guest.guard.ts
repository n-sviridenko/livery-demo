import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthService } from '../core';

@Injectable()
export class GuestGuard implements CanActivate {
  public constructor(
    private router: Router,
    private authService: AuthService
  ) {}

  public canActivate(): Observable<boolean> {
    const isLogged = this.authService.isLogged();

    if (isLogged) {
      this.router.navigate(['/']);
    }

    return Observable.of(!isLogged);
  }
}
