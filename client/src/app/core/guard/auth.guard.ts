import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from '../core';
import { LoginRedirector } from '../subscriber';

@Injectable()
export class AuthGuard implements CanActivate {
  public constructor(
    private authService: AuthService,
    private loginRedirector: LoginRedirector
  ) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const isLogged = this.authService.isLogged();

    if (!isLogged) {
      this.loginRedirector.redirectToLogin(state.url);
    }

    return Observable.of(isLogged);
  }
}
