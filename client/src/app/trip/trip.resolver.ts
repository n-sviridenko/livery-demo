import * as Joi from 'joi';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { Trip } from 'app/model';
import { TripService, AbstractRouteResolver } from 'app/core';

// @todo: create a beautiful search form on error
@Injectable()
export class TripResolver extends AbstractRouteResolver<Trip> {
  public constructor(private tripService: TripService) {
    super();
  }

  protected getDataObservable(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Trip> {
    const id = parseInt(route.params['id'], 10);

    return this.tripService.getTrip(id);
  }

  protected getRouteSchemaMap(route): Joi.SchemaMap {
    return {
      params: Joi.object().keys({
        id: Joi.number().integer().positive(),
      }),
    };
  }
}
