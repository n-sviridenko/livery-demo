import { NgModule } from '@angular/core';

import { routing } from './trip.routing';
import { SharedModule } from 'app/shared';
import { TripShowComponent } from './show';
import { TripResolver } from './trip.resolver';
import {
  TripCreateComponent,
  TripCreateFormComponent,
  TripCreateHeaderComponent,
} from './create';

@NgModule({
  imports: [
    routing,
    SharedModule,
  ],
  providers: [
    TripResolver,
  ],
  declarations: [
    // create
    TripCreateComponent,
    TripCreateFormComponent,
    TripCreateHeaderComponent,
    // show
    TripShowComponent,
  ],
})
export class TripModule {}
