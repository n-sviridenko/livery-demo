import * as moment from 'moment';
import { NgForm } from '@angular/forms';
import {
  Input,
  Output,
  Component,
  ViewChild,
  EventEmitter,
} from '@angular/core';

import { TripMaxResponseDelaysHourly } from 'app/model';
import {
  IntlService,
  TripMessage,
  StepControls,
  FormErrorList,
  AbstractStepperForm,
} from 'app/core';

const css = require('./create-form.component.scss');

@Component({
  selector: 'l-trip-create-form',
  templateUrl: './create-form.component.html',
})
export class TripCreateFormComponent extends AbstractStepperForm {
  @Input()
  public model: TripMessage;

  @Input()
  public step: number;

  @Output()
  public formSubmit = new EventEmitter();

  @Output()
  public stepChange = new EventEmitter<number>();

  public maxResponseDelaysHourly = TripMaxResponseDelaysHourly;

  public css = css;

  public constructor(intlService: IntlService) {
    super();
  }

  public get minDate(): moment.Moment {
    return moment().startOf('day');
  }

  @Input()
  public set errors(errors: FormErrorList) {
    this.applyFormErrorList(errors);

    if (errors && errors.hasChildren()) {
      const step = this.getFirstErredStep(errors);

      if (step) {
        this.stepChange.emit(step);
      }
    }
  }

  @ViewChild('form')
  public set ngForm(ngForm: NgForm) {
    this.form = ngForm.form;
  }

  public isNextStepAvailable(): boolean {
    return this.isStepValid(this.step);
  }

  public doFormSubmit() {
    this.formSubmit.emit();
  }

  protected getStepControls(): StepControls {
    return {
      1: ['from', 'to', 'departAt', 'arriveAt'],
      2: ['description'],
      3: ['maxResponseDelayHourly'],
    };
  }
}
