import { Component, Input } from '@angular/core';

const css = require('./header.component.scss');

@Component({
  selector: 'l-trip-create-header',
  templateUrl: './header.component.html',
})
export class TripCreateHeaderComponent {
  @Input()
  public step: number;

  @Input()
  public stepsCount: number;

  public css = css;

  public get barWidth(): number {
    return (this.step / this.stepsCount) * 100;
  }
}
