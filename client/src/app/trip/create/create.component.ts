import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { OnInit, Component } from '@angular/core';

import { AbstractLoadable } from 'app/shared';
import { Trip, TripMaxResponseDelaysHourly, TripVehicles } from 'app/model';
import {
  TripService,
  TripMessage,
  SharedService,
} from 'app/core';

const css = require('./create.component.scss');

@Component({
  selector: 'l-trip-create',
  templateUrl: './create.component.html',
})
export class TripCreateComponent extends AbstractLoadable<Trip> implements OnInit {
  public message: TripMessage;

  public step: number = 1;

  public stepsCount: number = 3;

  public css = css;

  public constructor(
    private router: Router,
    private tripService: TripService,
    private sharedService: SharedService
  ) {
    super();

    this.createMessage();
  }

  public ngOnInit() {
    const options = {
      scheme: 'fixed',
      hideHeader: true,
      hideFooter: true,
    };

    setTimeout(() => {
      this.sharedService.setData('pageOptions', options);
    }, 0);
  }

  protected getDataSource(): Observable<Trip> {
    return this.tripService.postTrip(this.message);
  }

  protected onLoad(data: Trip) {
    this.router.navigate(['trips', data.id]);
  }

  private createMessage() {
    const message = new TripMessage();
    message.maxResponseDelayHourly = TripMaxResponseDelaysHourly[1];
    message.vehicle = TripVehicles.CAR;

    this.message = message;
  }
}
