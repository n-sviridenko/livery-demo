import { ActivatedRoute } from '@angular/router';
import { OnInit, Component } from '@angular/core';

import { Trip } from 'app/model';
import { AppState } from 'app/core';

@Component({
  selector: 'l-trip-show',
  templateUrl: './show.component.html',
})
export class TripShowComponent implements OnInit {
  public trip: Trip;

  public constructor(
    private route: ActivatedRoute,
    private appState: AppState
  ) {}

  public ngOnInit() {
    this.route.data.subscribe(
      (data: { trip: Trip }) => {
        this.trip = data.trip;
      }
    );
  }
}
