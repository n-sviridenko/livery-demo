import { Routes, RouterModule }  from '@angular/router';

import { TripShowComponent } from './show';
import { TripCreateComponent } from './create';
import { TripResolver } from './trip.resolver';

const routes: Routes = [
  {
    path: 'create',
    component: TripCreateComponent,
  },
  {
    path: ':id',
    component: TripShowComponent,
    resolve: {
      trip: TripResolver,
    },
  },
];

export const routing = RouterModule.forChild(routes);
