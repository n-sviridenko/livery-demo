export class IntlCountry {
  public name: string;

  public code: string;

  public static create(name: string, code: string): IntlCountry {
    const country = new IntlCountry();
    country.name = name;
    country.code = code;

    return country;
  }
}
