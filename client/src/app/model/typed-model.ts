import { Model } from './model';

export class TypedModel extends Model {
  public type: string;
}
