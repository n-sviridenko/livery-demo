// @todo: move into session service
export class Locale {
  public name: string;
  public code: string;

  public static create(name: string, code: string): Locale {
    const locale = new Locale();
    locale.name = name;
    locale.code = code;

    return locale;
  }
}
