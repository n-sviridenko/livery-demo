export const MediaCategories = {
  PROFILE_PICTURE: 'profile_picture',
  ITEM_PICTURE: 'item_picture',
};

export abstract class AbstractMedia {
  public id: number;
  public type: string;
  public url: string;
}
