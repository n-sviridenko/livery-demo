export const ThumbnailTypes = {
  PROFILE_MEDIUM: 'profile_medium',
  PROFILE_SMALL: 'profile_small',
};

export class Thumbnail {
  public type: string;
  public url: string;
}
