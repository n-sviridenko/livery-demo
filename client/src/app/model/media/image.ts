import { Thumbnail } from './thumbnail';
import { AbstractMedia } from './abstract-media';
import { ImageInterface } from './image-interface';

export class Image extends AbstractMedia implements ImageInterface {
  public width: number;
  public height: number;
  public thumbnails: Thumbnail[] = [];

  public getThumbnail(type: string): Thumbnail {
    return _.find(this.thumbnails, { type });
  }

  public getThumbnailUrl(type: string): string {
    const thumbnail = this.getThumbnail(type);

    return thumbnail ? thumbnail.url : null;
  }
}
