export interface ImageInterface {
  url: string;
  width: number;
  height: number;
}
