import * as _ from 'lodash';
import * as moment from 'moment';

import { Model } from '../model';
import { PhoneNumber } from '../phone';
import { Image, ThumbnailTypes } from '../media';

export const UserSexes = {
  MALE: 'male',
  FEMALE: 'female',
};

export class User extends Model {
  public firstName: string;

  public lastName: string;

  public locale: string;

  public picture: Image;

  public age: number;

  public createdAt: moment.Moment;

  public lastLogin: moment.Moment;

  public about: string;

  public email: string;

  public phone: PhoneNumber;

  public sex: string;

  public birthdate: moment.Moment;

  public get mediumPictureUrl(): string {
    return this.picture
      ? this.picture.getThumbnailUrl(ThumbnailTypes.PROFILE_MEDIUM)
      : null;
  }

  public get smallPictureUrl(): string {
    return this.picture
      ? this.picture.getThumbnailUrl(ThumbnailTypes.PROFILE_SMALL)
      : null;
  }
}
