import { AbstractLocation } from './abstract-location';

export abstract class CountryAwareLocation extends AbstractLocation {
  public lat: number;
  public lng: number;
  public formattedAddress: string;

  public getDisplayName(): string {
    return this.formattedAddress;
  }
}
