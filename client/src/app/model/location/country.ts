import { AbstractLocation } from './abstract-location';

export class Country extends AbstractLocation {
  public code: string;
  public name: string;
  public slug: string;

  public getAlias(): string {
    return this.slug;
  }

  public getDisplayName(): string {
    return this.name;
  }

  public getCountryCode(): string {
    return this.code;
  }
}
