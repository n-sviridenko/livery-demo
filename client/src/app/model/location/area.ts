import { Country } from './country';
import { CountryAwareLocation } from './country-aware-location';

export class Area extends CountryAwareLocation {
  public name: string;
  public slug: string;
  public country: Country;

  public getAlias(): string {
    return `${this.slug}--${this.country.slug}`;
  }

  public getCountryCode(): string {
    return this.country.code;
  }
}
