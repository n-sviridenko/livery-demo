export abstract class AbstractLocation {
  public id: number;
  public type: string;
  public placeId: string;

  public abstract getAlias(): string;
  public abstract getDisplayName(): string;
  public abstract getCountryCode(): string;
}
