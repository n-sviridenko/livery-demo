import { AbstractLocation } from './abstract-location';

// @todo: move into location service
export class LocationPrediction {
  public placeId: string;
  public name: string;
  public types: Array<string>;

  public static createFromLocation(location: AbstractLocation): LocationPrediction {
    const data = {
      placeId: location.placeId,
      name: location.getDisplayName(),
    };

    return new LocationPrediction(data);
  }

  public constructor(data: any = {}) {
    this.placeId = data.placeId;
    this.name = data.name;
    this.types = data.types || [];
  }
}
