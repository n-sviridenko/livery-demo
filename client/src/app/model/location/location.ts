import { Area } from './area';
import { CountryAwareLocation } from './country-aware-location';

export class Location extends CountryAwareLocation {
  public area: Area;

  public get name(): string {
    return this.formattedAddress;
  }

  public getAlias(): string {
    return this.area.getAlias();
  }

  public getCountryCode(): string {
    return this.area.country.code;
  }
}
