import * as moment from 'moment';

import { User } from '../user';
import { CountryAwareLocation } from '../location';

export const TripMaxResponseDelaysHourly = [1, 3, 6, 12];

export const TripVehicles = {
  CAR: 'car',
  TRAIN: 'train',
  PLANE: 'plane',
  SHIP: 'ship',
};

export class Trip {
  public id: number;

  public from: CountryAwareLocation;

  public to: CountryAwareLocation;

  public traveller: User;

  public departAt: moment.Moment;

  public arriveAt: moment.Moment;

  public maxResponseDelayHourly: number;

  public description?: string;

  public vehicle: string;

  public createdAt: moment.Moment;
}
