export class PhoneNumber {
  public countryCode: number;
  public nationalNumber: string;
  public localized: string;

  public get cleanNationalNumber(): string {
    return this.nationalNumber.replace(/\D/, '');
  }

  public format(): string {
    return `+${this.countryCode}${this.cleanNationalNumber}`;
  }
}
