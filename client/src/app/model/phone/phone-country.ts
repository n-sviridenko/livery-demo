export class PhoneCountry {
  public name: string;
  public code: string;
  public phoneNumberCode: number;
}
