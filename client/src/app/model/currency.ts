// @todo: move into session service
export class Currency {
  public symbol: string;
  public code: string;

  public static create(symbol: string, code: string): Currency {
    const currency = new Currency();
    currency.symbol = symbol;
    currency.code = code;

    return currency;
  }
}
