import { TabsModule } from 'ng2-bootstrap/tabs';
import { AlertModule } from 'ng2-bootstrap/alert';
import { ModalModule } from 'ng2-bootstrap/modal';
import { TooltipModule } from 'ng2-bootstrap/tooltip';
import { DropdownModule } from 'ng2-bootstrap/dropdown';
import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TypeaheadModule } from 'ng2-bootstrap/typeahead';
import { PaginationModule } from 'ng2-bootstrap/pagination';
import { ProgressbarModule } from 'ng2-bootstrap/progressbar';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import {
  removeNgStyles,
  createNewHosts,
  createInputTransfer
} from '@angularclass/hmr';

import { BaseModule } from './base';
import { config } from 'config/config';
import { SharedModule } from './shared';
import { routing } from './app.routing';
import { AppComponent } from './app.component';
import {
  // core
  AppState,
  ApiService,
  APP_CONFIG,
  AuthService,
  IntlService,
  AppStateType,
  FlashService,
  RouteGuesser,
  USER_SERVICE,
  SharedService,
  SessionService,
  // guards
  AuthGuard,
  GuestGuard,
  // apis
  TripService,
  UserService,
  // subscribers
  AuthSubscriber,
  PageSubscriber,
  FlashSubscriber,
  LoginRedirector,
  AuthFormSubscriber,
  LoadingBarSubscriber,
} from 'app/core';

const core = [
  ApiService,
  AuthService,
  IntlService,
  FlashService,
  RouteGuesser,
  SharedService,
  SessionService,
  { provide: APP_CONFIG, useValue: config },
  { provide: USER_SERVICE, useClass: UserService },
];

const apis = [
  TripService,
  UserService,
];

const guards = [
  AuthGuard,
  GuestGuard,
];

const subscribers = [
  AuthSubscriber,
  PageSubscriber,
  FlashSubscriber,
  LoginRedirector,
  AuthFormSubscriber,
  LoadingBarSubscriber,
];

type StoreType = {
  state: AppStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

@NgModule({
  imports: [
    // vendors
    BrowserModule,
    SlimLoadingBarModule.forRoot(),
    // bootstrap
    TabsModule.forRoot(),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    DropdownModule.forRoot(),
    TypeaheadModule.forRoot(),
    PaginationModule.forRoot(),
    ProgressbarModule.forRoot(),
    // app
    routing,
    BaseModule,
    SharedModule,
  ],
  providers: [
    ...core,
    ...apis,
    ...guards,
    ...subscribers,
    AppState,
  ],
  declarations: [
    AppComponent,
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule {
  public constructor(
    private appRef: ApplicationRef,
    private appState: AppState,
    // services
    private sharedService: SharedService,
    private sessionService: SessionService,
    // subscribers
    private authSubscriber: AuthSubscriber,
    private pageSubscriber: PageSubscriber,
    private flashSubscriber: FlashSubscriber,
    private loginRedirector: LoginRedirector,
    private authFormSubscriber: AuthFormSubscriber,
    private loadingBarSubscriber: LoadingBarSubscriber
  ) {}

  public hmrOnInit(store: StoreType) {
    if (!store || !store.state) {
      return;
    }

    console.log('HMR store', JSON.stringify(store, null, 2));

    this.appState.replaceState(store.state);

    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();

    delete store.state;
    delete store.restoreInputValues;
  }

  public hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);

    store.state = this.appState.state;
    store.disposeOldHosts = createNewHosts(cmpLocation);
    store.restoreInputValues  = createInputTransfer();

    removeNgStyles();
  }

  public hmrAfterDestroy(store: StoreType) {
    store.disposeOldHosts();

    delete store.disposeOldHosts;
  }
}
