import { NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';

import { AbstractAuthForm } from 'app/shared';
import { AuthService, FlashService, LoginRedirector } from 'app/core';

@Component({
  selector: 'l-auth-form',
  templateUrl: './auth-form.component.html'
})
export class AuthFormComponent extends AbstractAuthForm implements OnInit {
  @ViewChild('form')
  public form: NgForm;

  public constructor(
    protected authService: AuthService,
    protected flashService: FlashService,
    private loginRedirector: LoginRedirector
  ) {
    super();
  }

  public ngOnInit() {
    super.ngOnInit();

    if (!this.loginRedirector.returnUrl) {
      this.loginRedirector.returnUrl = '/';
    }
  }
}
