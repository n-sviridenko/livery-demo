import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared';
import { LoginComponent, AuthFormComponent } from './auth';
import { PageNotFoundComponent } from './page-not-found.component';

@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    // auth
    LoginComponent,
    AuthFormComponent,
    // other
    PageNotFoundComponent,
  ],
})
export class BaseModule {}
