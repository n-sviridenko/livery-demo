declare class PhotoSwipeUIDefault extends PhotoSwipeUI_Default {}

declare module 'photoswipe/dist/photoswipe-ui-default' {
  export default PhotoSwipeUIDefault;
}
