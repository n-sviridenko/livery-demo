declare module 'joi' {

  export interface LocationSchema extends AnySchema<LocationSchema> {
    alias(): LocationSchema;
    locationAlias(): LocationSchema;
  }

  export function location(): LocationSchema;

}
